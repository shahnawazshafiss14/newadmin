  <?php 
  $check_p  = check_permisson('30');
  ?>
   <!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Combo Offer</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <!-- [ form-element ] start -->
							
                            <div class="col-md-12">
							<div class="page-header">
                         
						 <div class="page-wrapper" style="background-color: #fff;padding: 20px 0PX;margin-bottom: 25px;">
                            
							 
							<form action="<?= base_url();?>admin/combooffer/addcombo" method="POST">
							<div class="row">
									<div class="col-xl-3 col-md-4 mb-3 ml-3">
												
													<label>Combo Name</label>
													<input type="text" id="combo_name" name="combo_name" value="<?= !empty($combo->combo_name) ? $combo->combo_name : ''; ?>" class="form-control" placeholder="" required>
													
													<input type="hidden" id="combo_slug" name="combo_slug" value="<?= !empty($combo->combo_slug) ? $combo->combo_slug : ''; ?>" class="form-control" placeholder="">
									
									</div>
									<div class="col-xl-3 col-md-4 mb-3">
												
													<label>Combo Discount</label>
													<select name="combo_offer" id="combo_offer" class="form-control" required>
													<option value="">Select Discount</option>
													<?php 
													
													for($i = 0; $i <100; $i++):
													if(!empty($combo->combo_offer)):
													?>
													<option value="<?= $i; ?>" <?= $combo->combo_offer == $i ? 'selected' : ''; ?>><?= $i; ?>%</option>
													<?php else: ?>
													<option value="<?= $i; ?>"><?= $i; ?>%</option>
													<?php endif;?>
													<?php endfor;?>
													</select>
									
									</div>
									<?php 
									$find_url_slug = $this->uri->segment(4);
													if(!empty($find_url_slug)){
									?>
									<div class="col-xl-2 col-md-4 mb-3">
													<label>Status</label>
													<select name="combo_status" id="combo_status" class="form-control" required>
													<option value="">Select status</option>
													<option value="1" <?= $combo->combo_status == 1 ? 'selected' : ''; ?>>Active</option>
													<option value="0" <?= $combo->combo_status == 0 ? 'selected' : ''; ?>>inActive</option>
													
													</select>
									
									</div>
									<?php 
									}
									?>
									<div class="col-xl-3 col-md-4 mb-3">
									
									<?php 
															if(!empty($check_p->is_inserted)):
															?>
													<input class="btn btn-primary mt-4" type="submit" name="addcombo" id="addcombo" value="<?= !empty($combo->combo_slug) ? 'Update' : 'Add Combo'; ?>" />
													<?php endif;?>
									
                                    </div>
									</div>
									</form>
									  </div>
									</div>
                                <div class="card">
                                     
									
									<hr />
									<?php 
									$find_url_slug = $this->uri->segment(4);
													if(empty($find_url_slug)){
									?>
                                    <div class="card-body">

                                        <!-- [ configuration table ] start -->

                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Combo Name</th>                                          
                                                            <th>Combo Offer</th>                                          
                                                            <th>Status</th>                                          
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
													
														$s=1;	
														if(count($combos)>0):			
															foreach($combos as $combo): ?>
																<tr>
																	<td><?=$s++;?></td>
																	<td><?=$combo->combo_name; ?></td>
																	<td><?=$combo->combo_offer; ?>%</td>
																	<td><span class="label label-<?= $combo->combo_status == 1 ? 'success' : 'danger'; ?>"><?= status($combo->combo_status); ?></span></td>
																	<td>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
																	| <a title="Edit Details" href="<?= base_url(ADMIN_URL.'/combooffer/add/'.$combo->combo_slug)?>" class="btn theme-bg text-white f-12 btn-sm"><i class="feather icon-edit"></i></a> | 
																	<?php endif;?>
															<?php 
															if(!empty($check_p->is_deleted)):
															?>
																	<a title="Delete combosdelete" href="javascript:;" class="btn btn-danger btn-sm"><i class="feather icon-trash-2"></i></a>
																	<?php 
															endif;
															?>
																	</td>
																</tr>
															<?php
															endforeach; 
														endif;
													
													?>		
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Combo Name</th>                                             
                                                            <th>Combo Offer</th>                                             
                                                            <th>statusS</th>                                             
                                                            <th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                  </div>
								  <?php 
													}
								  ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
    <!-- [ Main Content ] end -->
</div>