  <?php 
  $check_p  = check_permisson('34');
  ?>	 

<div class="row">
                <style>
                    .tblPass {
                        background-color: white;
                        font-size: 14px;
                        width: 100%;
                    }

                        .tblPass th, td {
                            padding: 10px;
                        }

                    .pagebreak {
                        page-break-before: always;
                    }
                </style>
                <table style="width: 450px; height: 600px; border-collapse: collapse" border="1" class="tblPass" cellspacing="0">
                    <tbody><tr>
                        <td colspan="2" style="text-align: center">
                            <div>
                                <span style="padding-right: 11px">GP No. <?= $consignmentd->gp_id;?></span>
                                <span style="padding-right: 6px">GATE PASS / CASH MEMO</span>
                                <span style="text-align: right">Date : <?= $consignmentd->genrated_date;?></span>
                            </div>
							<?php 
							$acc_details = $this->login_model->find_by_row($this->acc_id);
							?>

                            <p style="font-family: arial; font-size: 25px; margin-top: -2px !important"><b><i><?= $acc_details->user_company; ?></i></b></p>
                            <!--p style="font-family: arial; font-size: 16px; margin-top: -26px !important">FLEET OWNERS &amp; TRANSPORT CONTRACTORS</p-->
                            <p style="font-weight: normal; font-size: 15px; margin-top: -12px !important; text-align: left; margin-left: 8px;">
                                <strong>Office : </strong> <?= $acc_details->user_address; ?>
                            </p>

                            <p style="font-weight: normal; font-size: 15px; margin-top: -10px !important; text-align: left; margin-left: 8px;">
                                <strong>Contact No. :</strong> <?= $acc_details->user_mobile; ?>
                            </p>
                            

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <strong>Consignee / Party : </strong>
                            <?= //$this->client_model->find_by_name($consignmentd->consignee_name);
							$consignmentd->consignee_details;
							?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <strong>Receive with Thanks from Mr/Ms : </strong>
                            <?= $consignmentd->receiver_name; ?>
                        </td>
                    </tr>
					<tr>
                        <td colspan="2">
                            <table border="1" class="tblPass" cellspacing="0">
							<tr class="thead-default">
								<th>Goods</th>
								<th>Nug(s)</th>
								<th>Charged/Nug</th>
								<th>Weight</th>
								<th style="text-align:right">Total</th>
							</tr>
							<?php 
											$find_url_slug = $this->uri->segment(4);
											if(!empty($find_url_slug)){
											$fetch_array= array(
												'consignment_id' => $consignmentd->consignment_id
											);
											
										$fetch_googds = $this->consignmentgoods_model->viewRecordAnyR($fetch_array);
										if(count($fetch_googds) > 0){
										$i = 1;
										foreach($fetch_googds as $fet_good):
										if(!empty($fet_good->order_nugs)):
											?>
										<tr>
											<td>
												<h6><?= $this->product_model->find_by_name($fet_good->goods_id); ?></h6>
											</td>
											<td><?= !empty($fet_good->order_nugs) ? $fet_good->order_nugs : ''; ?></td>
											<td><?= !empty($fet_good->order_nug_charge) ? $fet_good->order_nug_charge : ''; ?></td>
											<td><?= !empty($fet_good->order_nug_weight) ? $fet_good->order_nug_weight : ''; ?></td>
											<td style="text-align:right"><?= $fet_good->order_nugs * $fet_good->order_nug_charge; ?></td>
										</tr>
										<?php 
										endif;
										endforeach;
										}
									}
									?>
							</table>
                        </td>
                    </tr>
                     <tr>
                        
                        <td colspan="2"><strong>Vehicle Number : </strong>
                            <?= $this->fleetmanagement_model->find_by_truckno($consignmentd->truck_no); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Consignment No. : </strong> <?=  amb($consignmentd->consignment_id);?></td>
                    </tr>

                    <tr>
                        <td>
                            <strong>Station From:  </strong>
                            <?= $this->zone_model->find_by_name($consignmentd->zone_from); ?>
                        </td>

                        <td><strong>Station To: </strong>
                           <?= $this->zone_model->find_by_name($consignmentd->zone_to); ?>
						   </td>

                    </tr>


                    <tr>
                        <td>
                            <div style="font-weight: bold"></div> 
                            <p><strong>Note:</strong> DEMURRAGE IS CHARGEABLE IF DELIVERY EFFECTS AFTER THREE DAYS OF RECEIPT</p>

                            <p><strong>Cashier: </strong> <?= $acc_details->user_fullname; ?></p>
                            <p style="font-weight: normal; font-size: 14px; line-height; 14px !important;"><strong>GSTNO. :</strong> <?= $acc_details->user_gstno; ?> </p>

                        </td>

                        <td style="width: 49%">
                            <table border="1" style="width: 100%; border-collapse: collapse" class="tblPass">
                                <tbody><tr>
                                    <td style="width: 25%; font-weight: bold">Charges</td>
                                    <td style="text-align: center; font-weight: bold">Rs</td> 
                                </tr>
                                <tr style="font-family: -moz-fixed">
                                    <td style="font-weight: bold">Freight</td>
                                    <td style="text-align: right">
                                        <?= $consignmentd->freight; ?></td>
                                     
                                </tr>
                                <tr style="font-family: -moz-fixed">
                                    <td style="font-weight: bold">Hamali</td> 
                                    <td style="text-align: right">
                                        <?= !empty($consignmentd->hamali) ? $consignmentd->hamali : 0; ?></td>
                                </tr>
                                <tr style="font-family: -moz-fixed">
                                    <td style="font-weight: bold">Statistical</td>
                                    <td style="text-align: right">
                                        <?= !empty($consignmentd->statistical) ? $consignmentd->statistical : 0; ?></td>
                                     
                                </tr>
                                <tr style="font-family: -moz-fixed">
                                    <td style="font-weight: bold">Demurrage</td>
                                    <td style="text-align: right">
                                        <?= !empty($consignmentd->demurrage) ? $consignmentd->demurrage : 0; ?></td> 
                                </tr>
                                <tr style="font-weight: bold">
                                    <td>Total</td>
									<td style="text-align: right">
                                        <?= 
									$consignmentd->amount 
									?></td> 
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>

                </tbody></table>
            </div>