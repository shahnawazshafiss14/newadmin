  <?php 
  $check_p  = check_permisson('34');
  ?>	 
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container page">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                    </div>
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="javascript:;"><i class="feather icon-home"></i></a></li>
                                        <li class="breadcrumb-item"><a href="javascript:;">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="javascript:;"><?= $Page_Title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <d<div class="row">
                                <!-- [ Invoice ] start -->
                                <div class="container" id="printTable">
                                    <div>
                                        <div class="card">
                                            <div class="row invoice-contact">
                                                <div class="col-md-8">
                                                    <div class="invoice-box row">
                                                        <div class="col-sm-12">
                                                            <table class="table table-responsive invoice-table table-borderless">
                                                                <tbody>  
																	<tr>
																		<td style="font-weight:bold;">Conginee Details </td>
																	</tr>
                                                                    <tr>
                                                                        <td><?= $consignmentd->consignee_details;?></td>
                                                                    </tr> 
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4"></div>
                                            </div>
                                            <div class="card-block">
                                                <div class="row invoive-info">
                                                    <div class="col-md-4 col-xs-12 invoice-client-info">
                                                        <h6>Consignor Details: </h6>                                                        
                                                        <p><?= $consignmentd->consigner_details;?></p>
                                                    </div>
                                                    <div class="col-md-4 col-sm-6">
                                                        <h6>Billty Information :</h6>
                                                        <table class="table table-responsive invoice-table invoice-order table-borderless">
                                                            <tbody>
                                                                <tr>
                                                                    <th>Date :</th>
                                                                    <td><?= !empty($consignmentd->bilty_date) ? datedmymonth($consignmentd->bilty_date) : date('d/m/Y');?></td>
                                                                </tr>
																<tr>
                                                                    <th>Payment Mode :</th>
                                                                    <td><span class="label label-info"><?= paid_by($consignmentd->paid_by);?></span></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Status :</th>
                                                                    <td>
                                                                        <span class="label label-warning">Pending</span>
                                                                    </td>
                                                                </tr>
                                                                 
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-4 col-sm-6">
                                                        <h6 class="m-b-20">Invoice Number <span>#<?= $consignmentd->invoice_no;?></span></h6>
                                                        <h6 class="m-b-20">eWay Bill Number <span>#<?= $consignmentd->form_number;?></span></h6>
                                                        <h6 class="text-uppercase text-primary">Total Amount:
                                                            <span><?= $consignmentd->nettotal;?></span>
                                                        </h6>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="table-responsive">
                                                            <table class="table  invoice-detail-table">
                                                                <thead>
                                                                    <tr class="thead-default">
                                                                        <th>Goods</th>
                                                                        <th>Nug(s)</th>
                                                                        <th>Charged/Nug</th>
                                                                        <th>Weight</th>
                                                                        <th style="text-align:right">Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
																<?php 
											$find_url_slug = $this->uri->segment(4);
											if(!empty($find_url_slug)){
											$fetch_array= array(
												'consignment_id' => $consignmentd->consignment_id
											);
											
										$fetch_googds = $this->consignmentgoods_model->viewRecordAnyR($fetch_array);
										if(count($fetch_googds) > 0){
										$i = 1;
										foreach($fetch_googds as $fet_good):
											?>
											<tr>
												<td><h6><?= $this->product_model->find_by_name($fet_good->goods_id); ?></h6></td>
												<td><?= !empty($fet_good->order_nugs) ? $fet_good->order_nugs : ''; ?></td>
												<td><?= !empty($fet_good->order_nug_charge) ? $fet_good->order_nug_charge : ''; ?></td>
												<td><?= !empty($fet_good->order_nug_weight) ? $fet_good->order_nug_weight : ''; ?></td>
												<td style="text-align:right"><?= $fet_good->order_nugs * $fet_good->order_nug_charge; ?></td>
											</tr>
																	<?php 
																	endforeach;
										}
											}
																	?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="row invoive-info">
                                                    <div class="col-md-2 col-xs-12 invoice-client-info">
                                                        <h6>Freight Charges: </h6>                                                        
                                                        <p><?= !empty($consignmentd->freight) ? $consignmentd->freight : 0;?></p>
                                                    </div>
                                                    <div class="col-md-2 col-sm-3">
                                                        <h6>Bilty Charges :</h6>
                                                          <p><?= !empty($consignmentd->bilty) ? $consignmentd->bilty : 0;?></p>
                                                    </div>
													<div class="col-md-2 col-sm-3">
                                                        <h6>Other Charges Narration :</h6>
                                                          <p><?= !empty($consignmentd->narrationcrg) ? $consignmentd->narrationcrg : 0;?></p>
                                                    </div>
													<div class="col-md-2 col-sm-3">
                                                        <h6>Other Charges :</h6>
                                                          <p><?= !empty($consignmentd->narrationother) ? $consignmentd->narrationother : 0;?></p>
                                                    </div> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 totalrem">
                                                        <table class="table table-responsive invoice-table invoice-total">
                                                            <tbody>
                                                                 
                                                                <tr class="text-info">
                                                                    <td>
                                                                        <hr>
                                                                        <h5 class="text-primary m-r-10">Total :</h5>
                                                                    </td>
                                                                    <td>
                                                                        <hr>
                                                                        <h5 class="text-primary"><?= $consignmentd->nettotal;?></h5>
                                                                    </td>
                                                                </tr>
																
                                                            </tbody>
                                                        </table>
														<?php 
											$gatepass_arr = array(
											'consigment_id' => $consignmentd->consignment_id,
											'status' => '1' 
											);
											$feth_row = $this->gatepass_model->viewRecordAny($gatepass_arr); 
											if(!empty($feth_row->payment_id)){
											$gatepass_arr_app = array(
												'id' => $feth_row->payment_id
											);
											$feth_row_payment = $this->consigpayment_model->viewRecordAny($gatepass_arr_app); 
											}
											
											?>
											<!--form name="frmgatepass" action="" method="POST"--> 
										<form name="frmgatepass" action="<?= base_url(); ?>admin/gatepass/generategetpass" method="POST">
											<div class="row">
											<div class="col-xl-3 col-md-4 mb-3"> 
                                                <div class="form-group">
                                                    <label>Hamali</label>
													<input type="text" name="hamali" id="hamali" class="form-control" value="<?= !empty($feth_row->hamali) ? $feth_row->hamali : ''; ?>" />
                                                </div>
                                            </div>
											<div class="col-xl-3 col-md-4 mb-3"> 
                                                <div class="form-group">
                                                    <label>Statistical</label>
													<input type="text" name="statistical" id="statistical" class="form-control" value="<?= !empty($feth_row->statistical) ? $feth_row->statistical : ''; ?>" />
                                                </div>
                                            </div>
											<div class="col-xl-3 col-md-4 mb-3"> 
                                                <div class="form-group">
                                                    <label>Demurrage</label>
													<input type="text" name="demurrage" id="demurrage" class="form-control" value="<?= !empty($feth_row->demurrage) ? $feth_row->demurrage : ''; ?>" />
                                                </div>
                                            </div>
											</div>
										<div class="row">
											<div class="col-xl-3 col-md-4 mb-3">
											<div class="form-group">
											<label>Payment </label>
											<select class="form-control" name="payment_type" id="payment_type11">
											<?php 
											if(!empty($feth_row_payment->payment_mode)):
											?>
												<option value="<?= !empty($feth_row_payment->payment_mode) ? $feth_row_payment->payment_mode : ''; ?>" selected><?= !empty($feth_row_payment->payment_mode) ? payment_mode_type1($feth_row_payment->payment_mode) : ''; ?></option>
											<?php 
											endif;
											?>
														 <option value="">Select Payment Mode</option>
														 <option value="1">Cash</option>
														 <option value="2">Cheque</option>
													</select>
												</div>
                                                </div>
												<div class="col-xl-3 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label>Amount </label>
															<?php 
															
															if($consignmentd->paid_by == 1){
																$nettotal = !empty($feth_row_payment->amount) ? $feth_row_payment->amount : $consignmentd->nettotal;
																
															}else{
																$nettotal = !empty($feth_row_payment->amount) ? $feth_row_payment->amount : 0;
															}
															?>
															<input type="hidden" id="totalamount1" class="form-control" value="<?= !empty($nettotal) ? $nettotal : 0;?>" />
															<input type="text" name="totalamount" id="<?= !empty($feth_row_payment->amount) ? 'none' : 'totalamount'  ?>" class="form-control" value="<?= !empty($nettotal) ? $nettotal : 0;?>" />
                                                        </div>
                                                </div>
												<div class="col-xl-3 col-md-4 mb-3" id="checks"> 
                                                        <div class="form-group">
                                                            <label>Check Number</label>
															<input type="text" name="checks" class="form-control" value="<?= !empty($feth_row_payment->checks) ? $feth_row_payment->checks : ''; ?>" />
                                                        </div>
                                                </div>
												<div class="col-xl-3 col-md-4 mb-3" id="checks"> 
                                                        <div class="form-group">
                                                            <label>Receiver Name</label>
															<input type="text" name="receiver_name" id="receiver_name" class="form-control" value="<?= !empty($feth_row->receiver_name) ? $feth_row->receiver_name : ''; ?>" />
                                                        </div>
                                                </div>
												<div class="col-xl-3 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label>Remarks </label>
															<input type="text" name="remarks" class="form-control" value="<?= !empty($feth_row_payment->remarks) ? $feth_row_payment->remarks : ''; ?>" />
															<input type="hidden" name="payment_id_id" value="<?= !empty($feth_row_payment->id) ? $feth_row_payment->id : ''; ?>" /> 
															<input type="hidden" name="gatepass_id" value="<?= !empty($feth_row->id) ? $feth_row->id : ''; ?>" /> 
															<input type="hidden" name="consignement_id" value="<?= $consignmentd->consignment_id; ?>" /> 
															<input type="hidden" name="consignment_slug" value="<?= $consignmentd->consignment_slug; ?>" /> 
                                                        </div>
                                                </div> 		 
														</div>
														<div class="row text-center">
                                            <div class="col-sm-12 invoice-btn-group text-center">
							<input type="submit" id="genrateGate" class="btn btn-primary btn-print-invoice m-b-10" name="genrateGate" value="<?= !empty($feth_row->id) ? 'Update' : 'Generate'; ?> Gate Pass" />
                                            </div>
                                        </div>
										</form>
										
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!-- [ Invoice ] end -->
                            </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div> 
