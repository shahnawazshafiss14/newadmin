  <?php 
  $check_p  = check_permisson('34');
  ?>
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                               
                                <!-- [ form-element ] start -->
                                <div class="col-sm-12">
                                    <div class="card"> 
                                        <div class="card-body">
                                            
                                             <!-- [ configuration table ] start -->
                               
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
														<th>Sr.</th>
														    <th>Challan No.</th>
                                                            <th>Bilty No.</th>
                                                            <th>Bilty Booking Date</th> 
                                                            <th>Consignee</th>
                                                            <th>Consigner</th> 
                                                            <th>Quantity</th>															
															<th>Delivery Status</th>
															<th>Reciver Name</th>
															<th>Delivery Date</th>
															<th>Truck No.</th> 
															<th>Action</th> 
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php  
													$i = 1;
													foreach($consignments as $congi): 
													$arr_gate = array(
														'consigment_id' => $congi->consignment_id
													);
													$fetch_qest = $this->gatepass_model->viewRecordAny($arr_gate);
													if(count($fetch_qest) > 0){
														$status_r = 'Recived';
														$reciver_name = $fetch_qest->receiver_name;
														$delivery_date = $fetch_qest->genrated_date;
													}else{
														$status_r = 'Waiting';
														$reciver_name = 'Waiting';
														$delivery_date = 'Waiting';
													}
													?>
                                                        <tr>
														
														<td><?= $i++;?></td>
														<td><?= $congi->challan_no; ?></td>
                                                            <td><?= amb($congi->consignment_id); ?></td>
                                                            <td><?= $congi->bilty_date; ?></td>
                                                            <td><?= $this->client_model->find_by_name($congi->consignee_name); ?></td>
                                                            <td><?= $this->client_model->find_by_name($congi->consigner); ?></td>  
															<td><?= consignment_good($congi->consignment_id, 'nug'); ?> Nags</td>
															<td><?= $status_r;?></td>
															<td><?= $reciver_name;?></td>
															<td><?= $delivery_date; ?></td>
															<td><?= $this->fleetmanagement_model->find_by_truckno($congi->truck_no); ?></td>  
															<td>
															<?php 
															 
															$gatepass_arr = array(
											'consigment_id' => $congi->consignment_id,
											'status' => '1' 
											);
											$feth_row = $this->gatepass_model->viewRecordAny($gatepass_arr); 
											if(count($feth_row) > 0):
													if(!empty($check_p->is_viewed)):
															?>
															<a title="Print GatePass" href="<?= base_url().'admin/gatepass/invoice/'.$congi->consignment_slug; ?>" class="label theme-bg text-white f-12 btn-sm"><i class="feather icon-printer"></i></a>
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
															| <a title="Edit Details" href="<?= base_url(ADMIN_URL.'/gatepass/details/'.$congi->consignment_slug)?>" class="btn btn-primary text-white f-12 btn-sm"><i class="feather icon-edit"></i></a>
															<?php endif;?>
															<?php 
															else:
															if(!empty($check_p->is_viewed)):
															?>
															<a title="Generate" href="<?= base_url(ADMIN_URL.'/gatepass/details/'.$congi->consignment_slug); ?>" class="label theme-bg2 text-white f-12 btn-sm"><i class="feather icon-eye"></i></a>
															<?php 
															endif;
															endif;
															?>															
															
															
															<?php 
															if(!empty($check_p->is_deleted)):
															?>
															| <a title="Delete Consignment" data-url="consignment/deletecong" data-deleteid="<?= $congi->consignment_id; ?>" href="javascript:;" class="btn btn-danger btn-sm deletes"><i class="feather icon-trash-2"></i></a>
															<?php 
															endif;
															?>
															 </td>
															 
                                                        </tr>
                                                     <?php 
													 endforeach;
													 ?>  
                                                    </tbody>
                                                    
                                                </table>
                                            </div>
                                        </div>
                               
                                            </div> 
												 
                                                </div> 
                                            </div>   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                  