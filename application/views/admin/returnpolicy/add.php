 <?php 
  $check_p  = check_permisson('13');
  ?> 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-1"></div><div class="col-sm-10">
							
							<!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Return Policy Details</h5>
											<?php 
											$flash_data =  $this->session->flashdata('rpolicy_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> <?= $flash_data; ?>
											</div>
											<?php 
											endif;
											?>
											<?php 
											$flash_data2 =  $this->session->flashdata('warning');
											if(!empty($flash_data2)):
											?>
											<div class="alert alert-warning alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Warning!</strong> <?= $flash_data2; ?>
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('rpolicy_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                        </div>
                                        <div class="card-body">
										
                                            <form action="<?= U_A_URL . 'returnpolicy/savereturnpolicy'?>" id="frmReturnpolicy" name="frmReturnpolicy" method="POST" />
                                            <div class="row">
											 
												<div class="col-xl-6 col-md-6 mb-3">
                                                        <div class="form-group">
                                                            <label>Number of days</label>
                                                            <input type="text" id="nodays" name="nodays" value="<?= !empty($returnpolicyd->r_days) ? $returnpolicyd->r_days : ''; ?>" class="form-control" placeholder="">
                                                        </div> 
                                                </div>
												<div class="col-xl-6 col-md-6 mb-3">
                                                        <div class="form-group">
                                                            <label>Cut Off Price(%)</label>
                                                            <select id="r_cutoff" name="r_cutoff" class="form-control">
															<option value="">Select Cut offrate</option>
															<option value="2">2%</option>
															<option value="3">3%</option>
															<?php 
															for($i = 5; $i <= 100; $i++):
															?>
															<option value="<?= $i; ?>"><?= $i; ?>%</option>
															<?php 
															$i = $i + 4;
															endfor;
															?>
															</select>
                                                        </div> 
                                                </div> 
                                                <div class="col-xl-12 col-md-12 mb-3">
                                                        <div class="form-group">
                                                            <label>Description</label>
                                                            <input type="text" id="r_description" name="r_description" value="<?= !empty($returnpolicyd->r_description) ? $returnpolicyd->r_description : ''; ?>" class="form-control" placeholder="">
                                                        </div>
														<input type="hidden" id="slug" name="slug" value="<?= !empty($returnpolicyd->slug) ? $returnpolicyd->slug : ''; ?>" class="form-control" placeholder="">
                                                </div> 
												
												 <div class="mb-5"></div>
												 <?php 
													if(!empty($check_p->is_inserted)):
												 ?>
												 <button type="submit" id="btnAddReturn" name="btnAddReturn" class="btn btn-primary float-right"><?= !empty($returnpolicyd->slug) ? 'Update' : 'Submit'; ?></button>
												<?php 
															endif;
															?>
                                                </div>
												
												
                                            </div>
											</form>  	
												 
                                                </div> 
                                            </div>
											
											
											<div class="col-sm-1"></div>
                                            
                                                    
                                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>