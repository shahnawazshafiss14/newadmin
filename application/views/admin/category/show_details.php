 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                           <div class="col-sm-1"></div>  <div class="col-sm-10">
							 <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                                    <div class="card">
                                        
                                        <div class="card-body">
										
                                            
                                            <div class="row">
											 
											<div class="col-xl-4 col-md-6 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Company Name</label>
                                                          : <?= !empty($clientd->company_name) ? $clientd->company_name : ''; ?> 
                                                        </div> 
                                                </div>
											
                                                
                                                <!--div class="col-xl-4 col-md-4 mb-3">
                                                    
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Client Name  ( owner )</label>
                                                            : <?= !empty($clientd->client_name_ower) ? $clientd->client_name_ower : ''; ?> 
                                                        </div>
                                                       
                                                    
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Consignement Type</label>
                                                            : <?php
															$array_cong = array(
																'1' => 'Consignee',
																'2' => 'Consigner'
															);

															echo !empty($clientd->type) ? $array_cong[$clientd->type] : ''; ?> 
                                                        </div>
                                                       
                                                    
                                                </div>
												 <div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Contact Person</label>
                                                            : <?= !empty($clientd->contact_person) ? $clientd->contact_person : ''; ?> 
                                                        </div>
												 
                                                </div-->
												
												
												 <!--div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Consignee (For Bilty Purpose):</label>
                                                            : <?= !empty($clientd->consignee) ? $clientd->consignee : ''; ?> 
                                                        </div>
												 
                                                </div-->
												
												
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">GST No.:</label>
                                                            : <?= !empty($clientd->gst_no) ? $clientd->gst_no : ''; ?> 
                                                        </div>
												 
                                                </div>
												
												
													<div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Price:</label>
                                                            : <?= !empty($clientd->price) ? $clientd->price : ''; ?> 
                                                        </div>
												 
                                                </div>
												
												
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Address:</label>
                                                            : <?= !empty($clientd->address) ? $clientd->address : ''; ?> 
                                                             
															
                                                        </div>
												 
                                                </div>
												
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Contact Number:</label>
                                                           : <?= !empty($clientd->contact_number) ? $clientd->contact_number : ''; ?> 
                                                        </div>
												 
                                                </div>
												
												
												<div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Email ID:</label>
                                                           : <?= !empty($clientd->email_id) ? $clientd->email_id : ''; ?> 
                                                        </div> 
												 <div class=" mb-5"> </div>
												  
												
                                                </div>
												
												
                                            </div>
											 
														
												 
                                                </div>
												
												
												
                                            </div>
											
											
                                                </div><div class="col-sm-1"></div> 

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>