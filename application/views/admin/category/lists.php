  <?php 
  $check_p  = check_permisson('11');
  ?>
   <!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
               
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-1"></div> <div class="col-sm-10">
							
							 <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Category List</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                                <div class="card">
                                   <!--  <div class="card-header">
                                        <h5>Clients</h5>
										<?php echo $this->session->flashdata('msg');?>
                                    </div>-->
                                    <div class="card-body">

                                        <!-- [ configuration table ] start -->

                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Category Name</th>                                                           
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
														$s=1;	
														if(count($categorys)>0):			
															foreach($categorys as $category): ?>
																<tr>
																	<td><?=$s++;?></td>
																	<td><?=$category->category_name?></td>
																	<td>
																	<?php 
															if(!empty($check_p->is_viewed)):
															?>
																	<a title="Client View Details" href="<?= base_url(ADMIN_URL.'/category/details/'.$category->category_slug)?>" class="btn btn-dark btn-sm"><i class="feather icon-eye"></i></a>
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
																	 <a title="Edit Details" href="<?= base_url(ADMIN_URL.'/category/add/'.$category->category_slug)?>" class="btn btn-dark btn-sm"><i class="feather icon-edit"></i></a>  
																	<?php endif;?>
															<?php 
															if(!empty($check_p->is_deleted)):
															?>
																	<a title="Delete Client" href="javascript:;" class="btn btn-danger btn-sm"><i class="feather icon-trash-2"></i></a>
																	<?php 
															endif;
															?>
																	</td>
																</tr>
															<?php
															endforeach; 
														endif;
													?>		
                                                    </tbody>
                                                   
                                                </table>
                                            </div>
                                        </div>
                                  </div>
                                </div>
                            </div> <div class="col-sm-1"></DIV>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
    <!-- [ Main Content ] end -->
</div>