 <?php 
  $check_p  = check_permisson('10');
  ?> 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-1"></div><div class="col-sm-10">
							<!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Category Details</h5>
											<?php 
											$flash_data =  $this->session->flashdata('category_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> Category has been add successful.
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('category_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                        </div>
                                        <div class="card-body">
										
                                            <form action="<?= U_A_URL . 'category/savecategory'?>" id="frmCategory" name="frmCategory" method="POST" />
                                            <div class="row">
											 
											<div class="col-xl-4 col-md-6 mb-3"> 
                                                        <div class="form-group">
                                                            <label>Category Name</label>
                                                            <input type="text" id="category_name" name="category_name" value="<?= !empty($categoryd->category_name) ? $categoryd->category_name : ''; ?>" class="form-control" placeholder="">
                                                        </div> 
                                            </div>
												 
												<input type="hidden" name="category_slug" id="category_slug" value="<?= !empty($categoryd->category_slug) ? $categoryd->category_slug : ''; ?>" />
												   
												
												<div class="col-xl-4 col-md-4 mb-3"> 
                                                        
												 <div class=" mb-5"> </div>
												 <?php 
													if(!empty($check_p->is_inserted)):
												 ?>
												 <button type="submit" id="btnAddCategory" name="btnAddCategory" class="btn btn-primary float-right"><?= !empty($categoryd->category_slug) ? 'Update' : 'Submit'; ?></button>
												<?php 
															endif;
															?>
                                                </div>
												
												
                                            </div>
											</form> 
														
												 
                                                </div> 
												
                                            </div> 
                                                </div><div class="col-sm-1"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>