<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Add Gst Rate</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Gst Rate</h5>
										
										<?php echo form_error('gst_rate'); ?>
                                    </div>
                                    <div class="card-body">
										<form class="form-signin" action="<?php echo site_url(ADMIN_URL.'/gst/save');?>" method="post">
                                        <div class="row">
                                            <div class="col-xl-4 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label>Gst Rate</label>
                                                        <input type="text" class="form-control" name="gst_rate" placeholder="">
                                                    </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
										</form>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>