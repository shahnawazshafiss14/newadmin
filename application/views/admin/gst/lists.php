<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
   <!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Gst Rate</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Gst Rate</h5>
										<?php echo $this->session->flashdata('msg');?>
                                    </div>
                                    <div class="card-body">

                                        <!-- [ configuration table ] start -->

                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Gst Rate</th>                                                           
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
														$s=1;	
														if(count($gsts)>0):			
															foreach($gsts as $product): ?>
																<tr>
																	<td><?=$s++;?></td>
																	<td><?=$product->gst_rate?></td>
																	<td><a href="<?=base_url(ADMIN_URL.'/gst/edit/'.$product->id)?>" class="label theme-bg2 text-white f-12">Edit</a></td>
																</tr>
															<?php
															endforeach; 
														endif;
													?>		
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Gst Rate</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
    <!-- [ Main Content ] end -->
</div>