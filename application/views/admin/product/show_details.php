 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-1"></div><div class="col-sm-10">
							<!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
				
				
				<div class="row">
                                            <div class="col-md-12">
                                                <!-- Product detail page start -->
                                                <div class="card product-detail-page">
                                                    <div class="card-block">
                                                        <div class="row">
                                                            <div class="col-lg-5 col-xs-12">
                                                                <div class="port_details_all_img row">
                                                                    <div class="col-lg-12 m-b-15">
                                                                        <div id="big_banner" class="slick-initialized slick-slider">
                                                                            <div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 2758px;">
																			
																			
																			<div class="port_big_img slick-slide" data-slick-index="0" aria-hidden="true" style="width: 394px; position: relative; left: 0px; top: 0px; z-index: 998; transition: opacity 500ms ease 0s;" tabindex="-1">
                                                                                
																				
																				<img class="img img-fluid" src="<?= U_A_ASST.'images/'. $product->p_image; ?>" alt="Big_ Details">
                                                                            </div>
																			
																			
																			
																			
																			
																			</div></div>
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 product-right">
                                                                        <div id="small_banner" class="slick-initialized slick-slider"><button class="slick-prev slick-arrow" aria-label="Previous" type="button" style="">Previous</button>
                                                                            <div class="slick-list draggable" style="padding: 0px 50px;"><div class="slick-track" style="opacity: 1; width: 1406px; transform: translate3d(-518px, 0px, 0px);">
																			
																			
																			<div class="slick-slide slick-cloned" data-slick-index="-5" aria-hidden="true" style="width: 74px;" tabindex="-1">
                                                                                <img class="img img-fluid" src="../files/assets/images/product-detail/pro-d-s-3.jpg" alt="small-details">
                                                                            </div>
																			
																			
																			
																			</div></div>
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                            
                                                                        <button class="slick-next slick-arrow" aria-label="Next" type="button" style="">Next</button></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-7 col-xs-12 product-detail" id="product-detail">
                                                                <div class="row">
                                                                    <div>
                                                                       
                                                                        <div class="col-lg-12">
                                                                            <h4 class="pro-desc"><?= $product->p_name; ?> </h4>
                                                                        </div>
                                                                        <div class="col-lg-12">
                                                                            <span class="txt-muted"> Product Category : <?= !empty($product->p_category) ? $this->category_model->find_by_name($product->p_category) : ''; ?>  </span>
                                                                        </div>
                                                                        
                                                                        <div class="col-lg-12">
                                                                           
                                                                            <hr>
                                                                            <p> <?= !empty($product->p_desc) ? $product->p_desc : ''; ?> 
                                                                            </p>
                                                                            <hr>
																			
																			<h6 class="f-16 f-w-600 m-t-10 m-b-10">
																			Price: <?= !empty($product->p_price) ? $product->p_price : ''; ?> 
																			</h6>
																			
																			<h6 class="f-16 f-w-600 m-t-10 m-b-10">
																			SKU: <?= !empty($product->p_skucode) ?$product->p_skucode : ''; ?> 
																			</h6>
																			
                                                                            <h6 class="f-16 f-w-600 m-t-10 m-b-10">
																			Stock Availability : <?= !empty($product->p_stock) ? $product->p_stock : ''; ?> 
																			</h6> 
                                                                        </div>
                                                                        <div class="col-xl-3 col-sm-12">
                                                                            
                                                                        </div>
                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Product detail page end -->
                                            </div>
                                        </div>
				
                                    <div class="card">
                                       
                                        <div class="card-body">
										
                                            
                                            <div class="row">
											 
											<div class="col-xl-4 col-md-6 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Product Name</label>
                                                          : <?= $product->p_name; ?> 
                                                        </div> 
                                                </div>
											
                                                
                                                <div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Product SKU Code</label>
                                                            : <?= !empty($product->p_skucode) ?$product->p_skucode : ''; ?> 
                                                        </div> 
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Product Price</label>
                                                            : <?= !empty($product->p_price) ? $product->p_price : ''; ?> 
                                                        </div> 
                                                </div>
												 <div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Product Stock</label>
                                                            : <?= !empty($product->p_stock) ? $product->p_stock : ''; ?> 
                                                        </div> 
                                                </div> 
												<div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Product Category</label>
                                                            : <?= !empty($product->p_category) ? $this->category_model->find_by_name($product->p_category) : ''; ?> 
                                                        </div> 
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Product Member Discount</label>
                                                            : <?= !empty($product->p_memberdiscount) ? $product->p_memberdiscount : ''; ?> 
                                                        </div> 
                                                </div> 
												<div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Created on</label>
                                                            : <?= !empty($product->created_on) ? $product->created_on : ''; ?> 
                                                        </div> 
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Product Policy</label>
                                                            : <?php 
												$p_rpolicy = explode(',', $product->p_rpolicy);
												 
														$arr_cat = array(
														'view_status' => '1'
														);
														$ret_fetch = $this->returnpolicy_model->viewRecordAnyR($arr_cat);
														for($i=0; $i < count($ret_fetch); $i++):
														if(in_array($i, $p_rpolicy)){
															$check = "checked";
														}else{
															$check = "";
														}
														
														?>
                                                <input type="checkbox" name="p_rpolicy[]" class="p_rpolicy" value="<?= $ret_fetch[$i]->id; ?>" <?= $check; ?> disabled> <?= $ret_fetch[$i]->r_description; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
												<?php 
														endfor;
														?> 
                                                        </div> 
                                                </div>												
												<div class="col-sm-6" style="border-right: 2px dotted #ded2d2;">


			<div class="col-sm-9">
				<div id="response">
			    <b>Product Images</b>
			    <input type="file" id="productImage" name="productImage" />
			    <input type="button" name="uploadproductImage" id="uploadproductImage" value="Upload" class="btn btn-primary float-right" />
				</div>
			</div>

			<img style="width:100%;height:250px;margin-bottom:10px;margin-top:10px;"
			     src="<?= U_A_ASST.'images/'. $product->p_image; ?>" class="img" />

			<div class="row">
			    <?php
			    $ara = array(
				'pid' => $product->id,
				'view_status' => '1',
					
			    );
			    $extraimgs = $this->pimages_model->viewRecordAnyR($ara);
			    foreach ($extraimgs as $eximg):
				?>
    			    <div class="col-sm-2 rowimage" style="padding: 0px;padding-left: 16px;margin-bottom:15px;">
    				<img style="width:100%;height:75px;marign-bottom:5px;" src="<?= U_A_ASST.'images/'. $eximg->file_name?>" />

    				<a href="javascript:;" style="float:left;" id="deleteimg" class="deleteimg" data-id="<?= $eximg->id; ?>"> <i class="fa fa-trash"></i> Delete</a>


    			    </div>
			    <?php endforeach; ?>
                        </div>

                        <form class="form-horizontal" action="<?= base_url(); ?>admin/product/extraimagesupload" method="post" enctype="multipart/form-data">
                            <span id="addmore" style="cursor: pointer;border: 1px solid #aba0a0;padding: 5px 20px;background: #f77f14;">add more</span>
							<input type="hidden" name="productEdit" id="productEdit" value="<?= $product->id; ?>">
                            <div class="first" style="display:none">
                                <div class="form-group" style="margin-top:10px;"> 
                                    <label class="control-label col-sm-8 imgup" for="email">
									<input type="file" class="imgname" name="files[]" multiple></label>
                                    <div class="col-sm-4">
                                        <input type="submit" name="upload" id="" class="btn btn-success" value="upload" >
                                    </div>
                                </div>
                            </div>
                        </form>
		    </div>  	
                                            </div>
											 
														
												 
                                                </div>
												
												
												
                                            </div>
											
											
											
                                                </div><div class="col-sm-1"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>