<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Edit Products</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Products</h5>
										
										<?php echo form_error('p_name'); ?>
                                    </div>
                                    <div class="card-body">
										<form class="form-signin" action="<?php echo site_url(ADMIN_URL.'/product/edit/'.$product->id);?>" method="post">
                                        <div class="row">
                                            <div class="col-xl-4 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label>Product Name</label>
                                                        <input type="text" class="form-control" value="<?=$product->p_name; ?>" name="p_name" placeholder="Product Name">
														<input type="hidden" class="form-control" value="<?=$product->id; ?>" name="p_id" placeholder="">
                                                    </div>
                                            </div>
											<div class="col-xl-2 col-md-2 mb-3">
                                                    <div class="form-group">
                                                        <label>SKU Code</label>
                                                        <input type="text" class="form-control" value="<?=$product->p_skucode; ?>" name="p_skucode" id="p_skucode" placeholder="">
                                                    </div>
                                            </div>											
											
											<div class="col-xl-2 col-md-2 mb-3">
                                                    <div class="form-group">
                                                        <label>Product Price</label>
                                                        <input type="text" class="form-control price" value="<?=$product->p_price; ?>" name="p_price" id="p_price" placeholder="">
                                                    </div>
                                            </div>
											<div class="col-xl-2 col-md-2 mb-3">
                                                    <div class="form-group">
                                                        <label>Product Stock</label>
                                                        <input type="text" class="form-control number" name="p_stock" value="<?=$product->p_stock; ?>" id="p_stock" placeholder="">
                                                    </div>
                                            </div>
											<div class="col-xl-2 col-md-2 mb-3">
                                                    <div class="form-group">
                                                        <label>Product Location</label>
                                                <select id="p_location" name="p_location" class="form-control">
														<option value="">Select Location</option>
														<?php 
														$arr_cat = array(
														'view_status' => '1'
														);
														$loc_fetch = $this->zone_model->viewRecordAnyR($arr_cat);
														foreach($loc_fetch as $fetchloc):
														if($fetchloc->id == $product->p_location){
															$sel = "selected";
														}else{
															$sel = "";
														}
														?>
														<option value="<?= $fetchloc->id; ?>" <?= $sel; ?>><?= $fetchloc->zone_name; ?></option>
														<?php 
														endforeach;
														?>
												</select>
                                                    </div>
                                            </div>
											<div class="col-xl-3 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label>Select Category</label>
														<select id="p_category" name="p_category" class="form-control">
														<option value="">Select Category</option>
														<?php 
														$arr_cat = array(
														'view_status' => '1'
														);
														$cat_fetch = $this->category_model->viewRecordAnyR($arr_cat);
														foreach($cat_fetch as $fetchcat):
														if($fetchcat->id == $product->p_category){
															$sel = "selected";
														}else{
															$sel = "";
														}
														?>
														<option value="<?= $fetchcat->id; ?>" <?= $sel; ?>><?= $fetchcat->category_name; ?></option>
														<?php 
														endforeach;
														?>
														</select>
														
                                                    </div>
                                            </div>
											<div class="col-xl-2 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label>Card Member Discount</label>
														<select id="p_memberdiscount" name="p_memberdiscount" class="form-control">
														<option value="">Select Discount</option>
														<?php 
															for($i = 5; $i <= 100; $i++):
															if($i == $product->p_memberdiscount){
															$sel = "selected";
														}else{
															$sel = "";
														}
															?>
															<option value="<?= $i; ?>" <?= $sel; ?>><?= $i; ?>%</option>
															<?php 
															$i = $i + 4;
															endfor;
															?>
														</select>
                                                    </div>
                                            </div>
											 <div class="col-xl-3 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label>Product Description</label>
                                                        <textarea class="form-control" rows="4" name="p_desc" id="p_desc"><?= $product->p_desc; ?></textarea>
                                                    </div>
                                            </div>
											
											<!-- class="col-xl-3 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label>Main Image</label>
                                                  <input type="file" class="form-control"  name="p_image" id="p_image" placeholder="">
												 <img width="150" height="150" src="<?= U_A_ASST.'images/'. $product->p_image?>" alt="" />
                                                    </div>
                                            </div>
											
											<div class="col-xl-3 col-md-6 mb-3">
                                                    <div class="form-group">
                                                        <label>Secondry Image</label>
                                                  <input type="file" class="form-control" name="files[]" id="p_extra_image" placeholder="" multiple>
                                                    </div>
                                            </div-->
											
											<div class="col-xl-12 col-md-12 mb-3">
											<?php 
												$p_rpolicy = explode(',', $product->p_rpolicy);
												 
														$arr_cat = array(
														'view_status' => '1'
														);
														$ret_fetch = $this->returnpolicy_model->viewRecordAnyR($arr_cat);
														for($i=0; $i < count($ret_fetch); $i++):
														if(in_array($i, $p_rpolicy)){
															$check = "checked";
														}else{
															$check = "";
														}
														
														?>
                                                <input type="checkbox" name="p_rpolicy[]" class="p_rpolicy" value="<?= $ret_fetch[$i]->id; ?>" <?= $check; ?>> <?= $ret_fetch[$i]->r_description; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
												<?php 
														endfor;
														?>
                                            </div> 
                                        </div>
                                        <button type="submit" class="btn btn-primary">Update</button>
										</form>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>