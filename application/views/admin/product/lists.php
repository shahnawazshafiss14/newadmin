<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
  $check_p  = check_permisson('3');
  ?>
   <!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">List Products</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header"> 
										<?php echo $this->session->flashdata('msg');?>
                                    </div>
                                    <div class="card-body">

                                        <!-- [ configuration table ] start -->

                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Product Image</th>                                                           
                                                            <th>Product Name</th>                                                           
                                                            <th>Product Stock</th>                                                           
                                                            <th>Product Price</th>                                                           
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
														$s=1;	
														if(count($products)>0):			
															foreach($products as $product): ?>
																<tr>
																	<td><?=$s++;?></td>
																	<td><img width="50" height="50" src="<?= U_A_ASST.'images/'. $product->p_image?>" alt="" /></td>
																	<td><?=$product->p_name; ?></td>
																	<td><?=$product->p_stock; ?></td>
																	<td><?=$product->p_price; ?></td>
																	<td>
																	<?php 
															if(!empty($check_p->is_viewed)):
															?>
																	<a title="Product View Details" href="<?= base_url(ADMIN_URL.'/product/details/'.$product->id)?>" class="btn btn-dark btn-sm"><i class="feather icon-eye"></i></a>
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
																	<a title="Edit Details" href="<?=base_url(ADMIN_URL.'/product/edit/'.$product->id)?>" class="btn btn-blue btn-sm">Edit</a>
															<?php endif;?>	
																	</td>
																</tr>
															<?php
															endforeach; 
														endif;
													?>		
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                           <th>Sr.no.</th>
                                                            <th>Product Image</th>                                                           
                                                            <th>Product Name</th>                                                           
                                                            <th>Product Stock</th>                                                           
                                                            <th>Product Price</th>                                                           
                                                            <th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
    <!-- [ Main Content ] end -->
</div>