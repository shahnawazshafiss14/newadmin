<?php
defined('BASEPATH') OR exit('No direct script access allowed');
  $check_p  = check_permisson('3');
?>
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Add Products</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    
                                    <div class="card-body">
										<form class="form-signin" action="<?php echo site_url(ADMIN_URL.'/product/save');?>" method="post" enctype="multipart/form-data">
                                        <div class="row">
                                            <div class="col-md-6">
											
											<div class="row">
												<label class="control-label input-sm1 col-md-4">Product Name</label>
												<div class="col-md-6">
													<div class="form-group">
														<input type="text" class="form-control" name="p_name" id="p_name" placeholder="">
													</div>
												</div>
                                            </div>
											
											<div class="row">
											<label class="control-label input-sm1 col-md-4">SKU Code</label>
											<div class="col-md-6">
											<div class="form-group">
                                                         
                                                        <input type="text" class="form-control" name="p_skucode" id="p_skucode" placeholder="">
                                                    </div></div>
                                            </div>
											
											
											
											<div class="row">
											<label class="control-label input-sm1 col-md-4">Product Price</label>
											<div class="col-md-6">
											<div class="form-group">
                                                         
                                                       <input type="text" class="form-control price" name="p_price" id="p_price" placeholder="">
                                                    </div></div>
                                            </div>
											
											
											<div class="row">
											<label class="control-label input-sm1 col-md-4">Product Stock</label>
											<div class="col-md-6">
											<div class="form-group">
                                                         
                                                      <input type="text" class="form-control number" name="p_stock" id="p_stock" placeholder="">
                                                    </div></div>
                                            </div>
											
											
											<div class="row">
											<label class="control-label input-sm1 col-md-4">Product Location</label>
											<div class="col-md-6">
											<div class="form-group">
                                                         
                                                       <select id="p_location" name="p_location" class="form-control">
														<option value="">Select Location</option>
														<?php 
														$arr_cat = array(
														'view_status' => '1'
														);
														$loc_fetch = $this->zone_model->viewRecordAnyR($arr_cat);
														foreach($loc_fetch as $fetchloc):
														?>
														<option value="<?= $fetchloc->id; ?>"><?= $fetchloc->zone_name; ?></option>
														<?php 
														endforeach;
														?>
												</select>
                                                    </div></div>
                                            </div>
											
											
											
											
											<div class="row">
											<label class="control-label input-sm1 col-md-4">Select Category</label>
											<div class="col-md-6">
											<div class="form-group">
                                                         
                                                      <select id="p_category" name="p_category" class="form-control">
														<option value="">Select Category</option>
														<?php 
														$arr_cat = array(
														'view_status' => '1'
														);
														$cat_fetch = $this->category_model->viewRecordAnyR($arr_cat);
														foreach($cat_fetch as $fetchcat):
														?>
														<option value="<?= $fetchcat->id; ?>"><?= $fetchcat->category_name; ?></option>
														<?php 
														endforeach;
														?>
														</select>
                                                    </div></div>
                                            </div> 
											<div class="row">
											<label class="control-label input-sm1 col-md-4">Card Member Discount</label>
											<div class="col-md-6">
											<div class="form-group">
                                                         
                                                      <select id="p_memberdiscount" name="p_memberdiscount" class="form-control">
														<option value="">Select Discount</option>
														<?php 
															for($i = 0; $i <= 100; $i++):
															?>
															<option value="<?= $i; ?>"><?= $i; ?>%</option>
															<?php 
															 
															endfor;
															?>
														</select>
                                                    </div></div>
                                            </div>
											
											
											
											
											
											
											<div class="row">
											<label class="control-label input-sm1 col-md-4">Main Image</label>
											<div class="col-md-6">
											<div class="form-group">
                                                         
                                                      <input type="file" class="form-control" name="p_image" id="p_image" placeholder="">
                                                    </div></div>
                                            </div>
											
											
											<div class="row">
											<label class="control-label input-sm1 col-md-4">Secondry Image</label>
											<div class="col-md-6">
											<div class="form-group">
                                                         
                                                      <input type="file" class="form-control" name="files[]" id="p_extra_image" placeholder="" multiple>
                                                    </div></div>
                                            </div>
											
											
											
											</div>
											
											
											
											
											
											
											
											<div class="col-md-6">
											
											<div class="row">
											<label class="control-label input-sm1 col-md-4">Product Description</label>
											<div class="col-md-6">
											
											<div class="form-group">
                                                         <textarea class="form-control max-textarea" maxlength="255" rows="4" type="text" class="form-control" name="p_desc" id="p_desc" ></textarea>
                                                      

													
													</div>
													
													
													
																								<?php 
																								$i = 1;
														$arr_cat = array(
														'view_status' => '1'
														);
														$ret_fetch = $this->returnpolicy_model->viewRecordAnyR($arr_cat);
														foreach($ret_fetch as $fetchre):
														
														?>
													<div class="form-group">
                                                        <div class="checkbox checkbox-primary d-inline">
														 <input type="checkbox" name="p_rpolicy[]" class="p_rpolicy" value="<?= $fetchre->id; ?>" id="checkbox-p2-<?= $i; ?>" ="">
                                                            <label for="checkbox-p2-<?= $i; ?>" class="cr"></label>
                                                        </div>
														<?= $fetchre->r_days; ?> Days = <?= $fetchre->r_cutoff; ?> Money will be return<br/>
														<?= !empty($fetchre->r_description) ? $fetchre->r_description: ''; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    </div>
													<?php 
													$i++;
														endforeach;
														?> 
													</div>
                                            </div>
											
											</div>
											
										</div>
										<?php 
										if(!empty($check_p->is_inserted)):
										?>
                                        <button type="submit" id="abtnProduct" class="btn btn-primary">Submit</button>
										<?php endif;?>
										</form>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>