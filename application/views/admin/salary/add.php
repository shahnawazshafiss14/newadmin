 <?php 
  $check_p  = check_permisson('10');
  ?> 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Client Details</h5>
											<?php 
											$flash_data =  $this->session->flashdata('client_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> Client has been add successful.
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('client_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                        </div>
                                        <div class="card-body">
										
                                            <form action="<?= U_A_URL . 'client/saveclient'?>" id="frmClient" name="frmClient" method="POST" />
                                            <div class="row">
											 
											<div class="col-xl-4 col-md-6 mb-3">
                                                    
													
                                                        <div class="form-group">
                                                            <label>Company Name</label>
                                                            <input type="text" id="company_name" name="company_name" value="<?= !empty($clientd->company_name) ? $clientd->company_name : ''; ?>" class="form-control" placeholder="" <?= !empty($clientd->client_slug) ? 'disabled' : ''; ?>>
                                                        </div>
                                                       
                                                    
                                                  
                                                </div>
											
                                                
                                                <div class="col-xl-4 col-md-4 mb-3">
                                                    
                                                        <div class="form-group">
                                                            <label>Client Name  ( owner )</label>
                                                            <input type="text" id="client_name_ower" name="client_name_ower" value="<?= !empty($clientd->client_name_ower) ? $clientd->client_name_ower : ''; ?>" class="form-control" placeholder="" <?= !empty($clientd->client_slug) ? 'disabled' : ''; ?>>
                                                        </div>
                                                       
                                                    
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
                                                        <div class="form-group">
                                                            <label>Consigement Type</label>
															<select id="type" name="type" class="form-control">
															<?php 
															$array_cong = array(
																'1' => 'Consignee',
																'2' => 'Consigner'
															);
															if(!empty($clientd->type)):
															?>
															<option value="<?= $clientd->type; ?>" selected><?= $array_cong[$clientd->type]; ?></option>
															<?php endif;?>
															<option value="">Select Consigement</option>
															<option value="1">Consignee</option>
															<option value="2">Consigner</option>  
															</select>
                                                        </div>
                                                       
                                                    
                                                </div>
												 <div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label>Contact Person</label>
                                                            <input type="text" id="contact_person" name="contact_person" value="<?= !empty($clientd->contact_person) ? $clientd->contact_person : ''; ?>" class="form-control" placeholder="">
                                                        </div>
												 
                                                </div>
												
												
												 <div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label>Consignee (For Bilty Purpose):</label>
                                                            <input type="text" id="consignee" name="consignee" value="<?= !empty($clientd->consignee) ? $clientd->consignee : ''; ?>" class="form-control" placeholder="">
                                                        </div>
												 
                                                </div>
												
												
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label>GST No.:</label>
                                                            <input type="text" id="gst_no" name="gst_no" value="<?= !empty($clientd->gst_no) ? $clientd->gst_no : ''; ?>" class="form-control" placeholder="">
                                                        </div>
												 
                                                </div>
												
												
													<div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label>Price:</label>
                                                            <input type="text" id="price" name="price" value="<?= !empty($clientd->price) ? $clientd->price : ''; ?>" class="form-control price" placeholder="">
                                                        </div>
												 
                                                </div>
												
												
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label>Address:</label>
                                                            <input type="text" id="address" name="address" value="<?= !empty($clientd->address) ? $clientd->address : ''; ?>" class="form-control" placeholder="">
                                                            <input type="hidden" id="client_slug" name="client_slug" value="<?= !empty($clientd->client_slug) ? $clientd->client_slug : ''; ?>" class="form-control" placeholder="">
															
                                                        </div>
												 
                                                </div>
												
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
												
                                                        <div class="form-group">
                                                            <label>Contact Number:</label>
                                                            <input type="text" maxlength="10" id="contact_number" name="contact_number" value="<?= !empty($clientd->contact_number) ? $clientd->contact_number : ''; ?>" class="form-control number" placeholder="">
                                                        </div>
												 
                                                </div>
												
												
												<div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label>Email ID:</label>
                                                            <input type="text" id="email_id" name="email_id" value="<?= !empty($clientd->email_id) ? $clientd->email_id : ''; ?>" class="form-control" placeholder="">
                                                        </div> 
												 <div class=" mb-5"> </div>
												 <?php 
													if(!empty($check_p->is_inserted)):
												 ?>
												 <button type="submit" id="btnAddClient" name="btnAddClient" class="btn btn-primary"><?= !empty($clientd->client_slug) ? 'Update' : 'Submit'; ?></button>
												<?php 
															endif;
															?>
                                                </div>
												
												
                                            </div>
											</form> 
														
												 
                                                </div>
												
												
												
                                            </div>
											
											
											
											
											
											
											
											
											
											
											
											
                                            
                                                
                                                
                                            
                                            
                                            
                                            
                                            
                                                    
                                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>