  <?php 
  $check_p  = check_permisson('13');
  ?>	 
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container page">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                   
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php 
											$flash_data =  $this->session->flashdata('consignment_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="javascript:;" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> Client has been add successful.
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('consignment_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="javascript:;" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                </div>
                                <!-- [ form-element ] start -->
                                <div class="col-sm-1"> </div><div class="col-sm-10">
								 <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
								
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Bilty Details</h5>
                                        </div>
										<form action="" id="frmConinement" name="frmConinement" method="POST" />
										 <div class="card-body"> 
										 <div class="row">
											<div class="col-xl-2 col-md-2 mb-3">
											<label for="exampleInputEmail1">Choose Date</label>
											<input type="text" class="form-control datepicker" id="d_week" value="<?= !empty($consignmentd->bilty_date) ? dateFormatedmYs($consignmentd->bilty_date) : date('d/m/Y');?>"></div>
											<div class="col-xl-2 col-md-2 mb-3">
											<label for="exampleInputEmail1">Select Truck</label>
                                                    <select class="form-control" id="truck" name="truck">
                                                        <option value="">Select Truck</option>
														<?php 
														$where_truck = array(
															'view_status' => '1',
															'status' => '1'
														);
														$fetch_truck = $this->fleetmanagement_model->viewRecordAnyR($where_truck);
														if(count($fetch_truck) > 0): 
														foreach($fetch_truck as $fetch_congnee1):
														 
														  if($consignmentd->truck_no == $fetch_congnee1->id){
															$sel = "selected";
														}else{
															$sel =  "";
														}  
														?>
                                                            <option value="<?= $fetch_congnee1->id; ?>" <?= $sel; ?>><?= $fetch_congnee1->truck_no; ?></option>
                                                       <?php endforeach; ?>
													   <?php 
													   else:
													   ?>
													   <option value="">Record not found!</option>
                                                       <?php endif; ?>
                                                    </select>
											</div>
											<?php 
											$fetch_acc_details = $this->login_model->find_by_row($this->acc_id);
											?>
											<div class="col-xl-2 col-md-2 mb-3">
											<label for="exampleInputEmail1">From</label>
											<input type="text" class="form-control" value="<?= $this->zone_model->find_by_name($fetch_acc_details->user_zone) ?>" disabled>
											<input type="hidden" name="zone_from" id="zone_from" class="form-control" value="<?= $fetch_acc_details->user_zone; ?>">
											</div>
											<div class="col-xl-2 col-md-2 mb-3">
											<label for="exampleInputEmail1">Select To</label>
                                                    <select class="form-control" id="zone_to" name="zone_to">
                                                        <option value="">Select To</option>
														<?php 
														$where_zone = array(
															'view_status' => '1',
															'status' => '1',
															'id <>' => $fetch_acc_details->user_zone
														);
														$fetch_zone = $this->zone_model->viewRecordAnyR($where_zone);
														if(count($fetch_zone) > 0): 
														foreach($fetch_zone as $fetch_zone): 
														  if($consignmentd->zone_to == $fetch_zone->id){
															$sel = "selected";
														}else{
															$sel =  "";
														}  
														?>
                                                            <option value="<?= $fetch_zone->id; ?>" <?= $sel; ?>><?= $fetch_zone->zone_name; ?></option>
                                                       <?php endforeach; ?>
													   <?php 
													   else:
													   ?>
													   <option value="">Record not found!</option>
                                                       <?php endif; ?>
                                                    </select>
											</div>
											</div>
                                       
                                            <div class="row"> 
												<div class="col-xl-2 col-md-2 mb-3">
													<label for="exampleInputEmail1">Consignee</label>
                                                    <select class="form-control" id="consignee" name="consignee">
                                                        <option value="">Select Consignee</option>
														<?php 
														$where = array(
															'view_status' => '1',
															'cust_status' => '1',
															'acc_id' => $this->acc_id 
														);
														$fetch_client = $this->client_model->viewRecordAnyR($where);
														if(count($fetch_client) > 0): 
														foreach($fetch_client as $fetch_congnee):
														 if($consignmentd->consignee_name == $fetch_congnee->id){
															$sel = "selected";
														}else{
															$sel =  "";
														}
														?>
                                                            <option value="<?= $fetch_congnee->id; ?>" <?= $sel; ?>><?= $fetch_congnee->company_name; ?></option>
                                                       <?php endforeach; ?>
													   <?php 
													   else:
													   ?>
													   <option value="">Record not found!</option>
                                                       <?php endif; ?>
                                                    </select>
                                                </div>
											
                                                
                                                <div class="col-xl-2 col-md-2 mb-3">
                                                   
                                                        <div class="form-group">
                                                            <label>Consignee Details</label>
                                                            <input type="text" class="form-control" value="<?= !empty($consignmentd->consignee_details) ? $consignmentd->consignee_details : ''; ?>" placeholder="" id="consignee_details" name="consignee_details">
                                                        </div>
                                                       
                                                   
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-3">
                                                   
												
                                                        <div class="form-group">
                                                            <label>GST Number</label>
                                                            <input type="text" value="<?= !empty($consignmentd->consignee_gst) ? $consignmentd->consignee_gst : ''; ?>" class="form-control" placeholder="" id="consignee_gst" name="consignee_gst">
                                                        </div>
												
                                                </div>
                                            
											
											<div class="col-xl-2 col-md-2 mb-5">
                                                    
													<label for="exampleInputEmail1">Consigner</label>
                                                     <select class="form-control" id="consigner">
                                                        <option value="">Select Consigner</option>
														<?php 
														$where = array(
																	'view_status' => '1',
																	'cust_status' => '1',
																	'acc_id' => $this->acc_id 
																);
														$fetch_client = $this->client_model->viewRecordAnyR($where);
														if(count($fetch_client) > 0): 
														foreach($fetch_client as $fetch_congnee):
														if($consignmentd->consigner == $fetch_congnee->id){
															$sel = "selected";
														}else{
															$sel =  "";
														}
														?>
                                                            <option value="<?= $fetch_congnee->id; ?>" <?= $sel; ?>><?= $fetch_congnee->company_name; ?></option>
                                                       <?php endforeach; ?>
													   <?php 
													   else:
													   ?>
													   <option value="">There is no Consigner!</option>
                                                       <?php endif; ?>
                                                    </select>
                                                </div> 
                                                <div class="col-xl-2 col-md-2 mb-3">
                                                   
                                                        <div class="form-group">
                                                            <label>Consigner Details</label>
                                                            <input type="text" class="form-control"  value="<?= !empty($consignmentd->consigner_details) ? $consignmentd->consigner_details : ''; ?>" placeholder="" id="consigner_details" name="consigner_details">
                                                        </div>
                                                       
                                                   
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-3"> 
                                                        <div class="form-group">
                                                            <label>GST Number</label>
                                                            <input type="text" class="form-control" value="<?= !empty($consignmentd->consigner_gst) ? $consignmentd->consigner_gst : ''; ?>" placeholder="" id="consigner_gst" name="consigner_gst">
                                                            <input type="hidden" value="<?= !empty($consignmentd->consignment_slug) ? $consignmentd->consignment_slug : ''; ?>" id="consignment_slug" name="consignment_slug">
                                                        </div>  
												 <div class=" mb-5"> </div>
												<!-- <button type="submit" class="btn btn-primary">Add New Consignee</button>
												<button type="submit" class="btn btn-primary">Add New Consigner</button> -->
                                                </div>
												
                                            </div> 
											
                                           <!--  <h5 class="mt-1">GOODS</h5>
                                            <hr> -->
                                            
                                          <?php 
											$find_url_slug = $this->uri->segment(4);
											if(!empty($find_url_slug)){
											$fetch_array= array(
												'consignment_id' => $consignmentd->consignment_id
											);
											
										$fetch_googds = $this->consignmentgoods_model->viewRecordAnyR($fetch_array);
										if(count($fetch_googds) > 0){
										$i = 1;
										foreach($fetch_googds as $fet_good):
											?>
                                            <div class="row">
											
											<div class="col-xl-2 col-md-2 mb-5">
                                                    
													<label for="exampleInputEmail1">Goods<?= $i++; ?></label>
                                                    <select class="form-control goods_name" name="goods_name[]" disabled>
                                                            <?php 
															$where = array(
																'view_status' => '1',
																'p_status' => '1'
															);
															$goods = $this->product_model->viewRecordAnyR($where);
															foreach($goods as $good):
															if($fet_good->goods_id == $good->id){
																$sel = "selected";
															}else{
																$sel = "";
															}
																
															?>
															<option value="<?= $good->id; ?>" <?= $sel; ?>><?= $good->p_name; ?></option>
															<?php 
															endforeach;
															?>
                                                    </select>
                                                </div>
											
                                                
                                                <div class="col-xl-2 col-md-2 mb-5">
                                                   
                                                        <div class="form-group">
                                                            <label>Nug(s)</label>
                                                            <input type="text" value="<?= !empty($fet_good->order_nugs) ? $fet_good->order_nugs : ''; ?>" class="form-control nugs number" name="nugs[]" placeholder="" disabled>
                                                        </div>
                                                       
                                                   
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-5">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Charged/Nug</label>
                                                            <input type="text" value="<?= !empty($fet_good->order_nug_charge) ? $fet_good->order_nug_charge : ''; ?>" class="form-control chargednug price" name="chargednug[]" placeholder="" disabled>
                                                        </div>
												
                                                </div>												
												<div class="col-xl-1 col-md-1 mb-3">
                                                        <div class="form-group">
                                                            <label>Weight</label>
                                                            <input type="text" value="<?= !empty($fet_good->order_nug_weight) ? $fet_good->order_nug_weight : ''; ?>"  class="form-control weightnug number" name="weightnug[]" placeholder="" disabled>
                                                        </div>
                                                </div> 
												
												<div class="col-xl-2 col-md-2 mb-5">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Total</label>
															<input class="form-control totalnugprice1" type="text" value="" disabled /> 
                                                        </div>
												
												 
												 
                                                </div>
												<div class="col-xl-1 col-md-1 mb-2">
                                                   
												
                                                        <div class="form-group">
                                                          <a title="Delete Goods" href="javascript:;" data-url="consignment/goods_delete" data-deleteid="<?= !empty($fet_good->goods_order_id) ? $fet_good->goods_order_id : ''; ?>" class="btn btn-danger btn-sm deletes"><i class="feather icon-trash-2"></i></a>
															 
                                                        </div>
												
												 
												 
                                                </div>
												
												
                                            </div>
											
											
											<?php 
											endforeach;
											?>
											<div class="row">
											
											<div class="col-xl-4 col-md-6 mb-5">
                                                    
													<label for="exampleInputEmail1">Goods</label>
                                                    <select class="form-control goods_name" name="goods_name" id="goods_name">
													<option value="">Select Goods</option>
                                                            <?php 
															$where = array(
																'view_status' => '1',
																'p_status' => '1'
															);
															$goods = $this->product_model->viewRecordAnyR($where);
															foreach($goods as $good):
															?>
															<option value="<?= $good->id; ?>"><?= $good->p_name; ?></option>
															<?php 
															endforeach;
															?>
                                                    </select>
                                                </div>
											
                                                
                                                <div class="col-xl-2 col-md-2 mb-5">
                                                   
                                                        <div class="form-group">
                                                            <label>Nug(s)</label>
                                                            <input type="text" class="form-control nugs number" name="nugs" placeholder="" >
                                                        </div>
                                                       
                                                   
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-5">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Charged/Nug</label>
                                                            <input type="text" class="form-control chargednug price" name="chargednug" placeholder="">
                                                        </div>
												
                                                </div>												
												<div class="col-xl-2 col-md-2 mb-5">
                                                        <div class="form-group">
                                                            <label>Weight</label>
                                                            <input type="text" class="form-control weightnug number" name="weightnug" placeholder="">
                                                        </div>
                                                </div>
												
												 
												
											 
												
												<div class="col-xl-2 col-md-2 mb-5">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Total</label>
															<input class="form-control totalnugprice1" type="text" value="0.00" /> 
                                                        </div>
												
												 <div class=" mb-5"> </div>
												 <button type="button" data-consigmentd="<?= $consignmentd->consignment_id; ?>" class="btn btn-primary addmoregoods">Add More Goods</button>
                                                </div>
												
												
                                            </div>
											<?php
										} else{
											?>
											<div class="row">
											
											<div class="col-xl-4 col-md-6 mb-5">
                                                    
													<label for="exampleInputEmail1">Goods</label>
                                                    <select class="form-control goods_name" name="goods_name">
                                                            <?php 
															$where = array(
																'view_status' => '1',
																'p_status' => '1'
															);
															$goods = $this->product_model->viewRecordAnyR($where);
															foreach($goods as $good):
															?>
															<option value="<?= $good->id; ?>"><?= $good->p_name; ?></option>
															<?php 
															endforeach;
															?>
                                                    </select>
                                                </div>
											
                                                
                                                <div class="col-xl-2 col-md-2 mb-5">
                                                   
                                                        <div class="form-group">
                                                            <label>Nug(s)</label>
                                                            <input type="text" class="form-control nugs number" name="nugs" placeholder="" >
                                                        </div>
                                                       
                                                   
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-5">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Charged/Nug</label>
                                                            <input type="text" class="form-control chargednug price" name="chargednug" placeholder="">
                                                        </div>
												
                                                </div>												
												<div class="col-xl-2 col-md-2 mb-5">
                                                        <div class="form-group">
                                                            <label>Weight</label>
                                                            <input type="text" class="form-control weightnug number" name="weightnug" placeholder="">
                                                        </div>
                                                </div>
												
												 
												
											 
												
												<div class="col-xl-2 col-md-2 mb-5">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Total</label>
															<input class="form-control totalnugprice1" type="text" value="0.00" /> 
                                                        </div>
												
												 <div class=" mb-5"> </div>
												 <button type="button" data-consigmentd="<?=!empty( $consignmentd->consignment_id) ?  $consignmentd->consignment_id : ''; ?>" class="btn btn-primary addmoregoods">Add More Goods</button>
                                                </div>
												
												
                                            </div>
											<?php
											
										}
										}else{
											?>
											<div id="insertbefore"></div>
											 <div class="row" id="ssss">
											
											<div class="col-xl-4 col-md-6 mb-5">
                                                    
													<label for="exampleInputEmail1">Goods</label>
                                                    <select class="form-control goods_name" name="goods_name[]">
                                                            <?php 
															$where = array(
																'view_status' => '1',
																'p_status' => '1'
															);
															$goods = $this->product_model->viewRecordAnyR($where);
															foreach($goods as $good):
															?>
															<option value="<?= $good->id; ?>"><?= $good->p_name; ?></option>
															<?php 
															endforeach;
															?>
                                                    </select>
                                                </div>
											
                                                
                                                <div class="col-xl-2 col-md-2 mb-5">
                                                   
                                                        <div class="form-group">
                                                            <label>Nug(s)</label>
                                                            <input type="text" class="form-control nugs number" name="nugs[]" placeholder="" >
                                                        </div>
                                                       
                                                   
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-5">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Charged/Nug</label>
                                                            <input type="text" class="form-control chargednug price" name="chargednug[]" placeholder="">
                                                        </div>
												
                                                </div>												
												<div class="col-xl-2 col-md-2 mb-5">
                                                        <div class="form-group">
                                                            <label>Weight</label>
                                                            <input type="text" class="form-control weightnug number" name="weightnug[]" placeholder="">
                                                        </div>
                                                </div>
												
												<div class="col-xl-2 col-md-2 mb-5">
                                                        <div class="form-group">
                                                            <label>Total</label>
                                                             <input class="form-control totalnugprice1" type="text" value="0.00" /> 
                                                        </div>
												
												 
                                                </div>
												
												
                                            </div>
											
											
											
											<div class="row">
											
											<div class="col-xl-4 col-md-6 mb-5">
                                                    
													<label for="exampleInputEmail1">Goods</label>
                                                    <select class="form-control goods_name" name="goods_name[]">
                                                            <?php 
															$where = array(
																'view_status' => '1',
																'p_status' => '1'
															);
															$goods = $this->product_model->viewRecordAnyR($where);
															foreach($goods as $good):
															?>
															<option value="<?= $good->id; ?>"><?= $good->p_name; ?></option>
															<?php 
															endforeach;
															?>
                                                    </select>
                                                </div>
											
                                                
                                                <div class="col-xl-2 col-md-2 mb-5">
                                                   
                                                        <div class="form-group">
                                                            <label>Nug(s)</label>
                                                            <input type="text" class="form-control nugs number" name="nugs[]" placeholder="" >
                                                        </div>
                                                       
                                                   
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-5">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Charged/Nug</label>
                                                            <input type="text" class="form-control chargednug price" name="chargednug[]" placeholder="">
                                                        </div>
												
                                                </div>												
												<div class="col-xl-2 col-md-2 mb-5">
                                                        <div class="form-group">
                                                            <label>Weight</label>
                                                            <input type="text" class="form-control weightnug number" name="weightnug[]" placeholder="">
                                                        </div>
                                                </div> 
												<div class="col-xl-2 col-md-2 mb-5">  
                                                        <div class="form-group">
                                                            <label>Total</label>
															<input class="form-control totalnugprice1" type="text" value="0.00" /> 
                                                        </div>
												
												 <div class="mb-5" id="addmoreafter"> </div>
												 <button type="button" class="btn btn-primary" id="addMores">More + </button>
                                                </div>
												
												
                                            </div>
										<?php } ?>
											<!-- <h5 class="mt-1">Charges</h5>
                                            <hr>-->
											 <div class="row">
											
											<div class="col-xl-2 col-md-2 mb-3">
												<div class="form-group">
													<label>Freight Charges</label>
													<input type="text" class="form-control price" name="freight" id="freight" value="<?= !empty($consignmentd->freight) ? $consignmentd->freight : ''; ?>" placeholder="Freight Charges">
												</div> 
                                            </div>
											
                                                
                                                <div class="col-xl-2 col-md-2 mb-3">
                                                   
                                                        <div class="form-group">
                                                            <label>Bilty Charges</label>
                                                            <input type="text"  name="bilty" id="bilty" value="<?= !empty($consignmentd->bilty) ? $consignmentd->bilty : ''; ?>" class="form-control price" placeholder="">
                                                        </div>
                                                       
                                                   
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-3">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Narration Charges</label>
                                                            <input type="text" name="narrationcrg" value="<?= !empty($consignmentd->narrationcrg) ? $consignmentd->narrationcrg : ''; ?>" id="narrationcrg" class="form-control price" placeholder="">
                                                        </div>
												
                                                </div>
												
												<div class="col-xl-2 col-md-2 mb-3">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Other Charges </label>
                                                            <input type="text" name="narrationother" value="<?= !empty($consignmentd->narrationother) ? $consignmentd->narrationother : ''; ?>" id="narrationother" class="form-control price" placeholder="">
                                                        </div>
												
                                                </div>
												
												<div class="col-xl-2 col-md-2 mb-3">
                                                   
                                                        <div class="form-group">
                                                            <label>Party Invoice</label>
                                                            <input type="text" name="invoice_no" value="<?= !empty($consignmentd->invoice_no) ? $consignmentd->invoice_no : ''; ?>" id="invoice_no" class="form-control" placeholder="">
                                                        </div>
                                                       
                                                   
                                                </div>
												
												
												<div class="col-xl-2 col-md-2 mb-3">
                                                    <div class="form-group">
                                                            <label>eWay Bill Number</label>
                                                            <input type="text" value="<?= !empty($consignmentd->form_number) ? $consignmentd->form_number : ''; ?>" name="form_number" id="form_number" class="form-control" placeholder="">
                                                        </div>
                                                </div>
												
                                            </div>
											
											
											
											<h5 class="mt-1">Invoice</h5>
                                            <hr>
											 <div class="row">
											
											<div class="col-xl-3 col-md-3 mb-3">
                                                   
                                                        <div class="form-group">
                                                            <label>Grand Total</label> 
                                                            <input type="text" class="form-control" value="<?= !empty($consignmentd->grandtotal) ? $consignmentd->grandtotal : ''; ?>" name="grandtotal" id="grandtotal" placeholder="">
                                                        </div> 
                                                </div>
											
                                                
                                                
												<div class="col-xl-3 col-md-4 mb-3">
                                                   
												
                                                        <div class="form-group">
                                                            <label>Payment </label>
                                                        <select class="form-control" name="paid_by" id="paid_by"> 
															<option value="1">To Pay</option>
                                                            <option value="2">Paid</option>
														</select>
                                                        </div>
                                                </div>
												<div class="col-xl-3 col-md-3 mb-3 gstclass">
                                                   
												
                                                        <div class="form-group">
                                                            <label>GST Paid By</label> 
                                                            <select class="form-control" name="gst_paid" id="gst_paid">
																<option value="">Select paid by gst</option>
																 
															</select>
                                                        </div>
												
                                                </div>
												
												 <div class="col-xl-3 col-md-4 mb-3 gstclass">
                                                   
												
                                                        <div class="form-group">
                                                            <label>GST Rate</label>
                                                            <select class="form-control" id="gst_percent" name="gst_percent">
																<?php 
																$iss = array(
																	'view_status' => '1'
																);
																$gst_fetc = $this->gst_model->viewRecordAnyR($iss);
																foreach($gst_fetc as $fgst):
																?>
																<option value="<?= $fgst->gst_rate; ?>"><?= $fgst->gst_rate; ?>%</option>
																<?php 
																endforeach;
																?>
															</select>
                                                        </div> 
                                                </div>  
												<div id="gstid" class="col-xl-6 col-md-6 mb-6">
												
												</div>
												 
												<div class="col-xl-3 col-md-3 mb-3">
                                                    <div class="form-group">
                                                            <label>Net Total </label>
                                                            <input type="text" name="nettotal" id="nettotal" class="form-control" placeholder="">
                                                        </div>
                                                </div>
												
												
												
												
												<div class="col-xl-3 col-md-3 mb-3">
                                                    <div class="form-group">
                                                            <label>Value Rs </label>
                                                            <input type="text" value="<?= !empty($consignmentd->rs_value) ? $consignmentd->rs_value : ''; ?>" name="rs_value" id="rs_value" class="form-control price" placeholder="">
                                                        </div>
                                                </div> 
												 
												<div class="col-xl-3 col-md-4 mb-3">
												<div class="form-group">
                                                            <label>Payment By</label>
                                                            <select class="form-control" name="payment_type" id="payment_type" style="display:none">
															<?php 
															if(!empty($consignmentd->payment_type)){
															$arr_payment_type = array(
																'1' => 'Cash',
																'2' => 'Cheque'
															);
															if($consignmentd->payment_type == '1'):
															?>
																 <option value="<?= $consignmentd->payment_type; ?>" selected><?= $arr_payment_type[$consignmentd->payment_type]; ?></option>
															<?php
															else:
															?>
															<option value="<?= $consignmentd->payment_type; ?>" selected><?= $arr_payment_type[$consignmentd->payment_type]; ?></option>
															<?php
															endif;
															}
															?>
																 <option value="">Select Payment Mode</option>
																 <option value="1">Cash</option>
																 <option value="2">Cheque</option>
															</select>
															<?php  
															$userid = $this->login_model->find_by_user($this->acc_id);
															?>
															<input type="text" value="<?= !empty($userid) ? $userid : ''; ?>" name="payment_type" class="form-control" style="display:none" disabled>
															<input type="hidden" value="<?= !empty($consignmentd->payment_type) ? $consignmentd->payment_type : ''; ?>" name="payment_type" id="payment_type" class="form-control" placeholder="" style="display:none">
                                                        </div>
                                                </div>
                                            </div>
											<?php 
												if(!empty($check_p->is_inserted)):
											?>											
											<button class="btn btn-primary" type="button" name="btnSubmitCongsiment" id="<?= !empty($find_url_slug) ? 'btnSubmitCongsiment' : 'btnSubmitCongsiment'?>">Submit form</button>
											<?php endif;?>
										</form>
										</div>
									</div>
                                        </div><div class="col-sm-1"></div>
                                    </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div> 
