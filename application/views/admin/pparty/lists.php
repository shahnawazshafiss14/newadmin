  <?php 
  $check_p  = check_permisson('14');
  ?>
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                   
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                               
                                <!-- [ form-element ] start -->
                                <div class="col-sm-1"></div> <div class="col-sm-10">
								 <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                                    <div class="card"> 
                                        <div class="card-body">
                                            
                                             <!-- [ configuration table ] start -->
                               
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
														<th>Sr.</th>
                                                            <th>Bilty No.</th>
                                                            <th>Bilty Date</th>
                                                            
                                                            <th>Consignee</th>
                                                            <th>Consigner</th> 
															<th>Quantity</th>
                                                            <th>Payment</th> 															
                                                            <th>Challan No.</th> 
															<th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php  
													$i = 1;
													foreach($consignments as $congi):  
													?>
                                                        <tr>
														<td><?= $i++;?></td>
                                                            <td><?= amb($congi->consignment_id); ?></td>
                                                            <td><?= $congi->bilty_date; ?></td>
                                                            <!-- <td><?= $congi->created_date; ?></td> -->
                                                            <td><?= $this->client_model->find_by_name($congi->consignee_name); ?></td>
                                                            <td><?= $this->client_model->find_by_name($congi->consigner); ?></td> 
															<td><?= consignment_good($congi->consignment_id, 'nug'); ?> Nags</td>
															<td><?= paid_by($congi->paid_by); ?> (<?= $congi->nettotal; ?>rs)</td>
                                                            <td><?= !empty($congi->challan_no) ? $congi->challan_no : ''?></td> 
															<td>
															<?php 
															if(!empty($check_p->is_viewed)):
															?>
															<a title="Print/view" href="<?= base_url(ADMIN_URL.'/consignment/details/'.$congi->consignment_slug)?>" class="btn btn-dark btn-sm"><i class="feather icon-eye"></i></a> 
															
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
															 <a title="Edit Details" href="<?= base_url(ADMIN_URL.'/consignment/add/'.$congi->consignment_slug)?>" class="btn btn-dark btn-sm"><i class="feather icon-edit"></i></a>
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_deleted)):
															?>
															| <a title="Delete Consignment" data-url="consignment/deletecong" data-deleteid="<?= $congi->consignment_id; ?>" href="javascript:;" class="btn btn-danger btn-sm deletes"><i class="feather icon-trash-2"></i></a>
															<?php 
															endif;
															?>
															 </td>
                                                        </tr>
                                                     <?php 
													 endforeach;
													 ?>  
                                                    </tbody>
                                                   <!-- <tfoot>
                                                        <tr>
															<th>Sr.</th>
                                                            <th>Bilty No.</th>
                                                            <th>Bilty Date</th>
                                                            <th>Create Date</th>
                                                            <th>Consignee</th>
                                                            <th>Consigner</th> 
                                                            <th>Status</th> 
															<th>Action</th>
                                                        </tr>
                                                    </tfoot>-->
                                                </table>
                                            </div>
                                        </div>
                               
                                            </div> 
												 
                                                </div> 
                                            </div>    <div class="col-sm-1"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                  