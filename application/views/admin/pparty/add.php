  <?php 
  $check_p  = check_permisson('37');
  ?>	 
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container page">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                   
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php 
											$flash_data =  $this->session->flashdata('consignment_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="javascript:;" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> Client has been add successful.
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('consignment_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="javascript:;" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                </div>
                                <!-- [ form-element ] start -->
                                <div class="col-sm-1"> </div><div class="col-sm-10">
								 <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
								
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Bilty Details</h5>
                                        </div>
										<form action="" id="frmConinement" name="frmConinement" method="POST" />
										 <div class="card-body"> 
                                            <div class="row"> 
												<div class="col-xl-2 col-md-2 mb-3">
													<label for="exampleInputEmail1">Purshased party name</label>
                                                    <input type="text" value="<?= !empty($consignmentd->p_party_name) ? $consignmentd->p_party_name : ''; ?>" class="form-control" placeholder="" id="p_party_name" name="p_party_name">
                                                </div> 
                                                <div class="col-xl-2 col-md-2 mb-3"> 
                                                        <div class="form-group">
                                                            <label>Party Address</label>
                                                            <textarea class="form-control" placeholder="" id="p_party_address" name="p_party_address"><?= !empty($consignmentd->p_party_address) ? $consignmentd->p_party_address : ''; ?></textarea>
                                                        </div> 
                                                </div>
												 <div class="col-xl-2 col-md-2 mb-3"> 
                                                        <div class="form-group">
                                                            <label>GST Number</label>
                                                            <input type="text" value="<?= !empty($consignmentd->consignee_gst) ? $consignmentd->consignee_gst : ''; ?>" class="form-control" placeholder="" id="consignee_gst" name="consignee_gst">
                                                        </div> 
                                                </div>  
                                            </div>
											<input type="hidden" value="<?= !empty($consignmentd->consignment_slug) ? $consignmentd->consignment_slug : ''; ?>" id="consignment_slug" name="consignment_slug">
											
                                         
                                            
											<?php
										 
												if(!empty($check_p->is_inserted)):
											?>											
											<button class="btn btn-primary" type="button" name="btnSubmitCongsiment" id="<?= !empty($find_url_slug) ? 'btnSubmitCongsiment' : 'btnSubmitCongsiment'?>">Submit form</button>
											<?php endif;?>
										</form>
										</div>
									</div>
                                        </div><div class="col-sm-1"></div>
                                    </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div> 
