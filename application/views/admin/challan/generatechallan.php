  <?php 
  $check_p  = check_permisson('38');
  ?>
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                               
                                <!-- [ form-element ] start -->
                                <div class="col-sm-12">
                                    <div class="card"> 
                                        <div class="card-body">
                                            
                                             <!-- [ configuration table ] start -->
                               
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
														<th>Sr.</th>
                                                            <th>Zone From</th>
                                                            <th>Challan Number</th>
                                                            <th>Vehicle</th>
                                                            <th>Generate Date/Time</th>
                                                            <th>Consignment No</th>
															<th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php  
													$i = 1;
													foreach($consignments as $congi):  
													?>
                                                        <tr>
														<td><?= $i++;?></td>
														<td><?= $this->zone_model->find_by_name($congi->c_zone_from); ?></td> 
                                                            <td><?= $congi->challan_no; ?></td>
                                                            <td><?= $congi->truck_no; ?></td>
                                                            
                                                            <td><?= $congi->created_date; ?></td>
                                                            <td><?= challan_add($congi->consignee_ids); ?></td>
															<td> 
															<?php 
															if(!empty($check_p->is_viewed)):
															?>
															<a title="Print/view" href="<?= base_url(ADMIN_URL.'/challan/invoice/'.$congi->challan_no); ?>" class="label theme-bg2 text-white f-12 btn-sm"><i class="feather icon-eye"></i></a> 
															<?php endif;?> 
															<?php 
															if(!empty($check_p->is_edited)):
															if($congi->status == 1):
															?>
															 <a title="Update Status" id="btnchallanUpdate" data-challanno="<?= $congi->challan_no; ?>" href="javascript:;" class="label theme-bg text-white f-12 btn-sm btnchallanUpdate"><i class="feather icon-edit"></i></a> 
															<?php endif;?>
															<?php endif;?>
															</td>
                                                        </tr>
                                                     <?php 
													 endforeach;
													 ?>  
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
															<th>Sr.</th>
                                                            <th>Zone From</th>
                                                            <th>Challan Number</th>
															<th>Vehicle</th>
                                                            <th>Generate Date/Time</th>
                                                            <th>Consignment No</th>
															<th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                               
                                            </div> 
												 
                                                </div> 
                                            </div>   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                  