  <?php 
  $check_p  = check_permisson('37');
  ?>	 
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<div class="container">
<div class="row">
                <style>
                    .tblPass {
                        background-color: white;
                        font-size: 14px;
                        width: 100%;
                    }

                        .tblPass th, td {
                            padding: 10px;
                        }

                    .pagebreak {
                        page-break-before: always;
                    }
                </style>
                <table style="border-collapse: collapse" border="1" class="tblPass" cellspacing="0">
                    <tbody>
					<tr>
                        <td colspan="2">
                            <strong>Challan No: </strong>
							<?= $consignmentd->challan_no;?>
                        </td> 
						<td colspan="2">
                            <strong><th>Vehicle</th>: </strong>
							<?= $consignmentd->truck_no;?>
                        </td> 
                        <td colspan="3">
                            <strong>Date: </strong>
                             <?= $consignmentd->created_date;?>
                        </td>
                    </tr>
					<tr>
                        <td colspan="7" style="text-align: center">
							<?php 
							$acc_details = $this->login_model->find_by_row($this->acc_id);
							?> 
                            <p style="font-family: arial; font-size: 25px; margin-top: -2px !important"><b><?= $acc_details->user_company; ?></b></p>
                            <!--p style="font-family: arial; font-size: 16px; margin-top: -26px !important">FLEET OWNERS &amp; TRANSPORT CONTRACTORS</p-->
                            <p style="font-weight: normal; font-size: 15px; margin-top: -12px !important; text-align: center; margin-left: 8px;">
                                <strong>Office : </strong> <?= $acc_details->user_address; ?>
                            </p>

                            <p style="font-weight: normal; font-size: 15px; margin-top: -10px !important; text-align: center; margin-left: 8px;">
                                <strong>Contact No. :</strong> <?= $acc_details->user_mobile; ?>
                            </p> 
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <strong>From: </strong>
							<?= $this->zone_model->find_by_name($consignmentd->c_zone_from); ?> 
                        </td> 
                        <td colspan="3">
                            <strong>To: </strong>
                            <?= $this->zone_model->find_by_name($consignmentd->c_zone_to); ?> 
                        </td>
                    </tr>
					 <tr>
                        <td>From</td> 
                        <td>To</td>
                        <td>Billty No.</td>
                        <td>Weight</td>
                        <td>Payment</td> 
                        <td>Nag</td>
                        <td>Total</td>
                    </tr>
					<?php 
					$arr_consig = array(
						'challan_no' => $consignmentd->challan_no
					);
					$fetch_consig = $this->consignment_model->viewRecordAnyR($arr_consig);
					foreach($fetch_consig as $consig):					
					?>
					<tr>
                        <td><?= $consig->consignee_details;?></td> 
                        <td><?= $consig->consigner_details;?></td>
                        <td><?= 'AMB'.$consig->consignment_id;?></td>
                        <td><?php 
							$weight = consignment_good($consig->consignment_id, 'weight');
							echo !empty($weight) ? $weight : 0;
							?></td>
                        <td><?= paid_by($consig->paid_by);?></td>
                        <td><?php 
							$nug = consignment_good($consig->consignment_id, 'nug');
							echo !empty($nug) ? $nug : 0;
							?>
						</td>
                        <td><?php 
						if($consig->paid_by == 1){
							echo $consig->nettotal;
						}else{
							echo 'Paid';
						}
						?></td>
                    </tr>
					<?php 
					endforeach;
					?>
				</body>
			</table>
		</div>
		</div>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>