  <?php 
  $check_p  = check_permisson('36');
  ?>
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                               
                                <!-- [ form-element ] start -->
                                <div class="col-sm-12">
                                    <div class="card"> 
                                        <div class="card-body">
                                             <!-- [ configuration table ] start -->
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
														<th>Sr.</th>
                                                            <th>Consignment Number</th>
                                                            <th>Bilty Date</th>
                                                            <th>Create Date</th>
                                                            <th>Consignee</th>
                                                            <th>Consigner</th> 
                                                            <th>Status</th> 
															<th>Action&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" name="btnCreateChallan" class="btn btn-primary btnCreateChallan" value="Create" />
															<select name="c_challan_zone" id="c_challan_zone" class="form-control">
															<option value="">Select Zone</option>
															<?php 
															$array_fet = array(
																'view_status' => '1',
																'status' => '1',
																'id <>' => $this->acc_zone
															);
															$fetch_zone = $this->zone_model->viewRecordAnyR($array_fet);
															foreach($fetch_zone as $zone):
															?>
																<option value="<?= $zone->id; ?>"><?= $zone->zone_name; ?></option>
															<?php 
															endforeach;
															?>
															</select>
															
															
															</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php  
													$i = 1;
													foreach($consignments as $congi):  
													?>
                                                        <tr>
														<td><?= $i++;?></td>
                                                            <td>AMB<?= $congi->consignment_id; ?></td>
                                                            <td><?= $congi->bilty_date; ?></td>
                                                            <td><?= $congi->created_date; ?></td>
                                                            <td><?= $this->client_model->find_by_name($congi->consignee_name); ?></td>
                                                            <td><?= $this->client_model->find_by_name($congi->consigner); ?></td> 
                                                            <td><label class="label label-<?= $congi->challan_status == 0 ? 'warning' : 'success' ; ?>"><?= challan_status($congi->challan_status); ?></label></td> 
															<td>
															<?php 
															if(!empty($check_p->is_viewed)):
															?>
															<a title="Print/view" href="<?= base_url(ADMIN_URL.'/consignment/details/'.$congi->consignment_slug)?>" class="label theme-bg2 text-white f-12 btn-sm"><i class="feather icon-eye"></i></a> 
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
															 | <input type="checkbox" name="createChallan[]"  class="label theme-bg2 text-white f-12 btn-sm checkChallan" value="<?= $congi->consignment_id; ?>" />
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_deleted)):
															?>
															| <a title="Delete Consignment" data-url="consignment/deletecong" data-deleteid="<?= $congi->consignment_id; ?>" href="javascript:;" class="btn btn-danger btn-sm deletes"><i class="feather icon-trash-2"></i></a>
															<?php 
															endif;
															?>
															 </td>
                                                        </tr>
                                                     <?php 
													 endforeach;
													 ?>  
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
															<th>Sr.</th>
                                                            <th>Consignment Number</th> 
                                                            <th>Bilty Date</th>
                                                            <th>Create Date</th>
                                                            <th>Consignee</th>
                                                            <th>Consigner</th>
                                                            <th>Status</th> 
															<th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                               
                                            </div> 
												 
                                                </div> 
                                            </div>   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                  