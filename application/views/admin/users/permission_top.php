  
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                        <div class="page-block">
                            <div class="row align-items-center">
                                <div class="col-md-12">
                                    <div class="page-header-title">
                                        <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                               
                                <!-- [ form-element ] start -->
                                <div class="col-sm-3">
                                    <div class="card"> 
                                        <div class="card-body">
                                            
                                             <!-- [ configuration table ] start -->
                               
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table  class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
												<?php 
													$where = array(
														'view_status' => '1',
														'status' => '1',
														'user_level !=' => '3'
													); 
													$users = $this->login_model->viewRecordAnyR($where);
													foreach($users as $user):
													?>
													<tr>
														<td><a href="<?= U_A_URL; ?>userp/index?accident_id=<?= $user->acc_id; ?>"><?= $user->user_fullname; ?><br />Zone: <?= $this->zone_model->find_by_name($user->user_zone); ?></a></td>
													</tr>
                                                     <?php 
													 endforeach;
													 ?>  
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Name</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                               
                                            </div> 
												 
                                                </div> 
                                            </div>  
<div class="col-sm-9">
                                    <div class="card"> 
                                        <div class="card-body">
                                            
                                             <!-- [ configuration table ] start -->
                               
                                        <div class="card-block">
										<?php 
										if(isset($_GET['accident_id'])):
										$accident_id = $_GET['accident_id'];
										?>
                                            <div class="table-responsive">
                                                <table  class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Menu Name</th>
                                                            <th>Permission</th>
                                                            <th>viewed</th>
                                                            <th>inserted</th>
                                                            <th>edited</th>
                                                            <th>deleted</th>
                                                            <th>print</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
												<?php 
												
													$where = array(
														'view_status' => '1',
														'status' => '1',
														'menu_parent' => '0'
													);
													$menus = $this->topmenu_model->viewRecordAnyR($where);
													foreach($menus as $menu):
													$arr_fetch = array(
														'acc_id' => $accident_id,
														'top_menu_id' => $menu->menu_id
													);
													$fetch_permission = $this->permison_model->viewRecordAny($arr_fetch);
													
													?>
													<tr>
														<td><?= $menu->menu_name; ?></td>
														
														<td>
														
														<input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkmenpermission" title="<?= $menu->menu_name; ?> -> View" data-id="<?= $menu->menu_id; ?>" <?= !empty($fetch_permission->is_menued) ? 'checked' : ''; ?> /></td>
														
														<td>
														<?php 
														if(!empty($fetch_permission->is_menued)):
														?>
															<input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkedchid" data-types="is_viewed" title="<?= $menu->menu_name; ?> -> View" data-id="<?= $menu->menu_id; ?>" <?= !empty($fetch_permission->is_viewed) ? 'checked' : ''; ?>>
															<?php endif;?>
														</td>
														<td>
														<?php 
														if(!empty($fetch_permission->is_menued)):
														?>
															<input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkedchid" data-types="is_inserted" title="<?= $menu->menu_name; ?> -> Inserted" data-id="<?= $menu->menu_id; ?>" <?= !empty($fetch_permission->is_inserted) ? 'checked' : ''; ?>>
															<?php endif;?>
														</td>
														<td>
															<span class="label label-info"><i title="<?= $menu->menu_name; ?> -> Edit" class="feather icon-edit"></i></span>
														</td>
														<td>
															<span class="label label-danger"><i title="<?= $menu->menu_name; ?> -> Delete" class="feather icon-trash-2"></i></span>
														</td>
														<td>
															<span class="label label-success"><i title="<?= $menu->menu_name; ?> -> Print" class="feather icon-printer"></i></span>
														</td>
													</tr>
                                                     <?php 
													 $where_c = array(
														'view_status' => '1',
														'status' => '1',
														'menu_parent' => $menu->menu_id
													);
													$menus_c = $this->topmenu_model->viewRecordAnyR($where_c);
													foreach($menus_c as $menu_c):
													
													$arr_fetch_c = array(
														'acc_id' => $accident_id,
														'top_menu_id' => $menu_c->menu_id
													);
													$fetch_permission_c = $this->permison_model->viewRecordAny($arr_fetch_c);
													
													?>
													<tr>
													
														<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= $menu_c->menu_name; ?></td>
														<td><input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkmenpermission" title="<?= $menu_c->menu_name; ?> -> View" data-id="<?= $menu_c->menu_id; ?>" <?= !empty($fetch_permission_c->is_menued) ? 'checked' : ''; ?> /></td>
														<td>
														<?php 
														if(!empty($fetch_permission_c->is_menued)):
														?>
															<input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkedchid" data-types="is_viewed" title="<?= $menu_c->menu_name; ?> -> View" data-id="<?= $menu_c->menu_id; ?>" <?= !empty($fetch_permission_c->is_viewed) ? 'checked' : ''; ?>>
															<?php endif;?>
														</td>
														<td>
														<?php 
														if(!empty($fetch_permission_c->is_menued)):
														?>
															<input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkedchid" data-types="is_inserted" title="<?= $menu_c->menu_name; ?> -> Inserted" data-id="<?= $menu_c->menu_id; ?>" <?= !empty($fetch_permission_c->is_inserted) ? 'checked' : ''; ?>>
															<?php endif;?>
														</td> 
														<td>
														<?php 
														if(!empty($fetch_permission_c->is_menued)):
														?>
															<input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkedchid" data-types="is_edited" title="<?= $menu_c->menu_name; ?> -> View" data-id="<?= $menu_c->menu_id; ?>" <?= !empty($fetch_permission_c->is_edited) ? 'checked' : ''; ?>>
															<?php endif;?>
														</td>
														<td>
														<?php 
														if(!empty($fetch_permission_c->is_menued)):
														?>
															<input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkedchid" data-types="is_deleted" title="<?= $menu_c->menu_name; ?> -> View" data-id="<?= $menu_c->menu_id; ?>" <?= !empty($fetch_permission_c->is_deleted) ? 'checked' : ''; ?>>
															<?php endif;?>
														</td>
														<td>
														<?php 
														if(!empty($fetch_permission_c->is_menued)):
														?>
															<input type="checkbox" data-accountid="<?= $accident_id; ?>" class="form-control checkedchid" data-types="is_printed" title="<?= $menu_c->menu_name; ?> -> View" data-id="<?= $menu_c->menu_id; ?>" <?= !empty($fetch_permission_c->is_printed) ? 'checked' : ''; ?>>
															<?php endif;?>
														</td>
													</tr>
                                                     <?php 
													 endforeach;
													 endforeach;
													 ?>  
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Menu Name</th>
                                                            <th>Permission</th>
                                                            <th>viewed</th>
                                                            <th>edited</th>
                                                            <th>deleted</th>
															<th>print</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
											<?php endif;?>
                                        </div>
                               
                                            </div> 
												 
                                                </div> 
                                            </div>  											
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                  