<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title><?=$Page_Title?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/fontawesome/css/fontawesome-all.min.css');?>">
    <!-- animation css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/animation/css/animate.min.css');?>">
   <!-- select2 css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/css/select2.min.css');?>">
    <!-- multi-select css -->
    <!-- vendor css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/alertifyjs/css/alertify.min.css'); ?>"> 
	<link rel="stylesheet" href="<?php echo base_url('assets/alertifyjs/css/themes/default.min.css'); ?>"> 
	<link rel="stylesheet" href="<?php echo base_url('assets/alertifyjs/css/themes/bootstrap.css'); ?>"> 
	 <link rel="stylesheet" href="<?php echo base_url('assets/plugins/modal-window-effects/css/md-modal.css'); ?>"> 
	<link rel="stylesheet" href="<?php echo base_url('assets/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css'); ?>"> 
	<link rel="stylesheet" href="<?php echo base_url('assets/css/slick.css'); ?>"> 
	<link rel="stylesheet" href="<?php echo base_url('assets/css/slick-theme.css');?>">
	<style>
        .datepicker>.datepicker-days {
            display: block;
        }

        ol.linenums {
            margin: 0 0 0 -8px;
        }
    </style>

</head>

<body>
    <!-- [ navigation menu ] start -->
    <nav class="pcoded-navbar menu-light brand-lightblue icon-colored">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="<?php echo base_url(ADMIN_URL.'/dashboard');?>" class="b-brand">
                  <img src="/assets/images/logo.png">
                   <!--span class="b-title">Shop Delhi - Handle By Rajesh</span-->
               </a>
                <a class="mobile-menu" id="mobile-collapse" href="javascript:;"><span></span></a>
            </div>
            <div class="navbar-content scroll-div" id="layout-sidenav">
                <ul class="nav pcoded-inner-navbar sidenav-inner"> 
				<?php 
				$arr_menu = array( 
					'menu_parent' => '0',
					'tbl_permision.acc_id' => $this->acc_id,
					'menu_id <>' => '31'
				);
				$fetch_menu = $this->permison_model->viewRecordIdUserId($arr_menu);
				foreach($fetch_menu as $f_m):
				$arr_fetch = array(
						'acc_id' => $this->acc_id,
						'top_menu_id' => $f_m->menu_id 
					);
				$fetch_permission = $this->permison_model->viewRecordAny($arr_fetch);
				 
				?>
                <li data-username="To-Do notes" class="nav-item pcoded-hasmenu">
                        <a href="javascript:;" class="nav-link"><span class="pcoded-micon"><i class="<?= $f_m->icons; ?>"></i></span> <span class="pcoded-mtext"><?= $f_m->menu_name; ?></span></a>
                       
						<?php 
							$arr_menu = array(
								'menu_parent' => $f_m->menu_id,
								'tbl_permision.acc_id' => $this->acc_id
							);
						$fetch_menu_p = $this->permison_model->viewRecordIdUserId($arr_menu);
						if(count($fetch_menu_p) > 0):
						?>
						 <ul class="pcoded-submenu">
						<?php
						foreach($fetch_menu_p as $f_m_p): 
						?>
                            <li class=""><a href="<?= U_A_URL . $f_m_p->menu_slug; ?>" class=""><span class="pcoded-micon"><i class="<?= $f_m_p->icons; ?>"></i></span> <?= $f_m_p->menu_name; ?></a></li> 
						<?php 
						 
						endforeach;
						?>
                        </ul>
						<?php 
						endif;
						?>
                </li>   
				<?php 
				 
				endforeach;
				?>
				<?php 
				if($this->acc_id == 'TRA01'):
				?>
				<li data-username="To-Do notes" class="nav-item">
				<a href="<?= U_A_URL; ?>userp/index" class="">User Permission</a></li> 
<?php 
endif;
 ?>				
				 
                </ul>
            </div>
        </div>
    </nav>
    <!-- [ navigation menu ] end -->

    <!-- [ Header ] start -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light header-lightblue headerpos-fixed "> 
        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="javascript:;"><span></span></a>
            <a href="<?php echo base_url(ADMIN_URL.'/dashboard');?>" class="b-brand">
                <div class="b-bg">
                    <i class="feather icon-trending-up"></i>
                </div>
                <span class="b-title"><?= $this->zone_model->find_by_name($this->acc_zone); ?> Dashboard</span>
                <!-- <img class="img-fluid horizontal-dasktop" src="<?php echo base_url('assets/images/logo-dark.png');?>" alt="Theme-Logo" />
                <img class="img-fluid horizontal-mobile" src="<?php echo base_url('assets/images/logo.png');?>" alt="Theme-Logo" /> -->
            </a>
        </div>
        <div class="collapse navbar-collapse"> 
		<h5 style="color: #fff;
    margin-left: 21px; font-size: 15px">Technical Support 11am to 6pm | Call : <a href="tel:tel:+91 9999-70-7706" style="width: 40px;"> 9999-70-7706</a>  | E-Mail : <a href="mailto:tech@zupatech.com" style="width: 40px;"> tech@zupatech.com</a></h5>
            <ul class="navbar-nav ml-auto">
			
			<span class="b-title"><?= $this->zone_model->find_by_name($this->acc_zone); ?> Dashboard</span>
                <li><a href="javascript:;" class="displayChatbox"></a></li>
                <li>
                    <div class="dropdown drp-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span>Settings</span> <i class="icon feather icon-settings" style="padding: 0px 2px ; color: #fff"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="<?php echo base_url('assets/images/user/avatar-2.jpg');?>" class="img-radius" alt="User-Profile-Image">
                                <span><?= ucwords(strtolower($this->login_model->find_by_userfull($this->acc_id)));?></span>
                                <a title="Logout" href="<?=base_url(ADMIN_URL.'/login/logout')?>" class="dud-logout" title="Logout">
                                    <i class="feather icon-log-out"></i> 
                                </a>
                            </div>
                            <ul class="pro-body">
                                <li><a href="javascript:;" class="dropdown-item"><i class="feather icon-settings"></i> Settings</a></li>
                                <li><a href="<?= base_url(ADMIN_URL.'/settings/profile')?>" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
								<li><a href="<?=base_url(ADMIN_URL.'/login/logout')?>" class="dropdown-item"><i class="feather icon-user"></i>Logout</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </header>
<!-- [ Header ] end -->
 <?php $this->load->view($contentView); ?>
 
	<!-- Required Js -->
   
    <script src="<?php echo base_url('assets/js/vendor-all.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
	 <script src="<?php echo base_url('assets/plugins/data-tables/js/datatables.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/pages/tbl-datatable-custom.js');?>"></script>
	
    <script src="<?php echo base_url('assets/js/pcoded.min.js');?>"></script>
    <!--<script src="<?php echo base_url('assets/js/menu-setting.min.js');?>"></script>-->
    <!-- form-select-custom Js -->
    <!--<script src="<?php echo base_url('assets/js/pages/form-select-custom.js');?>"></script>-->
    <script src="<?php echo base_url('assets/js/horizontal-menu.js');?>"></script>
	
    <script src="<?php echo base_url('assets/plugins/bootstrap-datetimepicker/js/bootstrap-datepicker.min.js');?>"></script>
	   <!-- pnotify Js -->
	   <!-- include alertify script -->
	 <script src="<?php echo base_url('assets/plugins/modal-window-effects/js/classie.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/modal-window-effects/js/modalEffects.js');?>"></script>

   <script src="<?php echo base_url('assets/js/slick.min.js'); ?>"></script>
	<script src="<?php echo base_url('assets/alertifyjs/alertify.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/plugins/pnotify/js/pnotify.custom.min.js');?>"></script> 
    <script src="<?php echo base_url('assets/plugins/sweetalert/js/sweetalert.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/notify.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/custom.js');?>"></script>

	<script type="text/javascript">
	$.fn.datepicker.defaults.format = "dd/mm/yyyy";
$('.datepicker').datepicker();
	/*$(function (){ 

	 $('#d_week').datepicker({
         format: 'mm-dd-yyyy'
});
});
*/
        // Collapse menu
        (function() {
            if ($('#layout-sidenav').hasClass('sidenav-horizontal') || window.layoutHelpers.isSmallScreen()) {
                return;
            }
            try {
                window.layoutHelpers.setCollapsed(
                    localStorage.getItem('layoutCollapsed') === 'true',
                    false
                );
            } catch (e) {}
        })();
        $(function() {
            // Initialize sidenav
            $('#layout-sidenav').each(function() {
                new SideNav(this, {
                    orientation: $(this).hasClass('sidenav-horizontal') ? 'horizontal' : 'vertical'
                });
            });

            // Initialize sidenav togglers
            $('body').on('click', '.layout-sidenav-toggle', function(e) {
                e.preventDefault();
                window.layoutHelpers.toggleCollapsed();
                if (!window.layoutHelpers.isSmallScreen()) {
                    try {
                        localStorage.setItem('layoutCollapsed', String(window.layoutHelpers.isCollapsed()));
                    } catch (e) {}
                }
            });
        });
        $(document).ready(function() {
            $("#pcoded").pcodedmenu({
                themelayout: 'horizontal',
                MenuTrigger: 'hover',
                SubMenuTrigger: 'hover',
            });
			
			
        });
		
    </script>

</body>

</html>