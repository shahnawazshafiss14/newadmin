<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title><?=$Page_Title?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/fontawesome/css/fontawesome-all.min.css');?>">
    <!-- animation css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/animation/css/animate.min.css');?>">
   <!-- select2 css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/select2/css/select2.min.css');?>">
    <!-- multi-select css -->
    <!-- vendor css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
</head>

<body>
    <!-- [ navigation menu ] start -->
	
	<nav class="pcoded-navbar">
        <div class="navbar-wrapper">
            <div class="navbar-brand header-logo">
                <a href="index.html" class="b-brand">
                    <div class="b-bg"> 
                        <i class="feather icon-trending-up"></i>
                    </div>
                    <span class="b-title">Datta Able</span>
                </a>
                <a class="mobile-menu" id="mobile-collapse" href="#!"><span></span></a>
            </div>
            <div class="navbar-content scroll-div ps ps--active-y">
                <ul class="nav pcoded-inner-navbar">
                    <li data-username="dashboard Default Ecommerce CRM Analytics Crypto Project" class="nav-item pcoded-hasmenu active">
                        <a href="#!" class="nav-link"><span class="pcoded-micon"><i class="feather icon-home"></i></span><span class="pcoded-mtext">Dashboard</span></a>
                        <ul class="pcoded-submenu">
                            <li class="active"><a href="index.html" class="">Default</a></li>
                            <li class=""><a href="dashboard-ecommerce.html" class="">Ecommerce</a></li>
                            <li class=""><a href="dashboard-crm.html" class="">CRM</a></li>
                            <li class=""><a href="dashboard-analytics.html" class="">Analytics</a></li>
                            <li class=""><a href="dashboard-crypto.html" class="">Crypto<span class="pcoded-badge label label-danger">NEW</span></a></li>
                            <li class=""><a href="dashboard-project.html" class="">Project</a></li>
                        </ul>
                    </li>
                    
                </ul>
            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 587px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 160px;"></div></div></div>
        </div>
    </nav>
	
	
    <!-- [ navigation menu ] end -->

    <!-- [ Header ] start -->
    <header class="navbar pcoded-header navbar-expand-lg navbar-light">
        <div class="m-header">
            <a class="mobile-menu" id="mobile-collapse1" href="javascript:;"><span></span></a>
            <a href="<?php echo base_url(ADMIN_URL.'/dashboard');?>" class="b-brand">
                <div class="b-bg">
                    <i class="feather icon-trending-up"></i>
                </div>
                <span class="b-title">Admin Dashboard</span>
                <!-- <img class="img-fluid horizontal-dasktop" src="<?php echo base_url('assets/images/logo-dark.png');?>" alt="Theme-Logo" />
                <img class="img-fluid horizontal-mobile" src="<?php echo base_url('assets/images/logo.png');?>" alt="Theme-Logo" /> -->
            </a>
        </div>
        <div class="collapse navbar-collapse"> 
            <ul class="navbar-nav ml-auto">
                <li><a href="javascript:;" class="displayChatbox"></a></li>
                <li>
                    <div class="dropdown drp-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon feather icon-settings"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-notification">
                            <div class="pro-head">
                                <img src="<?php echo base_url('assets/images/user/avatar-1.jpg');?>" class="img-radius" alt="User-Profile-Image">
                                <span><?=ucwords(strtolower($this->session->userdata('username')));?></span>
                                <a href="auth-signin.html" class="dud-logout" title="Logout">
                                    <i class="feather icon-log-out"></i>
                                </a>
                            </div>
                            <ul class="pro-body">
                                <li><a href="javascript:;" class="dropdown-item"><i class="feather icon-settings"></i> Settings</a></li>
                                <li><a href="javascript:;" class="dropdown-item"><i class="feather icon-user"></i> Profile</a></li>
								<li><a href="<?=base_url(ADMIN_URL.'/login/logout')?>" class="dropdown-item"><i class="feather icon-user"></i>Logouttt</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </header>
<!-- [ Header ] end -->
