<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
	<!-- Required Js -->
    <script src="<?php echo base_url('assets/js/vendor-all.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
	 <script src="<?php echo base_url('assets/plugins/data-tables/js/datatables.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/pages/tbl-datatable-custom.js');?>"></script>
	
    <script src="<?php echo base_url('assets/js/pcoded.min.js');?>"></script>
    <!--<script src="<?php echo base_url('assets/js/menu-setting.min.js');?>"></script>-->
    <!-- form-select-custom Js -->
    <!--<script src="<?php echo base_url('assets/js/pages/form-select-custom.js');?>"></script>-->
    <script src="<?php echo base_url('assets/js/horizontal-menu.js');?>"></script>
	   <!-- pnotify Js -->
    <script src="<?php echo base_url('assets/plugins/pnotify/js/pnotify.custom.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/pnotify/js/notify-event.js');?>"></script>

	<script type="text/javascript">
        // Collapse menu
        (function() {
            if ($('#layout-sidenav').hasClass('sidenav-horizontal') || window.layoutHelpers.isSmallScreen()) {
                return;
            }
            try {
                window.layoutHelpers.setCollapsed(
                    localStorage.getItem('layoutCollapsed') === 'true',
                    false
                );
            } catch (e) {}
        })();
        $(function() {
            // Initialize sidenav
            $('#layout-sidenav').each(function() {
                new SideNav(this, {
                    orientation: $(this).hasClass('sidenav-horizontal') ? 'horizontal' : 'vertical'
                });
            });

            // Initialize sidenav togglers
            $('body').on('click', '.layout-sidenav-toggle', function(e) {
                e.preventDefault();
                window.layoutHelpers.toggleCollapsed();
                if (!window.layoutHelpers.isSmallScreen()) {
                    try {
                        localStorage.setItem('layoutCollapsed', String(window.layoutHelpers.isCollapsed()));
                    } catch (e) {}
                }
            });
        });
        $(document).ready(function() {
            $("#pcoded").pcodedmenu({
                themelayout: 'horizontal',
                MenuTrigger: 'hover',
                SubMenuTrigger: 'hover',
            });
			
			
        });
		
    </script>

</body>

</html>