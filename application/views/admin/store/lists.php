  <?php 
  $check_p  = check_permisson('27');
  ?>
   <!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Cients</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Employees</h5>
										<?php echo $this->session->flashdata('msg');?>
                                    </div>
                                    <div class="card-body">

                                        <!-- [ configuration table ] start -->

                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Name</th>                                                           
                                                            <th>Adhaar no</th>                                                           
                                                            <th>Mobile</th>                                                           
                                                            <th>Salary</th>                                                           
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
														$s=1;	
														if(count($employees)>0):			
															foreach($employees as $empy): ?>
																<tr>
																	<td><?=$s++;?></td>
																	<td><?=$empy->user_name?></td>
																	<td><?= !empty($empy->user_adharno) ? $empy->user_adharno : ''; ?></td>
																	<td><?= !empty($empy->user_mobile) ? $empy->user_mobile : ''; ?></td>
																	<td><button class="btn btn-primary md-trigger" data-modal="modal-<?= $s; ?>">Add Salary</button>
																	
																	 <div class="md-modal md-effect-10 dionip" id="modal-<?= $s; ?>">
        <div class="md-content">
		
            <h3 class="theme-bg2">Add Salary To <?= ucwords($empy->user_name);?> RS:<?= $empy->user_salary; ?> </h3>
            <div> 
                <ul>
					<li><input type="text" class="form-control price" name="add_amount" id="add_amount"  /></li>
					<li><button type="button" class="btn btn-success add_salary" data-empid="<?= $empy->acc_id; ?>" data-modeid="modal-<?= $s; ?>">Add</button></li>
                </ul>
                <button class="btn btn-danger md-close">Close me!</button>
               
            </div>
        </div>
    </div>
																	</td>
																	<td>
																	<?php 
															if(!empty($check_p->is_viewed)):
															?>
																	<a title="Employee View Details" href="<?= base_url(ADMIN_URL.'/employee/details/'.$empy->acc_id)?>" class="label theme-bg2 text-white f-12 btn-sm"><i class="feather icon-eye"></i></a>
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
																	| <a title="Edit Details" href="<?= base_url(ADMIN_URL.'/employee/add/'.$empy->acc_id)?>" class="btn theme-bg text-white f-12 btn-sm"><i class="feather icon-edit"></i></a> | 
																	<?php endif;?>
															<?php 
															if(!empty($check_p->is_deleted)):
															?>
																	<a title="Delete Employee" href="javascript:;" class="btn btn-danger btn-sm"><i class="feather icon-trash-2"></i></a>
																	<?php 
															endif;
															?>
																	</td>
																</tr>
															<?php
															endforeach; 
														endif;
													?>		
                                                    </tbody>
                                                    <tfoot>
                                                         <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Name</th>                                                           
                                                            <th>Adhaar no</th>                                                           
                                                            <th>Mobile</th>
															<th>Salary</th>  															
                                                            <th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		

        <!-- [ form-element ] end -->
    </div>
    <!-- [ Main Content ] end -->
</div>