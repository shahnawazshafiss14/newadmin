 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-6">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Employee Details</h5>
                                        </div>
                                        <div class="card-body"> 
                                            <div class="row"> 
											<div class="col-xl-4 col-md-6 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Name</label>
                                                          : <?= !empty($employeed->user_name) ? $employeed->user_name : ''; ?> 
                                                        </div> 
                                                </div>
                                                <div class="col-xl-4 col-md-4 mb-3">
                                                    
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">User Email</label>
                                                            : <?= !empty($employeed->user_email) ? $employeed->user_email : ''; ?> 
                                                        </div>
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3">
                                                    
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">User Adhaar No</label>
                                                            : <?= !empty($employeed->user_adharno) ? $employeed->user_adharno : ''; ?> 
                                                        </div>
                                                       
                                                    
                                                </div>
												 <div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">User Mobile</label>
                                                            : <?= !empty($employeed->user_mobile) ? $employeed->user_mobile : ''; ?> 
                                                        </div>
												 
                                                </div>
												 <div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">User Address:</label>
                                                            : <?= !empty($employeed->user_address) ? $employeed->user_address : ''; ?> 
                                                        </div>
                                                </div>
												<!--div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">User State:</label>
                                                            : <?= !empty($employeed->user_state) ? $employeed->user_state : ''; ?> 
                                                        </div>
                                                </div-->
												<div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
                                                            <label class="font-weight-bold">User Town:</label>
                                                            : <?= !empty($employeed->user_town) ? $employeed->user_town : ''; ?> 
                                                        </div>
                                                </div>
												<!--div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">User City:</label>
                                                            : <?= !empty($employeed->user_city) ? $employeed->user_city : ''; ?> 
                                                        </div>
                                                </div-->
												<div class="col-xl-4 col-md-4 mb-3">
														<div class="form-group">
                                                            <label class="font-weight-bold">User City:</label>
                                                           : <?= !empty($employeed->user_city) ? $employeed->user_city : ''; ?> 
                                                        </div>
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label class="font-weight-bold">Joining Date:</label>
                                                           : <?= !empty($employeed->created_date) ? $employeed->created_date : ''; ?> 
                                                        </div> 
												 <div class="mb-5"> </div>
                                                </div>
                                            </div>
											</div>
									</div>
									</div>
									<div class="col-sm-6">
									<div class="card-header">
                                            <h5>Employee Salary Details</h5>
                                        </div>
									<div class="table-responsive">
                                                <table id="key-act-button1" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Date</th>                                                           
                                                            <th>Rs</th>                                                           
                                                            <th>Send on</th>
															<th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
														$s=1;
														$array_em = array(
															'emp_id' => $employeed->acc_id
														);
														$employees = $this->salary_model->viewRecordAnyR($array_em);
														if(count($employees)>0){			
															foreach($employees as $empy){ ?>
																<tr>
																	<td><?=$s++;?></td>
																	<td><?=$empy->trans_date?></td>
																	<td><?= !empty($empy->trans_amount) ? $empy->trans_amount : ''; ?></td>
																	<td><?= !empty($empy->created_on) ? $empy->created_on : ''; ?></td>
																	<td>  
																	  
																	</td>
																	<td>
																	</tr>
																	<?php 
															}
														}
																	?>
																	 		
                                                    </tbody>
                                                    <tfoot>
                                                         <tr>
                                                             <th>Sr.no.</th>
                                                            <th>Date</th>                                                           
                                                            <th>Rs</th>                                                           
                                                            <th>Send on</th>
															<th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
									</div>
									</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>