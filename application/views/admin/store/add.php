 <?php 
  $check_p  = check_permisson('26');
  ?> 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-1"></div><div class="col-sm-10">
							<!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Store Details</h5>
											<?php 
											$flash_data =  $this->session->flashdata('store_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> store has been add successful.
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('store_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                        </div>
                                        <div class="card-body">
										
                                            <form action="<?= U_A_URL . 'store/savestore'?>" id="frmstore" name="frmstore" method="POST" />
                                            <div class="row">
											<div class="col-xl-4 col-md-6 mb-3">
												<div class="form-group">
													<label>Name</label>
													<input type="text" id="user_fullname" name="user_fullname" value="<?= !empty($employeed->user_fullname) ? $employeed->user_fullname : ''; ?>" class="form-control" placeholder="">
												</div>
                                            </div>
											<div class="col-xl-4 col-md-6 mb-3">
												<div class="form-group">
													<label>User ID(For Login ID)</label>
													<input type="text" id="user_name" name="user_name" value="<?= !empty($employeed->user_name) ? $employeed->user_name : ''; ?>" class="form-control" placeholder="">
												</div>
                                            </div>
											<div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
													<label>User Email</label>
													<input type="text" id="user_email" name="user_email" value="<?= !empty($employeed->user_email) ? $employeed->user_email : ''; ?>" class="form-control" placeholder="">
												</div> 
                                            </div>
											<div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
													<label>User Adhaar No.</label>
													<input type="text" id="user_adharno" name="user_adharno" value="<?= !empty($employeed->user_adharno) ? $employeed->user_adharno : ''; ?>" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
													<label>User Mobile:</label>
													<input type="text" maxlength="10" id="user_mobile" name="user_mobile" value="<?= !empty($employeed->user_mobile) ? $employeed->user_mobile : ''; ?>" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
													<label>User State:</label>
													<select id="user_state" name="user_state" class="form-control find_city">
													<option value="">Select State</option>
													<?php 
													$arr_location = array(
														'location_parent' => '101'
													);
													$fetch_location = $this->location_model->viewRecordAnyR($arr_location);
													foreach($fetch_location as $loc):
													if($employeed->user_state == $loc->location_id){
														$state_va = "selected";
													}else{
														$state_va = "";
													}
													?>
													<option value="<?= $loc->location_id; ?>" <?= $state_va; ?>><?= $loc->location_name; ?></option>
													<?php endforeach; ?>
													</select>
												</div>
											</div>											
											<div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
												<label>User City:</label>
												<div id="userajaxcity">
												<select id="user_city" name="user_city" class="form-control">
													<option value="">Select City</option>
													<?php 
													$arr_location = array(
														'location_parent' => $employeed->user_state
													);
													$fetch_location = $this->location_model->viewRecordAnyR($arr_location);
													foreach($fetch_location as $loc):
													if($employeed->user_city == $loc->location_id){
														$city_va = "selected";
													}else{
														$city_va = "";
													}
													?>
													<option value="<?= $loc->location_id; ?>" <?= $city_va; ?>><?= $loc->location_name; ?></option>
													<?php endforeach; ?>
													</select>
												</div>
												</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
													<label>User Town:</label>
													<input type="text" id="user_town" name="user_town" value="<?= !empty($employeed->user_town) ? $employeed->user_town : ''; ?>" class="form-control" placeholder="">
													<input type="hidden" id="employee_slug" name="employee_slug" value="<?= !empty($employeed->acc_id) ? $employeed->acc_id : ''; ?>" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
													<label>Zone(Registered office area):</label>
													<select id="user_zone" name="user_zone" class="form-control">
													<option value="">Select Zone</option>
													<?php 
													$arr_zone = array(
														'view_status' => '1',
														'status' => '1'
													);
													$fetch_zone = $this->zone_model->viewRecordAnyR($arr_zone);
													foreach($fetch_zone as $zone):
													if($employeed->user_zone == $zone->id){
														$zone_va = "selected";
													}else{
														$zone_va = "";
													}
													?>
													<option value="<?= $zone->id; ?>" <?= $zone_va; ?>><?= $zone->zone_name; ?></option>
													<?php endforeach; ?>
													</select>
													<input type="hidden" id="employee_slug" name="employee_slug" value="<?= !empty($employeed->acc_id) ? $employeed->acc_id : ''; ?>" class="form-control" placeholder="">
												</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
											<div class="form-group">
												<label>User Address:</label>
												<input type="text" id="user_address" name="user_address" value="<?= !empty($employeed->user_address) ? $employeed->user_address : ''; ?>" class="form-control" placeholder="">
											</div>
											</div>
											 
											<div class="col-xl-4 col-md-4 mb-3">
											<div class="form-group">
												<label>User Address2:</label>
												<input type="text" id="user_address2" name="user_address2" value="<?= !empty($employeed->user_address2) ? $employeed->user_address2 : ''; ?>" class="form-control" placeholder="">
											</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
											<div class="form-group">
												<label>Company Name(For Invoicing):</label>
												<input type="text" id="user_company" name="user_company" value="<?= !empty($employeed->user_company) ? $employeed->user_company : ''; ?>" class="form-control" placeholder="">
											</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
											<div class="form-group">
												<label>Company Slogn(For Invoicing):</label>
												<input type="text" id="user_company_slogn" name="user_company_slogn" value="<?= !empty($employeed->user_company_slogn) ? $employeed->user_company_slogn : ''; ?>" class="form-control" placeholder="">
											</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
											<div class="form-group">
												<label>Company GSTNO:</label>
												<input type="text" id="user_gstno" name="user_gstno" value="<?= !empty($employeed->user_gstno) ? $employeed->user_gstno : ''; ?>" class="form-control" placeholder="">
											</div>
											</div>
											<div class="col-xl-4 col-md-4 mb-3">
											<div class="form-group">
												<label>Authorised Signature:</label>
												<input type="text" id="user_signature" name="user_signature" value="<?= !empty($employeed->user_signature) ? $employeed->user_signature : ''; ?>" class="form-control" placeholder="">
											</div>
											</div>
											<!--div class="col-xl-4 col-md-4 mb-3">
												<div class="form-group">
												<label>Salary:</label>
												<input type="text" id="user_salary" name="user_salary" value="<?= !empty($stored->user_salary) ? $stored->user_salary : ''; ?>" class="form-control price" placeholder="">
												</div>
											</div-->
											<div class="col-xl-4 col-md-4 mb-3"> 
												 
												 <div class=" mb-5"> </div>
												 <?php 
													if(!empty($check_p->is_inserted)):
												 ?>
												 <button type="submit" id="btnAddstore" name="btnAddstore" class="btn btn-primary"><?= !empty($stored->acc_id) ? 'Update' : 'Submit'; ?></button>
												<?php 
															endif;
															?>
                                                </div>
												
												
                                            </div>
											</form> 
														
												 
                                                </div>
												
												
												
                                            </div>
											
											
                                                </div><div class="col-sm-1"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>