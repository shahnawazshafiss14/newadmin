  <?php 
  $check_p  = check_permisson('30');
  ?>
   <!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Zones</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">
                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h5>Zones</h5>
										<?php echo $this->session->flashdata('zone_save');?>
                                    </div>
									<form action="<?= base_url();?>admin/master/addzone" method="POST">
									<div class="col-xl-4 col-md-6 mb-3">
												<div class="form-group">
													<label>Name</label>
													<input type="text" id="zone_name" name="zone_name" value="<?= !empty($zone->zone_name) ? $zone->zone_name : ''; ?>" class="form-control" placeholder="" required>
													
													<input type="hidden" id="zone_slug" name="zone_slug" value="<?= !empty($zone->zone_slug) ? $zone->zone_slug : ''; ?>" class="form-control" placeholder="">
													<?php 
															if(!empty($check_p->is_inserted)):
															?>
													<input class="btn btn-primary" type="submit" name="addzone" id="addzone" value="<?= !empty($zone->zone_slug) ? 'Update' : 'Add Zone'; ?>" />
													<?php endif;?>
												</div>
                                    </div>
									</form>
									<hr />
									<?php 
									$find_url_slug = $this->uri->segment(4);
													if(empty($find_url_slug)){
									?>
                                    <div class="card-body">

                                        <!-- [ configuration table ] start -->

                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Name</th>                                          
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
													
														$s=1;	
														if(count($zones)>0):			
															foreach($zones as $zone): ?>
																<tr>
																	<td><?=$s++;?></td>
																	<td><?=$zone->zone_name; ?></td>
																	<td>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
																	| <a title="Edit Details" href="<?= base_url(ADMIN_URL.'/master/zone/'.$zone->zone_slug)?>" class="btn theme-bg text-white f-12 btn-sm"><i class="feather icon-edit"></i></a> | 
																	<?php endif;?>
															<?php 
															if(!empty($check_p->is_deleted)):
															?>
																	<a title="Delete zonesdelete" href="javascript:;" class="btn btn-danger btn-sm"><i class="feather icon-trash-2"></i></a>
																	<?php 
															endif;
															?>
																	</td>
																</tr>
															<?php
															endforeach; 
														endif;
													
													?>		
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Name</th>                                             
                                                            <th>Action</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                  </div>
								  <?php 
													}
								  ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
    <!-- [ Main Content ] end -->
</div>