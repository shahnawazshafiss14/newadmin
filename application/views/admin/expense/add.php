 <?php 
  $check_p  = check_permisson('16');
  ?> 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10"><?= $Page_Title; ?></h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Expense Details</h5>
											<?php 
											$flash_data =  $this->session->flashdata('expense_save');
											if(!empty($flash_data)):
											?>
											<div class="alert alert-success alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Success!</strong> Expense has been add successful.
											</div>
											<?php 
											endif;
											?>
											 <?php 
											$flash_data1 =  $this->session->flashdata('expense_save_danger');
											if(!empty($flash_data1)):
											?>
											<div class="alert alert-danger alert-dismissible">
												<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
												<strong>Error!</strong> something is wrong.
											</div>
											<?php 
											endif; 
											?>
                                        </div>
                                        <div class="card-body">
										
                                            <form action="<?= U_A_URL . 'expense/saveexpense'?>" id="frmExpense" name="frmExpense" method="POST" />
                                            <div class="row">
											<?php 
											if($this->acc_level == '1'):
											?>
											 <div class="col-xl-4 col-md-4 mb-3"> 
												<div class="form-group">
													<label>Select User</label>
													<select id="expn_by" name="expn_by" class="form-control">
													<option value="">Select User</option>
													<?php 
													$array_users = array(
														'view_status' => '1',
														'user_level' => '2'
													);
													$userquery = $this->login_model->viewRecordAnyR($array_users);
													if(count($userquery) > 0):
													foreach($userquery as $user):
														if($user->acc_id == $expensed->expn_by){
															$val = "selected";
														}else{
															$val = "";
														}
													?>
													<option value="<?= $user->acc_id; ?>" <?= $val; ?>><?= $user->user_name; ?></option>
													<?php endforeach;?>
													<?php endif;?>
													</select>
												</div> 
                                                </div>
												<?php 
												else:
												?>
												
												<input id="expn_by" name="expn_by" class="form-control" type="hidden" value="<?= $this->acc_id; ?>">
												
												<?php
												endif;
												?>
												
											<div class="col-xl-4 col-md-6 mb-3"> 
                                                        <div class="form-group">
                                                            <label>Expense Date</label> 
                                                            <input type="text" id="expn_date" name="expn_date" value="<?= !empty($expensed->expn_date) ? $expensed->expn_date : date('Y-m-d'); ?>" class="form-control datepicker" data-date-format="yyyy-mm-dd" placeholder="" autocomplete="off">
                                                        </div> 
                                                </div>
											
                                                
                                                <div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label>Expense Type</label>
                                                            <select id="expn_type" name="expn_type" class="form-control">
															<option value="">Select Type</option>
																<?php 
																$array_types = array(
																	'view_status' => '1'
																);
																$typequery = $this->expensetype_model->viewRecordAnyR($array_types);
																if(count($typequery) > 0):
																foreach($typequery as $type):
																if($type->type_id == $expensed->expn_type){
																	$val = "selected";
																}else{
																	$val = "";
																}
																?>
																<option value="<?= $type->type_id; ?>" <?= $val; ?>><?= $type->type_name; ?></option>
																<?php endforeach;?>
																<?php endif;?>
															</select>
                                                        </div> 
                                                </div> 
												 <div class="col-xl-4 col-md-4 mb-3">
                                                        <div class="form-group">
                                                            <label>Amount</label>
                                                            <input type="text" id="expn_amount" name="expn_amount" value="<?= !empty($expensed->expn_amount) ? $expensed->expn_amount : ''; ?>" class="form-control" placeholder="">
                                                        </div>
														<input type="hidden" id="expense_slug" name="expense_slug" value="<?= !empty($expensed->slug) ? $expensed->slug : ''; ?>" class="form-control" placeholder="">
                                                </div> 
												<div class="col-xl-4 col-md-4 mb-3"> 
                                                        <div class="form-group">
                                                            <label>Expense Mode</label>
                                                            <select id="expn_mode" name="expn_mode" class="form-control">
															<?php 
															if(!empty($expensed->expn_mode)):
															?>
																<option value="<?= $expensed->expn_mode; ?>"><?= payment_mode_type($expensed->expn_mode); ?></option>
																<?php 
																endif;
															?>
																<option value="">Select Mode</option>
																<option value="1">Cash</option>
																<option value="2">Online</option>
															</select>
                                                        </div> 
                                                </div>
												<div class="col-xl-4 col-md-4 mb-3">  
												 <div class=" mb-5"> </div>
												 <?php 
													if(!empty($check_p->is_inserted)):
												 ?>
												 <button type="submit" id="btnAddExpense" name="btnAddExpense" class="btn btn-primary"><?= !empty($expensed->slug) ? 'Update' : 'Submit'; ?></button>
												<?php 
															endif;
															?>
                                                </div>
												
												
                                            </div>
											</form> 
														
												 
                                                </div>
												
												
												
                                            </div>
											
											
											
											
											
											
											
											
											
											
											
											
                                            
                                                
                                                
                                            
                                            
                                            
                                            
                                            
                                                    
                                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>