  <?php 
  $check_p  = check_permisson('17');
  ?>
   <!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
               
                <div class="main-body">
                    <div class="page-wrapper">
                        <!-- [ Main Content ] start -->
                        <div class="row">

                            <!-- [ form-element ] start -->
                            <div class="col-sm-1"></div><div class="col-sm-10">
							 <!-- [ breadcrumb ] start -->
                <div class="page-header">
                    <div class="page-block">
                        <div class="row align-items-center">
                            <div class="col-md-12">
                                <div class="page-header-title">
                                    <h5 class="m-b-10">Expenses</h5>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- [ breadcrumb ] end -->
                                <div class="card">
                                    
                                    <div class="card-body">

                                        <!-- [ configuration table ] start -->

                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Sr.no.</th>
                                                            <th>Expense User</th>                                                           
                                                            <th>Expense Type</th>                                                           
                                                            <th>Expense Amount</th>                                    
                                                            <th>Expense Date</th>                                                           
                                                            <th>Expense Mode</th>                                                           
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php
														$s=1;	
														if(count($expensed)>0):			
															foreach($expensed as $expense): ?>
																<tr>
																	<td><?=$s++;?></td>
																	<td><?= $this->login_model->find_by_user($expense->expn_by);?></td>
																	<td><?= !empty($expense->expn_type) ? $this->expensetype_model->find_by_name($expense->expn_type) : ''; ?></td>
																	<td><?= !empty($expense->expn_amount) ? $expense->expn_amount : ''; ?></td>
																	<td><?= !empty($expense->expn_date) ? $expense->expn_date : ''; ?></td>
																	<td><?= !empty($expense->expn_mode) ? payment_mode_type($expense->expn_mode) : ''; ?></td>
																	<td>
																	<?php 
															if(!empty($check_p->is_viewed)):
															?>
																	<a title="Client View Details" href="<?= base_url(ADMIN_URL.'/expense/details/'.$expense->slug)?>" class="btn btn-dark btn-sm"><i class="feather icon-eye"></i></a>
															<?php endif;?>
															<?php 
															if(!empty($check_p->is_edited)):
															?>
																	 <a title="Edit Details" href="<?= base_url(ADMIN_URL.'/expense/add/'.$expense->slug)?>" class="btn btn-dark btn-sm"><i class="feather icon-edit"></i></a>  
																	<?php endif;?>
															<?php 
															if(!empty($check_p->is_deleted)):
															?>
																	<a title="Delete Client" href="javascript:;" class="btn btn-danger btn-sm"><i class="feather icon-trash-2"></i></a>
																	<?php 
															endif;
															?>
																	</td>
																</tr>
															<?php
															endforeach; 
														endif;
													?>		
                                                    </tbody>
                                                   
                                                </table>
                                            </div>
                                        </div>
                                  </div>
                                </div>
                            </div><div class="col-sm-1"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- [ form-element ] end -->
    </div>
    <!-- [ Main Content ] end -->
</div>