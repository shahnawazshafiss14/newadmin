 
<!-- [ Main Content ] start -->
<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <div class="pcoded-content">
            <div class="pcoded-inner-content">
                <!-- [ breadcrumb ] start -->
                
                <!-- [ breadcrumb ] end -->
                <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                                <!-- [ online-order section ] start -->
                                <div class="col-md-6 col-xl-4">
                                    <div class="card Online-Order">
                                        <div class="card-block">
                                            <h5>Online Orders</h5>
                                            <h6 class="text-muted d-flex align-items-center justify-content-between m-t-30">Delivery Orders<span class="float-right f-18 text-c-green">237 / 400</span></h6>
                                            <div class="progress mt-3">
                                                <div class="progress-bar progress-c-theme" role="progressbar" style="width:65%;height:6px;" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <span class="text-muted mt-2 d-block">37% Done</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-xl-4">
                                    <div class="card Online-Order">
                                        <div class="card-block">
                                            <h5>Pending Orders</h5>
                                            <h6 class="text-muted d-flex align-items-center justify-content-between m-t-30">Pending Orders<span class="float-right f-18 text-c-purple">100 / 500</span></h6>
                                            <div class="progress mt-3">
                                                <div class="progress-bar progress-c-theme2" role="progressbar" style="width:50%;height:6px;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <span class="text-muted mt-2 d-block">20% Pending</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-xl-4">
                                    <div class="card Online-Order">
                                        <div class="card-block">
                                            <h5>Return Orders</h5>
                                            <h6 class="text-muted d-flex align-items-center justify-content-between m-t-30">Return Orders<span class="float-right f-18 text-c-blue">50 / 400</span></h6>
                                            <div class="progress mt-3">
                                                <div class="progress-bar progress-c-blue" role="progressbar" style="width:40%;height:6px;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                            <span class="text-muted mt-2 d-block">10% Return</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ online-order section ] end -->

                                <!-- [ yearly summary chart ] start -->
                                <div class="col-xl-8 col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Yearly Summary</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="row pb-3">
                                                <div class="col-md-4 col-6 text-center m-b-10">
                                                    <h3 class="f-w-300">$2356.4</h3>
                                                    <span>Invoiced</span>
                                                </div>
                                                <div class="col-md-4 col-6 text-center m-b-10">
                                                    <h3 class="f-w-300">$1935.6</h3>
                                                    <span>Profit</span>
                                                </div>
                                                <div class="col-md-4 col-12 text-center m-b-10">
                                                    <h3 class="f-w-300">$468.9</h3>
                                                    <span>Expenses</span>
                                                </div>
                                            </div>
                                            <div id="bar-chart3" class="bar-chart3" style="height: 270px; overflow: hidden; text-align: left;"><div class="amcharts-main-div" style="position: relative; width: 100%; height: 100%;"><div class="amChartsLegend amcharts-legend-div" style="overflow: hidden; position: relative; text-align: left; width: 623px; height: 48px;"><svg version="1.1" style="position: absolute; width: 623px; height: 48px;"><desc>JavaScript chart by amCharts 3.21.5</desc><g transform="translate(48,10)"><path cs="100,100" d="M0.5,0.5 L574.5,0.5 L574.5,37.5 L0.5,37.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0"></path><g transform="translate(0,11)"><g cursor="pointer" aria-label="Last Week " transform="translate(0,0)"><path cs="100,100" d="M-15.5,8.5 L16.5,8.5 L16.5,-7.5 L-15.5,-7.5 Z" fill="url(#AmChartsEl-5)" stroke="#1de9b6,#1dc4e9" fill-opacity="1" stroke-width="1" stroke-opacity="0.9" transform="translate(16,8)"></path><linearGradient id="AmChartsEl-5" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#1de9b6"></stop><stop offset="100%" stop-color="#1dc4e9"></stop></linearGradient><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" transform="translate(37,7)"><tspan y="6" x="0">Last Week </tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(163,7)"> </text><rect x="32" y="0" width="130.703125" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g><g cursor="pointer" aria-label="Market Place " transform="translate(178,0)"><path cs="100,100" d="M-15.5,8.5 L16.5,8.5 L16.5,-7.5 L-15.5,-7.5 Z" fill="url(#AmChartsEl-6)" stroke="#a389d4,#899ed4" fill-opacity="1" stroke-width="1" stroke-opacity="0.9" transform="translate(16,8)"></path><linearGradient id="AmChartsEl-6" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#a389d4"></stop><stop offset="100%" stop-color="#899ed4"></stop></linearGradient><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="start" transform="translate(37,7)"><tspan y="6" x="0">Market Place </tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(163,7)"> </text><rect x="32" y="0" width="130.703125" height="18" rx="0" ry="0" stroke-width="0" stroke="none" fill="#fff" fill-opacity="0.005"></rect></g></g></g></svg></div><div class="amcharts-chart-div" style="overflow: hidden; position: relative; text-align: left; width: 623px; height: 222px; padding: 0px; touch-action: auto;"><svg version="1.1" style="position: absolute; width: 623px; height: 222px; top: 0px; left: 0px;"><desc>JavaScript chart by amCharts 3.21.5</desc><g><path cs="100,100" d="M0.5,0.5 L622.5,0.5 L622.5,221.5 L0.5,221.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0"></path><path cs="100,100" d="M0.5,0.5 L574.5,0.5 L574.5,176.5 L0.5,176.5 L0.5,0.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" transform="translate(48,10)"></path></g><g><g transform="translate(48,10)"></g><g transform="translate(48,10)" visibility="visible"></g></g><g transform="translate(48,10)" clip-path="url(#AmChartsEl-7)"><g visibility="hidden"></g></g><g></g><g></g><g></g><g><g transform="translate(48,10)"><g><g transform="translate(58,176)" aria-label="Last Week  Q1 5.5"><path cs="100,100" d="M0.5,0.5 L0.5,-65.5 L12.5,-65.5 L12.5,0.5 L0.5,0.5 Z" fill="url(#AmChartsEl-9)" stroke="#1de9b6,#1dc4e9" fill-opacity="1" stroke-width="1" stroke-opacity="0.9"></path><linearGradient id="AmChartsEl-9" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#1de9b6"></stop><stop offset="100%" stop-color="#1dc4e9"></stop></linearGradient></g><g transform="translate(201,176)" aria-label="Last Week  Q2 6.5"><path cs="100,100" d="M0.5,0.5 L0.5,-109.5 L12.5,-109.5 L12.5,0.5 L0.5,0.5 Z" fill="url(#AmChartsEl-10)" stroke="#1de9b6,#1dc4e9" fill-opacity="1" stroke-width="1" stroke-opacity="0.9"></path><linearGradient id="AmChartsEl-10" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#1de9b6"></stop><stop offset="100%" stop-color="#1dc4e9"></stop></linearGradient></g><g transform="translate(345,176)" aria-label="Last Week  Q3 5.5"><path cs="100,100" d="M0.5,0.5 L0.5,-65.5 L12.5,-65.5 L12.5,0.5 L0.5,0.5 Z" fill="url(#AmChartsEl-11)" stroke="#1de9b6,#1dc4e9" fill-opacity="1" stroke-width="1" stroke-opacity="0.9"></path><linearGradient id="AmChartsEl-11" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#1de9b6"></stop><stop offset="100%" stop-color="#1dc4e9"></stop></linearGradient></g><g transform="translate(488,176)" aria-label="Last Week  Q4 7"><path cs="100,100" d="M0.5,0.5 L0.5,-131.5 L12.5,-131.5 L12.5,0.5 L0.5,0.5 Z" fill="url(#AmChartsEl-12)" stroke="#1de9b6,#1dc4e9" fill-opacity="1" stroke-width="1" stroke-opacity="0.9"></path><linearGradient id="AmChartsEl-12" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#1de9b6"></stop><stop offset="100%" stop-color="#1dc4e9"></stop></linearGradient></g></g></g><g transform="translate(48,10)"><g><g transform="translate(75,176)" aria-label="Market Place  Q1 4.5"><path cs="100,100" d="M0.5,0.5 L0.5,-21.5 L12.5,-21.5 L12.5,0.5 L0.5,0.5 Z" fill="url(#AmChartsEl-13)" stroke="#a389d4,#899ed4" fill-opacity="1" stroke-width="1" stroke-opacity="0.9"></path><linearGradient id="AmChartsEl-13" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#a389d4"></stop><stop offset="100%" stop-color="#899ed4"></stop></linearGradient></g><g transform="translate(218,176)" aria-label="Market Place  Q2 5"><path cs="100,100" d="M0.5,0.5 L0.5,-43.5 L12.5,-43.5 L12.5,0.5 L0.5,0.5 Z" fill="url(#AmChartsEl-14)" stroke="#a389d4,#899ed4" fill-opacity="1" stroke-width="1" stroke-opacity="0.9"></path><linearGradient id="AmChartsEl-14" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#a389d4"></stop><stop offset="100%" stop-color="#899ed4"></stop></linearGradient></g><g transform="translate(362,176)" aria-label="Market Place  Q3 6.5"><path cs="100,100" d="M0.5,0.5 L0.5,-109.5 L12.5,-109.5 L12.5,0.5 L0.5,0.5 Z" fill="url(#AmChartsEl-15)" stroke="#a389d4,#899ed4" fill-opacity="1" stroke-width="1" stroke-opacity="0.9"></path><linearGradient id="AmChartsEl-15" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#a389d4"></stop><stop offset="100%" stop-color="#899ed4"></stop></linearGradient></g><g transform="translate(505,176)" aria-label="Market Place  Q4 6"><path cs="100,100" d="M0.5,0.5 L0.5,-87.5 L12.5,-87.5 L12.5,0.5 L0.5,0.5 Z" fill="url(#AmChartsEl-16)" stroke="#a389d4,#899ed4" fill-opacity="1" stroke-width="1" stroke-opacity="0.9"></path><linearGradient id="AmChartsEl-16" x1="0%" x2="0%" y1="100%" y2="0%"><stop offset="0%" stop-color="#a389d4"></stop><stop offset="100%" stop-color="#899ed4"></stop></linearGradient></g></g></g></g><g></g><g><g><path cs="100,100" d="M0.5,0.5 L574.5,0.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(48,186)"></path></g><g><path cs="100,100" d="M0.5,0.5 L0.5,176.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(48,10)" visibility="visible"></path></g></g><g><g transform="translate(48,10)" clip-path="url(#AmChartsEl-8)" style="pointer-events: none;"><path cs="100,100" d="M0.5,0.5 L0.5,0.5 L0.5,176.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" visibility="hidden"></path><path cs="100,100" d="M0.5,0.5 L574.5,0.5 L574.5,0.5" fill="none" stroke-width="1" stroke-opacity="0.2" stroke="#000000" visibility="hidden"></path></g><clipPath id="AmChartsEl-8"><rect x="0" y="0" width="574" height="176" rx="0" ry="0" stroke-width="0"></rect></clipPath></g><g></g><g><g transform="translate(48,10)"></g><g transform="translate(48,10)"></g></g><g><g></g></g><g><g transform="translate(48,10)" visibility="visible"><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(72,193.5)"><tspan y="6" x="0">Q1</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(215,193.5)"><tspan y="6" x="0">Q2</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(359,193.5)"><tspan y="6" x="0">Q3</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="middle" transform="translate(502,193.5)"><tspan y="6" x="0">Q4</tspan></text></g><g transform="translate(48,10)" visibility="visible"><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,175)"><tspan y="6" x="0">400</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,131)"><tspan y="6" x="0">500</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,87)"><tspan y="6" x="0">600</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,43)"><tspan y="6" x="0">700</tspan></text><text y="6" fill="#000000" font-family="Verdana" font-size="11px" opacity="1" text-anchor="end" transform="translate(-12,-1)"><tspan y="6" x="0">800</tspan></text></g></g><g></g><g transform="translate(48,10)"></g><g></g><g></g><clipPath id="AmChartsEl-7"><rect x="-1" y="-1" width="576" height="178" rx="0" ry="0" stroke-width="0"></rect></clipPath></svg><a href="http://www.amcharts.com/javascript-charts/" title="JavaScript charts" style="position: absolute; text-decoration: none; color: rgb(0, 0, 0); font-family: Verdana; font-size: 11px; opacity: 0.7; display: block; left: 53px; top: 15px;">JS chart by amCharts</a></div></div></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ yearly summary chart ] end -->

                                <!-- [ earning-day section ] start -->
                                <div class="col-md-6 col-xl-4">
                                    <div class="card theme-bg earning-date">
                                        <div class="card-header borderless">
                                            <h5 class="text-white">Earnings</h5>
                                        </div>
                                        <div class="card-block">
                                            <div class="bd-example bd-example-tabs">
                                                <div class="tab-content" id="tabContent-pills">
                                                    <div class="tab-pane fade show active" id="earnings-mon" role="tabpanel" aria-labelledby="pills-earnings-mon">
                                                        <h2 class="text-white mb-3 f-w-300">359,234<i class="feather icon-arrow-up"></i></h2>
                                                        <span class="text-white mb-4 d-block">TOTAL EARNINGS</span>
                                                    </div>
                                                    <div class="tab-pane fade" id="earnings-tue" role="tabpanel" aria-labelledby="pills-earnings-tue">
                                                        <h2 class="text-white mb-3 f-w-300">222,586<i class="feather icon-arrow-down"></i></h2>
                                                        <span class="text-white mb-4 d-block">TOTAL EARNINGS</span>
                                                    </div>
                                                    <div class="tab-pane fade" id="earnings-wed" role="tabpanel" aria-labelledby="pills-earnings-wed">
                                                        <h2 class="text-white mb-3 f-w-300">859,745<i class="feather icon-arrow-up"></i></h2>
                                                        <span class="text-white mb-4 d-block">TOTAL EARNINGS</span>
                                                    </div>
                                                    <div class="tab-pane fade" id="earnings-thu" role="tabpanel" aria-labelledby="pills-earnings-thu">
                                                        <h2 class="text-white mb-3 f-w-300">785,684<i class="feather icon-arrow-up"></i></h2>
                                                        <span class="text-white mb-4 d-block">TOTAL EARNINGS</span>
                                                    </div>
                                                    <div class="tab-pane fade" id="earnings-fri" role="tabpanel" aria-labelledby="pills-earnings-fri">
                                                        <h2 class="text-white mb-3 f-w-300">123,486<i class="feather icon-arrow-down"></i></h2>
                                                        <span class="text-white mb-4 d-block">TOTAL EARNINGS</span>
                                                    </div>
                                                    <div class="tab-pane fade" id="earnings-sat" role="tabpanel" aria-labelledby="pills-earnings-sat">
                                                        <h2 class="text-white mb-3 f-w-300">762,963<i class="feather icon-arrow-up"></i></h2>
                                                        <span class="text-white mb-4 d-block">TOTAL EARNINGS</span>
                                                    </div>
                                                    <div class="tab-pane fade" id="earnings-sun" role="tabpanel" aria-labelledby="pills-earnings-sun">
                                                        <h2 class="text-white mb-3 f-w-300">984,632<i class="feather icon-arrow-down"></i></h2>
                                                        <span class="text-white mb-4 d-block">TOTAL EARNINGS</span>
                                                    </div>
                                                </div>
                                                <ul class="nav nav-pills align-items-center justify-content-center" id="pills-tab" role="tablist">
                                                    <li class="nav-item">
                                                        <a class="nav-link active" id="pills-earnings-mon" data-toggle="pill" href="#earnings-mon" role="tab" aria-controls="earnings-mon" aria-selected="true">Mon</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-earnings-tue" data-toggle="pill" href="#earnings-tue" role="tab" aria-controls="earnings-tue" aria-selected="false">Tue</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-earnings-wed" data-toggle="pill" href="#earnings-wed" role="tab" aria-controls="earnings-wed" aria-selected="false">Wed</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-earnings-thu" data-toggle="pill" href="#earnings-thu" role="tab" aria-controls="earnings-thu" aria-selected="false">Thu</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-earnings-fri" data-toggle="pill" href="#earnings-fri" role="tab" aria-controls="earnings-fri" aria-selected="false">Fri</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-earnings-sat" data-toggle="pill" href="#earnings-sat" role="tab" aria-controls="earnings-sat" aria-selected="false">Sat</a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a class="nav-link" id="pills-earnings-sun" data-toggle="pill" href="#earnings-sun" role="tab" aria-controls="earnings-sun" aria-selected="false">Sun</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card theme-bg2">
                                        <div class="card-block">
                                            <div class="row align-items-center justify-content-center">
                                                <div class="col-auto">
                                                    <img src="../assets/images/widget/shape5.png" alt="activity-user">
                                                </div>
                                                <div class="col">
                                                    <h2 class="text-white f-w-300">375</h2>
                                                    <h5 class="text-white">Sale Product</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [earning-day section] end -->

                                <!-- [ full width-table ] start -->
                                <div class="col-xl-8 col-md-6">
                                    <div class="card code-table">
                                        <div class="card-header">
                                            <h5>Full Width Table</h5>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block pb-0">
                                            <div class="table-responsive">
                                                <table class="table table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Id Number</th>
                                                            <th>Code</th>
                                                            <th>Date</th>
                                                            <th>Budget</th>
                                                            <th>Status</th>
                                                            <th class="text-right">Ratings</th>
                                                    </tr></thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <h6 class="mb-1">#467</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="mb-1">8765482</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">Nov 14, 2017</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">$ 874.23</h6>
                                                            </td>
                                                            <td><a href="#!" class="label theme-bg f-12 text-white">Active</a></td>
                                                            <td class="text-right"><a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <h6 class="mb-1">#466</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="mb-1">2366482</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">Nov 13, 2017</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">$ 235.34</h6>
                                                            </td>
                                                            <td><a href="#!" class="label theme-bg2 f-12 text-white">Not Active</a></td>
                                                            <td class="text-right"><a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                <h6 class="mb-1">#465</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="mb-1">8832638</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">Oct 14, 2017</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">$ 233.46</h6>
                                                            </td>
                                                            <td><a href="#!" class="label theme-bg f-12 text-white">Active</a></td>
                                                            <td class="text-right"><a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h6 class="mb-1">#464</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="mb-1">9632638</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">Dec 17, 2017</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">$ 133.46</h6>
                                                            </td>
                                                            <td><a href="#!" class="label theme-bg2 f-12 text-white">Not Active</a></td>
                                                            <td class="text-right"><a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <h6 class="mb-1">#463</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="mb-1">3332538</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">July 14, 2017</h6>
                                                            </td>
                                                            <td>
                                                                <h6 class="m-b-0">$ 244.46</h6>
                                                            </td>
                                                            <td><a href="#!" class="label theme-bg f-12 text-white">Active</a></td>
                                                            <td class="text-right"><a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-c-yellow"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                                <a href="#!"><i class="fa fa-star f-18 text-black-50"></i></a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ full width-table ] end -->

                                <!-- [ earning chart ] start -->
                                <div class="col-xl-4 col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Earnings</h5>
                                            <span class="d-block pt-2">Mon 15 - Sun 21</span>
                                            <div class="card-header-right">
                                                <div class="btn-group card-option">
                                                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="feather icon-more-horizontal"></i>
                                                    </button>
                                                    <ul class="list-unstyled card-option dropdown-menu dropdown-menu-right">
                                                        <li class="dropdown-item full-card"><a href="#!"><span><i class="feather icon-maximize"></i> maximize</span><span style="display:none"><i class="feather icon-minimize"></i> Restore</span></a></li>
                                                        <li class="dropdown-item minimize-card"><a href="#!"><span><i class="feather icon-minus"></i> collapse</span><span style="display:none"><i class="feather icon-plus"></i> expand</span></a></li>
                                                        <li class="dropdown-item reload-card"><a href="#!"><i class="feather icon-refresh-cw"></i> reload</a></li>
                                                        <li class="dropdown-item close-card"><a href="#!"><i class="feather icon-trash"></i> remove</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-block">
                                            <div class="earning-price mb-1">
                                                <h3 class="m-0 f-w-300">$894.39</h3>
                                            </div>
                                            <div id="Widget-line-chart1" class="WidgetlineChart " style="height: 245px; overflow: hidden; text-align: left;"><div class="amcharts-main-div" style="position: relative;"><div class="amcharts-chart-div" style="overflow: hidden; position: relative; text-align: left; width: 272px; height: 245px; padding: 0px; touch-action: auto;"><svg version="1.1" style="position: absolute; width: 272px; height: 245px; top: 0px; left: 0.328125px;"><desc>JavaScript chart by amCharts 3.21.5</desc><defs><filter x="-50%" y="-50%" width="200%" height="200%" id="blur"><feGaussianBlur in="SourceGraphic" stdDeviation="30"></feGaussianBlur></filter><filter id="shadow" x="-10%" y="-10%" width="120%" height="120%"><feOffset result="offOut" in="SourceAlpha" dx="0" dy="20"></feOffset><feGaussianBlur result="blurOut" in="offOut" stdDeviation="10"></feGaussianBlur><feColorMatrix result="blurOut" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 .2 0"></feColorMatrix><feBlend in="SourceGraphic" in2="blurOut" mode="normal"></feBlend></filter></defs><g><path cs="100,100" d="M0.5,0.5 L271.5,0.5 L271.5,244.5 L0.5,244.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-bg"></path><path cs="100,100" d="M0.5,0.5 L271.5,0.5 L271.5,224.5 L0.5,224.5 L0.5,0.5 Z" fill="#FFFFFF" stroke="#000000" fill-opacity="0" stroke-width="1" stroke-opacity="0" class="amcharts-plot-area" transform="translate(0,20)"></path></g><g><g class="amcharts-category-axis" transform="translate(0,20)"></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1557570389075" transform="translate(0,20)" visibility="visible"></g></g><g transform="translate(0,20)" clip-path="url(#AmChartsEl-19)"><g visibility="hidden"></g></g><g></g><g></g><g></g><g><g transform="translate(0,20)" class="amcharts-graph-line amcharts-graph-g1"><g></g><g clip-path="url(#AmChartsEl-21)"><path cs="100,100" d="M19.5,90.1 L58.5,112.5 L97.5,92.34 L136.5,101.3 L174.5,78.9 L213.5,101.3 L252.5,67.7 M0,0 L0,0" fill="none" stroke-width="3" stroke-opacity="1" stroke="#23d3d7" stroke-linejoin="round" class="amcharts-graph-stroke"></path></g><clipPath id="AmChartsEl-21"><rect x="0" y="0" width="271" height="224" rx="0" ry="0" stroke-width="0"></rect></clipPath><g></g></g></g><g></g><g><path cs="100,100" d="M0.5,224.5 L271.5,224.5 L271.5,224.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(0,20)" class="amcharts-axis-zero-grid-valueAxisAuto0_1557570389075 amcharts-axis-zero-grid"></path><g class="amcharts-category-axis"><path cs="100,100" d="M0.5,0.5 L271.5,0.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(0,224)" class="amcharts-axis-line"></path></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1557570389075"><path cs="100,100" d="M0.5,0.5 L0.5,224.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#000000" transform="translate(0,20)" class="amcharts-axis-line" visibility="visible"></path></g></g><g><g transform="translate(0,20)" clip-path="url(#AmChartsEl-20)" style="pointer-events: none;"><path cs="100,100" d="M0.5,0.5 L0.5,0.5 L0.5,224.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#fff" class="amcharts-cursor-line amcharts-cursor-line-vertical" visibility="hidden"></path><path cs="100,100" d="M0.5,0.5 L271.5,0.5 L271.5,0.5" fill="none" stroke-width="1" stroke-opacity="0" stroke="#fff" class="amcharts-cursor-line amcharts-cursor-line-horizontal" visibility="hidden"></path></g><clipPath id="AmChartsEl-20"><rect x="0" y="0" width="271" height="224" rx="0" ry="0" stroke-width="0"></rect></clipPath></g><g></g><g><g transform="translate(0,20)" class="amcharts-graph-line amcharts-graph-g1"></g></g><g><g></g></g><g><g class="amcharts-category-axis" transform="translate(0,20)" visibility="visible"><text y="8" fill="#393c40" font-family="Verdana" font-size="15px" opacity="1" text-anchor="middle" transform="translate(19,185.5)" class="amcharts-axis-label"><tspan y="8" x="0">Mon</tspan></text><text y="8" fill="#393c40" font-family="Verdana" font-size="15px" opacity="1" text-anchor="middle" transform="translate(58,185.5)" class="amcharts-axis-label"><tspan y="8" x="0">Tue</tspan></text><text y="8" fill="#393c40" font-family="Verdana" font-size="15px" opacity="1" text-anchor="middle" transform="translate(97,185.5)" class="amcharts-axis-label"><tspan y="8" x="0">Wed</tspan></text><text y="8" fill="#393c40" font-family="Verdana" font-size="15px" opacity="1" text-anchor="middle" transform="translate(136,185.5)" class="amcharts-axis-label"><tspan y="8" x="0">Thu</tspan></text><text y="8" fill="#393c40" font-family="Verdana" font-size="15px" opacity="1" text-anchor="middle" transform="translate(174,185.5)" class="amcharts-axis-label"><tspan y="8" x="0">Fri</tspan></text><text y="8" fill="#393c40" font-family="Verdana" font-size="15px" opacity="1" text-anchor="middle" transform="translate(213,185.5)" class="amcharts-axis-label"><tspan y="8" x="0">Sat</tspan></text><text y="8" fill="#393c40" font-family="Verdana" font-size="15px" opacity="1" text-anchor="middle" transform="translate(252,185.5)" class="amcharts-axis-label"><tspan y="8" x="0">Sun</tspan></text></g><g class="amcharts-value-axis value-axis-valueAxisAuto0_1557570389075" transform="translate(0,20)" visibility="visible"><text y="0" fill="#000000" font-family="Verdana" font-size="0px" opacity="1" text-anchor="start" transform="translate(9,223)" class="amcharts-axis-label"><tspan y="0" x="0">0</tspan></text><text y="0" fill="#000000" font-family="Verdana" font-size="0px" opacity="1" text-anchor="start" transform="translate(9,178)" class="amcharts-axis-label"><tspan y="0" x="0">20</tspan></text><text y="0" fill="#000000" font-family="Verdana" font-size="0px" opacity="1" text-anchor="start" transform="translate(9,133)" class="amcharts-axis-label"><tspan y="0" x="0">40</tspan></text><text y="0" fill="#000000" font-family="Verdana" font-size="0px" opacity="1" text-anchor="start" transform="translate(9,89)" class="amcharts-axis-label"><tspan y="0" x="0">60</tspan></text><text y="0" fill="#000000" font-family="Verdana" font-size="0px" opacity="1" text-anchor="start" transform="translate(9,44)" class="amcharts-axis-label"><tspan y="0" x="0">80</tspan></text><text y="0" fill="#000000" font-family="Verdana" font-size="0px" opacity="1" text-anchor="start" transform="translate(9,-1)" class="amcharts-axis-label"><tspan y="0" x="0">100</tspan></text></g></g><g></g><g transform="translate(0,20)"></g><g></g><g></g><clipPath id="AmChartsEl-19"><rect x="-1" y="-1" width="273" height="226" rx="0" ry="0" stroke-width="0"></rect></clipPath></svg><a href="http://www.amcharts.com/javascript-charts/" title="JavaScript charts" style="position: absolute; text-decoration: none; color: rgb(0, 0, 0); font-family: Verdana; font-size: 11px; opacity: 0.7; display: block; left: 5px; top: 25px;">JS chart by amCharts</a></div></div></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ earning chart ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>