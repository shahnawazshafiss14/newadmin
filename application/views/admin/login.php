<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title><?= COMPANYNAME; ?> Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!-- fontawesome icon -->
    <link rel="stylesheet" href="<?php echo base_url('assets/fonts/fontawesome/css/fontawesome-all.min.css');?>">
    <!-- animation css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/plugins/animation/css/animate.min.css');?>">
    <!-- vendor css -->
	  <link rel="stylesheet" href="<?php echo base_url('assets/plugins/pnotify/css/pnotify.custom.min.css');?>">
    <!-- pnotify-custom css -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/pages/pnotify.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css');?>">
	

</head>
<body>
    <div class="auth-wrapper">
            <div class="auth-content subscribe">
                <div class="card">
                    <div class="row no-gutters">
                        <div class="col-md-4 col-lg-6 d-none d-md-flex d-lg-flex theme-bg align-items-center justify-content-center">
                            <img src="<?php echo base_url('assets/images/user/lock.png')?>" alt="lock images" class="img-fluid">
                        </div>
                        <div class="col-md-8 col-lg-6">
                            <div class="card-body text-center">
                                <div class="row justify-content-center">
                                    <div class="col-sm-10">
                                        <h3 class="mb-4">Login</h3>
										<form class="form-signin" action="<?php echo site_url(ADMIN_URL.'/login/auth');?>" method="post">
										<?php echo $this->session->flashdata('msg');?>
                                        <div class="input-group mb-3">
                                            <input type="text" name="email" id="email" class="form-control" placeholder="Username">
                                        </div>
                                        <div class="input-group mb-4">
                                            <input type="password" name="password" id="password"  class="form-control" placeholder="Password">
                                        </div>
                                        <button type="submit" id="btnLogin" name="btnLogin" class="btn btn-primary shadow-2 mb-4">Login</button>
										</form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <!-- Required Js -->
      <script src="<?php echo base_url('assets/js/vendor-all.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/bootstrap/js/bootstrap.min.js');?>"></script>
	 <script src="<?php echo base_url('assets/plugins/data-tables/js/datatables.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/pages/tbl-datatable-custom.js');?>"></script>
	
    <script src="<?php echo base_url('assets/js/pcoded.min.js');?>"></script>
	<script src="<?php echo base_url('assets/plugins/pnotify/js/pnotify.custom.min.js');?>"></script>
    <script src="<?php echo base_url('assets/plugins/pnotify/js/notify-event.js');?>"></script>
	<script>
	   $(document).ready(function() {
		   var erroradd = 'animated shake errors';
		   $('#btnLogin').click(function(){
			   var email = $('#email').val();
			   var password = $('#password').val();
			   if($.trim(email) == ''){
				   $('#email').addClass(erroradd);
				   setTimeout(function () {
					   $('#email').removeClass(erroradd);
					}, 2000);
				   return false;
			   }else if($.trim(password) == ''){
				   $('#password').addClass(erroradd);
				   setTimeout(function () {
					   $('#password').removeClass(erroradd);
					}, 2000);
				   return false;
			   }else{
				   return true;
			   }
		   });
			
			
        });
	</script>
</body>
</html>
