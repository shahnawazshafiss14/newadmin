  
    <!-- [ Main Content ] start -->
    <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->
                    <div class="page-header">
                         
                            <div class="row">
							 <div class="col-sm-12">
                                 <form id="frmconsigment" name="frmconsigment" action="<?= U_A_URL; ?>reports/totaltopaid" method="get">
                                    <div class="col-xl-4 col-md-4">
												<div class="card">
													<div class="card-header">
														<h5>To</h5>
													</div>
													<div class="card-block">
														<input type="text" class="form-control datepicker" data-date-format="yyyy-mm-dd" id="to" name="to" value="<?= $start_date; ?>">
													</div>
												</div>
											</div>
											<div class="col-xl-4 col-md-4">
												<div class="card">
													<div class="card-header">
														<h5>From</h5>
													</div>
													<div class="card-block">
														<input type="text" class="form-control datepicker" id="from" name="from" value="<?= $end_date; ?>">
													</div>
												</div>
											</div>
											<div class="col-xl-4 col-md-4">
												<div class="card"> 
													<div class="card-block">
														<button type="submit" class="btn btn-primary" id="btnSubmitcongisment">Submit</button> 
													</div>
												</div>
											</div>
											</form> 
                                   
                                </div>
                            </div>
                         
                    </div>
                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <!-- [ Main Content ] start -->
                            <div class="row">
                               
                                <!-- [ form-element ] start -->
                                <div class="col-sm-12">
                                    <div class="card">
                                         
                                        <div class="card-body">
                                            
                                             <!-- [ configuration table ] start -->
                               
                                        <div class="card-block">
                                            <div class="table-responsive">
                                                <table id="key-act-button" class="display table nowrap table-striped table-hover" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Consignment Number</th>
                                                            <th>Bilty Date</th>
                                                            <th>Create Date</th>
                                                            <th>Consignee</th>
                                                            <th>Consigner</th>  
                                                            <th>Amount</th>  
                                                        </tr>
                                                    </thead>
                                                    <tbody>
													<?php 
													$nettoal = 0;
													foreach($consignments as $congi):
													 $nettoal += $congi->nettotal;
													?>
                                                        <tr>
                                                            <td><a title="Print/view" target="_blank" href="<?= base_url(ADMIN_URL.'/consignment/details/'.$congi->consignment_slug)?>">AMB<?= $congi->consignment_id; ?></a></td>
                                                            <td><?= $congi->bilty_date; ?></td>
                                                            <td><?= $congi->created_date; ?></td>
                                                            <td><?= $this->client_model->find_by_name($congi->consignee_name); ?></td>
                                                            <td><?= $this->client_model->find_by_name($congi->consigner); ?></td> 
                                                            <td><?= $congi->nettotal; ?></td> 
															 
                                                        </tr>
                                                     <?php 
													 endforeach;
													 ?>  
													<tr>
														<td></td>
														<td></td>
														<td></td>
														<td></td>
														<td style="text-align:right; font-weight: bold"><strong>Total</strong></td>
														<td><strong><?= $nettoal; ?></strong></td> 
                                                    </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th>Consignment Number</th>
                                                            <th>Bilty Date</th>
                                                            <th>Create Date</th>
                                                            <th>Consignee</th>
                                                            <th>Consigner</th> 
                                                            <th>Amount</th> 
															 
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                               
                                            </div> 
												 
                                                </div> 
                                            </div>   
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- [ form-element ] end -->
                            </div>
                            <!-- [ Main Content ] end -->
                        </div>
                  