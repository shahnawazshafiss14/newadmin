<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Listiry</title>
  <link href="https://fonts.googleapis.com/css?family=Cabin:400,600" rel="stylesheet">
  <link rel="stylesheet" href="<?= U_F_ASST; ?>styles/style.css">
</head>

<body>
<header id="masthead" class="site-header site-header--layout-1 site-header--fluid site-header--absolute">
  <div class="d-lg-flex justify-content-lg-between align-items-lg-center site-header__container">
    <div class="d-lg-flex align-items-lg-center">
      <div class="site-header__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Temper Wala</h1>
          <img src="<?= U_F_ASST; ?>images/logo-1.png" alt="Listiry">
        </a>
      </div><!-- .site-header__logo -->

     
    </div>

    <div class="d-lg-flex align-items-lg-center">
      <ul class="min-list main-navigation main-navigation--white">
        <li>
          <a href="#">Home</a>
          
        </li>
        <li>
          <a href="#">Products</a>
          
        </li>
        <li>
          <a href="#">About us</a>
          
        </li>
        <li>
          <a href="#">Contact us</a>
          
        </li>
        <li>
          <a href="#">Privacy Policy</a>
        </li>
      </ul><!-- .main-navigation -->

      
    </div>

    <div class="d-lg-none nav-mobile">
      <a href="#" class="nav-toggle js-nav-toggle nav-toggle--white">
        <span></span>
      </a><!-- .nav-toggle -->
    </div><!-- .nav-mobile -->
  </div><!-- .site-header__container -->
</header><!-- #masthead -->
<section class="page-banner page-banner--layout-1 parallax">
  <div class="container">
    <div class="page-banner__container animated fadeInUp">
      <div class="page-banner__textcontent t-center">
        <h2 class="page-banner__title c-white">Explore Your City Gems</h2>
        <p class="page-banner__subtitle c-white">Find the best places to eat, drink, or visit.</p>
      </div><!-- .page-banner__textcontent -->

      <div class="main-search-container">
        <form class="main-search main-search--layout-1 bg-mirage">
          <div class="main-search__group main-search__group--primary">
            <label for="main-search-name" class="c-white">What?</label>
            <input type="text" name="name" id="main-search-name" class="form-input" placeholder="restaurant, hotel, club...">
          </div><!-- .main-search__group -->

          
          <div class="main-search__group main-search__group--tertiary">
            <button type="submit" class="button button--medium button--square button--primary">
              <i class="fa fa-search"></i> Search
            </button>
          </div>
        </form>
      </div><!-- .main-search-container -->

    </div><!-- .page-banner__container -->
  </div><!-- .container -->
</section><!-- .page-banner -->

<section class="listing-list page-section listing-list--layout-1">
  <div class="container">
    <h2 class="page-section__title t-center">Great Places</h2>
    <div class="row">
	<?php 
	$arr_prod = array(
		'view_status' => '1'
	   );
	$products =$this->product_model->viewRecordAnyR($arr_prod);
	foreach($products as $prod):
	?>
      <div class="col-md-3">
        <div class="listing hover-effect">
          <div class="listing__wrapper">
            <div class="listing__thumbnail">
              <a href="single-listing-1.html">
                <img src="<?= U_A_ASST; ?>images/<?= $prod->p_image; ?>" alt="April Boutique Coffee">
                <span class="label label--primary"><?= $this->category_model->find_by_name($prod->p_category); ?></span>
              </a>
            </div>
            <div class="listing__detail">
              <h3 class="listing__title">
                <a href="<?= $prod->p_slug; ?>"><?= $prod->p_name; ?></a>
              </h3>
              <p class="listing__location c-dusty-gray">
                <i class="fa fa-map-marker"></i>
                <?= $this->zone_model->find_by_name($prod->p_location); ?>
              </p>
             
            </div>
          </div>
        </div>
      </div>
	 <?php 
	 endforeach;
	 ?>  

     

      
    </div><!-- .row -->
  </div><!-- .container -->
</section><!-- .instruction-container -->
<section class="page-section testimonials-container testimonial--layout-1">
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="testimonials">
          <h2 class="page-section__title">25,000+ Happy Clients</h2>
          <div class="swiper-container testimonial-container">
            <div class="swiper-wrapper testimonial-wrapper">
              <div class="swiper-slide testimonial">
                <p class="testimonial-content c-dove-gray">
                  Mitch and I have traveled all over but this was by far one of the best vacations I have been on.
                  Mitch will be writing you about future trips. Make sure Mitch books business class or first class in the future.
                </p>
                <div class="testimonial-footer">
                  <div class="testimonial-avatar">
                    <img src="<?= U_F_ASST; ?>images/uploads/testimonial-avatar.png"
                    class="Client Avatar"
                    alt="Testimonial Image"
                    >
                  </div>

                  <div class="testimonial-client">
                    <span class="testimonial-client-name">- Roberta V</span>
                    <span class="testimonial-client-location">India</span>
                  </div>
                </div><!-- .testimonial-footer -->
              </div><!-- .testimonial -->

              <div class="swiper-slide testimonial">
                <p class="testimonial-content c-dove-gray">
                  Mitch and I have traveled all over but this was by far one of the best vacations I have been on.
                  Mitch will be writing you about future trips. Make sure Mitch books business class or first class in the future.
                </p>
                <div class="testimonial-footer">
                  <div class="testimonial-avatar">
                    <img
                      src="<?= U_F_ASST; ?>images/uploads/testimonial-avatar.png"
                      class="Client Avatar" alt="Testimonial Image"
                    >
                  </div>

                  <div class="testimonial-client">
                    <span class="testimonial-client-name">- Roberta V</span>
                    <span class="testimonial-client-location">India</span>
                  </div>
                </div><!-- .testimonial-footer -->
              </div><!-- .testimonial -->

              <div class="swiper-slide testimonial">
                <p class="testimonial-content c-dove-gray">
                  Mitch and I have traveled all over but this was by far one of the best vacations I have been on.
                  Mitch will be writing you about future trips. Make sure Mitch books business class or first class in the future.
                </p>
                <div class="testimonial-footer">
                  <div class="testimonial-avatar">
                    <img
                      src="<?= U_F_ASST; ?>images/uploads/testimonial-avatar.png"
                      class="Client Avatar" alt="Testimonial Image"
                    >
                  </div>

                  <div class="testimonial-client">
                    <span class="testimonial-client-name">- Roberta V</span>
                    <span class="testimonial-client-location">India</span>
                  </div>
                </div><!-- .testimonial-footer -->
              </div><!-- .testimonial -->
            </div><!-- .testimonial-wrapper -->

            <div class="testimonial-button-container">
              <span class="ion-chevron-left testimonial-button testimonial-button-left"></span>
              <span class="ion-chevron-right testimonial-button testimonial-button-right"></span>
            </div>
          </div><!-- .testimonial-container -->
        </div><!-- .testimonials -->
      </div><!-- .col -->
    </div><!-- .row -->
  </div><!-- .container -->
</section><!-- .testimonial -->
<section class="ads ads--layout-1 parallax">
  <div class="t-center ads__container">
    <div class="container">
      <h2 class="ads__title">Explore exciting destinations</h2>
      <p class="ads__subtitle">
        Preparing for your traveling is very important. Our book store has lots of e-books, it might be helpful for you
      </p>
    </div>
  </div>
</section><!-- .ads -->




<footer id="colophone" class="site-footer">
  <div class="t-center site-footer__primary">
    <div class="container">
      <div class="site-footer__logo">
        <a href="index-2.html">
          <h1 class="screen-reader-text">Listiry</h1>
          <img src="<?= U_F_ASST; ?>images/logo-footer.png" alt="Listiry">
        </a>
      </div>
      <p class="t-small">Listiry is making finding destination faster, easier, and customized for you.</p>
      <ul class="min-list inline-list site-footer__links site-footer__social">
        <li>
          <a href="#">
            <i class="fa fa-facebook-f"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-twitter"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-pinterest"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-google-plus"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-linkedin"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-instagram"></i>
          </a>
        </li>
        <li>
          <a href="#">
            <i class="fa fa-youtube"></i>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <!-- .site-footer__primary -->

  <div class="site-footer__secondary">
    <div class="container">
      <div class="site-footer__secondary-wrapper">
        <p class="site-footer__copyright">&copy; 2018
          <span class="c-secondary">Listiry</span> by Felix. All Rights Reserved.</p>
        <ul class="min-list inline-list site-footer__links site-footer__details">
          <li>
            <a href="tel:+0987654321">Tel: +098 765 4321</a>
          </li>
          <li>
            <a href="#">Get this theme</a>
          </li>
          <li>
            <a href="#">About</a>
          </li>
          <li>
            <a href="#">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- .site-footer__secondary -->
</footer><!-- #colophone -->
<script src="<?= U_F_ASST; ?>scripts/app.js"></script>
</body>
</html>
