<?php 
class Admin_Controller extends MY_Controller{
	var $acc_id;
	var $acc_level;
	var $acc_zone;
	public function __construct(){
		parent::__construct();
		$this->acc_id = $this->session->userdata('acc_id');
		$this->acc_level = $this->session->userdata('level');
		$this->acc_zone = $this->session->userdata('zone_id');
		if($this->session->userdata('logged_in') !== TRUE){
			redirect(site_url(ADMIN_URL.'/login'));
		}
	}
}

?>