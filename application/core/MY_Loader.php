<?php
class MY_Loader extends CI_Loader {
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view(ADMIN_URL.'/include/header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view(ADMIN_URL.'/include/footer', $vars, $return);

        return $content;
    else:
        $this->view(ADMIN_URL.'/include/header', $vars);
        $this->view($template_name, $vars);
        $this->view(ADMIN_URL.'/include/footer', $vars);
    endif;
    }
}
?>