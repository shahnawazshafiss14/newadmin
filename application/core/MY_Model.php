<?php

class MY_Model extends CI_Model {

    const tableName = 'abstract';
    const tableName_pk = 'abstract';
    const tableName_username = 'username';

    function __construct() {
	parent::__construct();
    }

    public function recordInsert($data) {
	$this->db->insert($this::tableName, $data);
	$this->{$this::tableName_pk} = $this->db->insert_id();
    }

    public function viewRecordAll() {
	$this->db->order_by($this::tableName_pk, 'DESC');
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }
	public function viewRecordLast() {
	$this->db->order_by($this::tableName_pk, 'DESC');
	$querySql = $this->db->get($this::tableName);
	return $querySql->row();
    }

    public function viewRecordId($id) {
		$this->db->where($this::tableName_pk, $id);
		$querySql = $this->db->get($this::tableName);
		return $querySql->row();
    }

    public function viewRecordIdR($id) {
	$this->db->where($this::tableName_pk, $id);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function viewRecordAnyRLt($data) {
	$this->db->where($data);
	$this->db->limit(10);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function viewRecordAnyRL($data) {
	$this->db->where($data);
	$this->db->limit(20);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function record_count() {
	return $this->db->count_all($this::tableName);
    }

    public function record_countw($data) {
	$this->db->where($data);
	$this->db->from($this::tableName);
	return $this->db->count_all_results();
    }

    public function recordCheckAvaibility($data) {
	$this->db->where($data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->num_rows();
    }

    public function viewRecordAny($data) {
	$this->db->where($data);
	$querySql = $this->db->get($this::tableName);
	return $querySql->row();
    }
	public function viewRecordGC($data, $select) {
		$this->db->select($select);	
		$this->db->from($this::tableName);
		$this->db->where_in($data);
		$this->db->order_by($this::tableName_pk, 'DESC');
		$querySql = $this->db->get();
		return $querySql->row();
    }
	public function viewRecordGCIN($select) {
		$this->db->select($select);	
		$this->db->order_by($this::tableName_pk, 'DESC');
		$querySql = $this->db->get();
		return $querySql->row();
    }
	public function viewRecordIN($select, $colunmn) {
		$this->db->select($select);	
		$this->db->order_by($this::tableName_pk, 'DESC');
		$querySql = $this->db->get();
		$results = $querySql->result();
		$res_arr = array();
		foreach($results as $result){
			$res_arr[] = $result->$colunmn;
		}
		return implode(', ', $res_arr);
    }
	public function viewRecordGCINR($select) {
		$this->db->select('*');
		$this->db->select($select);	
		$this->db->order_by($this::tableName_pk, 'DESC');
		$querySql = $this->db->get();
		return $querySql->result();
    }
	public function viewRecordGCINRS($select) {
		$this->db->select('*');
		$this->db->select($select);	
		$this->db->order_by($this::tableName_pk, 'DESC');
		$querySql = $this->db->get();
		return $querySql->row();
    }
    public function viewRecordAnyRorder($data, $ordercol, $orderase) {
	$this->db->where($data);
	$this->db->order_by($ordercol, $orderase);
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function viewRecordAnyR($data) {
	$this->db->where($data);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }
	public function viewRecordAnyRG($data, $group) {
	$this->db->where($data);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$this->db->group_by($group);
	$querySql = $this->db->get($this::tableName);
	return $querySql->result();
    }

    public function viewRecordPage($limit, $start) {
	$this->db->order_by($this::tableName_pk, 'DESC');
	$this->db->limit($limit, $start);
	$query = $this->db->get($this::tableName);

	if ($query->num_rows() > 0) {
	    foreach ($query->result() as $row) {
		$data[] = $row;
	    }
	    return $data;
	}
	return false;
    }

    public function viewRecordPageR($data, $limit, $start) {
	$this->db->where($data);
	$this->db->order_by($this::tableName_pk, 'DESC');
	$this->db->limit($limit, $start);
	$query = $this->db->get($this::tableName);
	return $query->result();
    }

    public function recordUpdate($id, $data) {
	$this->db->where($this::tableName_pk, $id);
	$this->db->update($this::tableName, $data);
    }
	public function recordUpdateAny($id, $data) {
	$this->db->where($id);
	$this->db->update($this::tableName, $data);
    }
	public function recordUpdateSlug($slugname,$id, $data) {
	$this->db->where($slugname,$id);
	$this->db->update($this::tableName, $data);
    }

    public function recordDelete($data) {
	$this->db->delete($this::tableName, $data);
    }
	public function recordDelete1($tablename, $data) {
	$this->db->delete($tablename, $data);
    }

}

?>