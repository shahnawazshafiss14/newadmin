<?php 
class Customer_model extends MY_Model{
	const tableName = 'tbl_customer';
	 const tableName_pk = 'id';
	 public function __construct(){
			parent::__construct();
	 }
	 
	public function find_by_name($id){
		$this->db->where($this::tableName_pk, $id);
		$querySql = $this->db->get($this::tableName);
		return $querySql->row('category_name');
	}
	public function find_by_row($id){
		$this->db->where($this::tableName_pk, $id);
		$querySql = $this->db->get($this::tableName);
		return $querySql->row();
	}
}
?>