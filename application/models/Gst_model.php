<?php
class Gst_model extends MY_Model{
 
  const tableName = 'tbl_gsts';
  const tableName_pk = 'id';
    

    public function __construct() {
		parent::__construct();
    }
	
  public function insert($data){
	$this->db->insert('tbl_gsts', $data);
  }
  
   public function get_gsts()
   {
    $this->db->select('id,gst_rate');
	$this->db->order_by('id desc'); 
    $q = $this->db->get('tbl_gsts');   
    if($q->num_rows() > 0)
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
  }
  
}