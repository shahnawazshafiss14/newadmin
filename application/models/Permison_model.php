<?php
class Permison_model extends MY_Model{
 
const tableName = 'tbl_permision';
const tableName_pk = 'id';
    

    public function __construct() {
		parent::__construct();
    }
	public function viewRecordIdUserId($data) {
		$this->db->where($data);
		$this->db->select('*');
		$this->db->from('tbl_permision');
		$this->db->join('tbl_top_menu', 'tbl_top_menu.menu_id = tbl_permision.top_menu_id');
		$this->db->join('tbl_users', 'tbl_users.acc_id = tbl_permision.acc_id');
		$query = $this->db->get();
		return $query->result();
    }
 
}