<?php 
class Consignment_model extends MY_Model{
	const tableName = 'tbl_consignment';
	const tableName_pk = 'consignment_id';
	const tableName_slug = 'consignment_slug';
	 public function __construct(){
			parent::__construct();
	 }
	 
	 public function viewRecordAnyjion($id){
		 $this->db->select('*');
		 $this->db->select('tbl_consignment.acc_id as account_id');
		 $this->db->from('tbl_consignment');
		 $this->db->where($id);
		 $this->db->join('tbl_gatepass','tbl_consignment.consignment_id=tbl_gatepass.consigment_id', 'left');
		 $this->db->join('tbl_consig_payments','tbl_consignment.consignment_id=tbl_consig_payments.consignement_id', 'left');
		 $query = $this->db->get();
		 return $query->row();
	 }
	 public function viewRecordAnyjoinCh($id){
		$this->db->select('*'); 
		$this->db->select('tbl_consignment.acc_id as account_id');
		$this->db->select('tbl_challan.challan_no as challan');
		$this->db->order_by('consignment_id', 'DESC');
		$this->db->from('tbl_consignment');
		$this->db->where($id);
		$this->db->join('tbl_challan', 'tbl_consignment.challan_no=tbl_challan.challan_no');
		$query = $this->db->get();
		return $query->result();
		
	 }
	 public function viewRecordAnyjoincsi($id){
		$this->db->select('*'); 
		$this->db->order_by('consignment_id', 'DESC');
		$this->db->from('tbl_consignment');
		$this->db->where($id);
		$this->db->join('tbl_users', 'tbl_consignment.acc_id=tbl_users.acc_id');
		$query = $this->db->get();
		return $query->row();
		
	 }
	 
	 
}
?>