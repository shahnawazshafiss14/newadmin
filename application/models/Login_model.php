<?php
class Login_model extends MY_Model{
 
 const tableName = 'tbl_users';
  const tableName_pk = 'user_id';
    

    public function __construct() {
		parent::__construct();
    }
	
  public function validate($email,$password){
    $this->db->where('user_name',$email);
    $this->db->where('user_password',$password);
    $result = $this->db->get('tbl_users',1);
    return $result;
  }
  public function find_by_user($acc_id){
    $this->db->where('acc_id',$acc_id);
    $result = $this->db->get('tbl_users',1);
    return $result->row('user_name');
  }
  public function find_by_row($acc_id){
    $this->db->where('acc_id',$acc_id);
    $result = $this->db->get('tbl_users',1);
    return $result->row();
  }
  public function find_by_userfull($acc_id){
    $this->db->where('acc_id',$acc_id);
    $result = $this->db->get('tbl_users',1);
    return $result->row('user_fullname');
  }
  
 
}