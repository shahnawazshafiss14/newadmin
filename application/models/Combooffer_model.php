<?php 
class Combooffer_model extends MY_Model{
	const tableName = 'tbl_combo_offer';
	 const tableName_pk = 'id';
	 public function __construct(){
			parent::__construct();
	 }
	 public function find_by_name($id){
		$this->db->where($this::tableName_pk, $id);
		$querySql = $this->db->get($this::tableName);
		return $querySql->row('combo_name');
	}
}
?>