<?php 
class zone_model extends MY_Model{
	const tableName = 'tbl_zones';
	 const tableName_pk = 'id';
	 public function __construct(){
			parent::__construct();
	 }
	 public function find_by_name($id){
		$this->db->where($this::tableName_pk, $id);
		$querySql = $this->db->get($this::tableName);
		return $querySql->row('zone_name');
	}
}
?>