<?php
class Pimages_model extends MY_Model{
	
 const tableName = 'tbl_product_images';
 const tableName_pk = 'id';
    

    public function __construct() {
		parent::__construct();
    }
	
  public function insert($data){
	$this->db->insert('tbl_products', $data);
  }
  public function find_by_name($id){
	   $this->db->where('id',$id);
	  $result = $this->db->get('tbl_products',1);
		return $result->row('p_name');
  }
  
   public function get_products()
   {
    $this->db->select('id,p_name');
	$this->db->order_by('id desc'); 
    $q = $this->db->get('tbl_products');   
    if($q->num_rows() > 0)
    {
      foreach ($q->result() as $row)
      {
        $data[] = $row;
      }
      return $data;
    }
  }
  
}