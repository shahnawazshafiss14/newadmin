<?php

function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {
    $theta = $longitude1 - $longitude2;
    $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
    $distance = acos($distance);
    $distance = rad2deg($distance);
    $distance = $distance * 60 * 1.1515;
    switch ($unit) {
        case 'Mi': break;
        case 'Km' : $distance = $distance * 1.609344;
    }
    return (round($distance, 2));
}

function generateid(){
	return 'TEM'.time();
}
 
function dateFormateYmd($date) {
    $arr = explode('/', $date);
    $newDate = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
    //$create = date_create($date);
    return $newDate;
}

function dateFormatedmY($date) {
    $create = date_create($date);
    return date_format($create, "d-m-Y");
}
function dateFormatedmYs($date) {
    $create = date_create($date);
    return date_format($create, "d/m/Y");
}
function dateymd($month_from) {
    return date("Y-m-d", strtotime($month_from));
}

function datedmy($month_from) {
    return date("d-m-Y", strtotime($month_from));
}

function datedmymonth($month_from) {
    return date("d-M-Y", strtotime($month_from));
}
function challan_add($ids){
	$ids = explode(',', $ids);
	$imas = [];
	foreach($ids as $challan){
		$imas[] = 'AMB'.$challan;
	}
	echo implode(', ', $imas);
} 

function distance($lat1, $lon1, $lat2, $lon2, $unit) {

    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);

    if ($unit == "K") {
        return ($miles * 1.609344);
    } else if ($unit == "N") {
        return ($miles * 0.8684);
    } else {
        return (round($miles, 2));
    }
}

function nagitive_check($value, $reture1, $reture2) {
    if (isset($value)) {
        if (substr(strval($value), 0, 1) == "-") {
            return $reture1;
        } else {
            return $reture2;
        }
    }
}

function strreplace($string) {
    $str = preg_replace('/[^0-9a-zA-Z]/', "-", $string);
    $str = str_replace("--", "-", $str);
    $str = str_replace("---", "-", $str);
    $str = str_replace("----", "-", $str);
    return strtolower($str);
}

function seturl($string, $id) {
    $uniquid = uniqid();
    $str = preg_replace('/[^0-9a-zA-Z]/', "-", $string);
    $str = str_replace("--", "-", $str);
    $str = str_replace("---", "-", $str);
    $str = str_replace("----", "-", $str);
    return strtolower($str . '-' . base64_encode($id) . $uniquid);
}

function priceqty($price, $qty) {
    $output = $price * $qty;
    return $output;
}
function challan_status($id){
	$arr = array(
	'0' => 'Processing',
	'1' => ''
	);
	return $arr[$id];
}
function paid_by($id){
	$arr = array(
		'1' => 'To Pay',
		'2' => 'Paid'
	);
	return $arr[$id];
}

function consig_type($id){
	$arr = array(
		'1' => 'Consignee',
		'2' => 'Consignor'
	);
	return $arr[$id];
}

function consignment_good($consigment_id, $type){
	 $CI = &get_instance();
	 $array_good = array(
		'consignment_id' => $consigment_id
	 ); 
	 $fetchquestc = $CI->consignmentgoods_model->viewRecordAnyR($array_good);
	 $asub_a = 0;
	 foreach($fetchquestc as $consig){
		 if($type == 'nug'){
			$asub_a += $consig->order_nugs; 
		 }else{
			$asub_a += $consig->order_nug_weight; 
		 }
		 
	 }
	 return $asub_a; 
}
function amb($id){
	$CI = &get_instance();
	$array_good = array(
		'consignment_id' => $id
	 ); 
	 $fetchquestc = $CI->consignment_model->viewRecordAnyjoincsi($array_good);
	 return $fetchquestc->user_zone_name.$fetchquestc->consignment_id;
}

function inrcurr($number) {
    return number_format($number, 2);
}
function check_permisson($menu_id){
	$CI = & get_instance();
	$accid = $CI->session->userdata('acc_id');
	$find_user = array(
			'acc_id' => $accid,
			'top_menu_id' => $menu_id
		);
	$fetch_user = $CI->permison_model->viewRecordAny($find_user);
	return $fetch_user;
} 
function payment_mode_type($id){
	$arr_type = array(
		'1' => 'Cash',
		'2' => 'Online'
	);
	return $arr_type[$id];
}
function status($id){
	$array_id = array(
		'1' => 'Active',
		'0' => 'inActive'
	);
	return $array_id[$id];
}
function status1($id){
	$arr = array(
		'1' => 'Yes',
		'0' => 'No'
	);
	return $arr[$id];
}
function payment_mode_type1($id){
	$arr_type = array(
		'1' => 'Cash',
		'2' => 'Check'
	);
	return $arr_type[$id];
}

function role_admin_type($id){
	$arr_type = array(
		'1' => 'Admin',
		'2' => 'Transporter',
		'3' => 'Employee'
	);
	return $arr_type[$id];
}
function o_status($id){
	$arr_type = array(
	'1' => 'Processing',
	'2' => 'Delivered',
	'3' => 'Returned'
	);
	return $arr_type[$id];
} 

/*
function itemtype() {
    $CI = & get_instance();
    $dataout = '';
    $fetarc = array(
        'parent_particular !=' => '0',
        'view_status' => '1'
    );
    $fetda = $CI->itemtype_model->viewRecordAnyR($fetarc);
    foreach ($fetda as $fevenue) {
        $dataout .= '<option value="' . $fevenue->item_id . '">' . $fevenue->item_particular_name . ', ' . $fevenue->item_hsn . '</option>';
    }
    return $dataout;
}
*/


?>
