<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Customer extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();  
        $this->methods['customers_create_post']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['customers_find_mobile_post']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['customers_add_membercard_post']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['customers_add_membercard_list_post']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['customers_find_membercard_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['customers_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['customers_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
	public function customers_create_post(){
		$cust_name = $this->post('cust_name');
		$cust_mobile = $this->post('cust_mobile');
		$acc_id = $this->post('acc_id');
		$cust_dob = $this->post('cust_dob');
		$membercard = $this->post('membercard');
		 
		$post_arr = [];
		if(!empty($cust_name) && !empty($cust_mobile)){
			$arr_fetch = array(
				'cust_name' => $cust_name,
				'cust_mobile' => $cust_mobile
			);
			$fetch_user = $this->customer_model->viewRecordAny($arr_fetch);
			if(count($fetch_user) > 0){ 
				$post_arr['status'] = '2';
				$post_arr['message'] = 'Customer Already registing with us';
				$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND); 
			}else{
				$arr_inst = array(
					'cust_name' => $cust_name,
					'acc_id' => $acc_id,
					'cust_dob' => $cust_dob,
					'cust_mobile' => $cust_mobile,
					'membership_card_no' => !empty($membercard) ? $membercard : '0'
				);
				$this->customer_model->recordInsert($arr_inst);
				$last_inserted = $this->db->insert_id();
				if(!empty($membercard)){
					$arr_s_upadte = array(
						'cust_used_by' => $last_inserted
					);
					$id = array(
						'mcard_no' => $membercard
					);
					$this->mcard_model->recordUpdateAny($id, $arr_s_upadte);
				}
				
				$post_arr['status'] = '1';
				$post_arr['id'] = (string) $last_inserted; 
				$post_arr['cust_name'] = $cust_name; 
				$post_arr['acc_id'] = $acc_id; 
				$post_arr['cust_dob'] = $cust_dob; 
				$post_arr['membership_card_no'] = !empty($membercard) ? $membercard : '0';
				$post_arr['cust_mobile'] = $cust_mobile; 
				
				$post_arr['message'] = 'Customer has been created successfully!';
				$this->set_response($post_arr, REST_Controller::HTTP_OK);
			}
			
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Parameter invalid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	public function customers_find_mobile_post(){
		$cust_mobile = $this->post('cust_mobile'); 
		$post_arr = [];
		if(!empty($cust_mobile)){
			if(( strlen($cust_mobile) == 10)){
				$arr_fetch = array(
					'cust_mobile' => $cust_mobile
				);
				$fetch_user = $this->customer_model->viewRecordAny($arr_fetch);
				if(count($fetch_user) > 0){  
					$post_arr['status'] = '1'; 
					$post_arr['cust_id'] = $fetch_user->id; 
					$post_arr['cust_mobile'] = $fetch_user->cust_mobile; 
					$post_arr['cust_name'] = $fetch_user->cust_name; 
					$post_arr['cust_membercard_no'] = !empty($fetch_user->membership_card_no) ? $fetch_user->membership_card_no : 'NA'; 
					$post_arr['cust_dob'] = $fetch_user->cust_dob; 
					$post_arr['message'] = 'Customer found...!';
					$this->set_response($post_arr, REST_Controller::HTTP_OK);
				}else{
					$post_arr['status'] = '2';
					$post_arr['message'] = 'Customer not found!';
					$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);  
				} 
			}else{
				$post_arr['status'] = '3';
				$post_arr['message'] = 'Mobile number invalid!';
				$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
			}
				
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Parameter invalid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	public function customers_find_membercard_post(){
		$cust_id = $this->post('cust_id');
		$membercard = $this->post('membercard');
		$arr_fetch = array(
			'view_status' => '1',
			'mcard_status' => '1',
			'cust_used_by' => $cust_id
		);
		$fetch_mcard = $this->mcard_model->viewRecordAny($arr_fetch);
		if(!empty($fetch_mcard->mcard_no)){  		
			if($fetch_mcard->mcard_no == $membercard){
				$post_arr['status'] = '1'; 
				$post_arr['availability'] = '1';
				$post_arr['message'] = 'Member card list available...!';
			}else{
				$post_arr['status'] = '2'; 
				$post_arr['availability'] = '0';
				$post_arr['message'] = 'Please add card no...!';
			}
			$this->set_response($post_arr, REST_Controller::HTTP_OK);
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Something is worng!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);  
		}
	}	
	public function customers_add_membercard_list_post(){
		$arr_fetch = array(
			'view_status' => '1',
			'mcard_status' => '1',
			'cust_used_by' => '0',
		);
		$fetch_mcard = $this->mcard_model->viewRecordAnyR($arr_fetch);
		if(count($fetch_mcard) > 0){  
			$post_arr['status'] = '1'; 
			$post_arr['message'] = 'Member card list available...!';
			$post_arr['mcard_list'] = [];
			foreach($fetch_mcard as $fetch_card){
				array_push($post_arr['mcard_list'], [
				'mcard_no' => $fetch_card->mcard_no
				]);
			}
			$this->set_response($post_arr, REST_Controller::HTTP_OK);
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Member card not available!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);  
		}
	}
	public function customers_add_membercard_post(){
		$custid = $this->post('cust_id');
		$mcard_no = $this->post('mcard_no');
		if(!empty($mcard_no) && !empty($custid)){
		$arr_fetch = array(
			'id' => $custid,
		);
		$fetch_user = $this->customer_model->viewRecordAny($arr_fetch);
		if(count($fetch_user) > 0){  
			if($fetch_user->membership_card_no == 0){
				$updat_card = array(
					'membership_card_no' => $mcard_no
				);
				$this->customer_model->recordUpdate($custid, $updat_card);
				$updat_card_mem = array(
					'cust_used_by' => $custid
				);
				$mcard_no1 = array(
					'mcard_no' => $mcard_no
				);
				$this->mcard_model->recordUpdateAny($mcard_no1, $updat_card_mem); 
				$post_arr['status'] = '1'; 
				$post_arr['message'] = 'Member card added successfully...!';
				$this->set_response($post_arr, REST_Controller::HTTP_OK);
			}else{
				$post_arr['status'] = '3';
				$post_arr['message'] = 'Member card Already using!';
				$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);  
			}
			
		}else{
			$post_arr['status'] = '2';
			$post_arr['message'] = 'Customer id not valid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);  
		}
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Parameter invalid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);  
		}
	}
}
