<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Order extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();  
        $this->methods['orders_create_post']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['orders_find_mobile_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['orders_add_membercard_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['orders_add_membercard_list_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['orders_list_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['orders_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
	public function orders_create_post(){ 
		 
		$cust_id = $this->post('cust_id');
		$transation_id1 = generateid();
		$transation_id = substr($transation_id1, 0, 10);
		 
		$post_arr = [];
		if(!empty($cust_id)){
			 
			$cust_id = $this->post('cust_id'); 
			$sale_type = $this->post('sale_type'); 
			$card_number = $this->post('card_number'); 
			$is_membership = $this->post('is_membership'); 
			$payment_mode = $this->post('payment_mode'); 
			$total_payment = $this->post('total_payment'); 
			$total_discount = $this->post('total_discount'); 
			$product_id = explode(",", $this->post('product_id'));  
			$product_price = explode(",", $this->post('product_price'));  
			$product_qty = explode(",", $this->post('product_qty'));  
			$payable_amount = $this->post('payable_amount');  
			$acc_id = $this->post('acc_id');  
			$created_on = date('Y-m-d H:i:s');
			$arr_payment_ins = array(
				'acc_id' => $acc_id,
				'transation_id' => $transation_id,
				'payment_mode' => $payment_mode,
				'cust_id' => $cust_id,
				'is_membership' => $is_membership,
				'card_number' => $card_number,
				'total_payment' => $payable_amount,
				'created_on' => $created_on,
				'total_discount' => $total_discount
			);
			$this->orderpayment_model->recordInsert($arr_payment_ins);
			
			$goods_nugs_count = count($product_id);
			$orders = array();
			for($i = 0; $i < $goods_nugs_count; $i++){
						if(!empty($product_id[$i])){
						$orders[] = array(
							'acc_id' => $acc_id,
							'prod_id' => $product_id[$i],
							'prod_category' => $product_id[$i],
							'order_price' => $product_price[$i],
							'order_qty' => $product_qty[$i],
							'cust_id' => $cust_id,
							'transation_id' => $transation_id,
							'created_on' => $created_on
							);
						}
					}
					 $this->db->insert_batch('tbl_order', $orders);  
					 $post_arr['status'] = '1';
					 $post_arr['transation_id'] = $transation_id;
					 $post_arr['datetime'] = $created_on;
					 $post_arr['order_id'] = $transation_id;
					$post_arr['message'] = 'Order has been successfully';
					$this->set_response($post_arr, REST_Controller::HTTP_OK); 
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Parameter invalid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	public function orders_list_post(){
		$acc_id = $this->post('acc_id'); 
		$post_arr = [];
		if(!empty($acc_id)){ 
				$arr_fetch = array(
					'acc_id' => $acc_id
				);
				$fetch_user = $this->orderpayment_model->viewRecordAnyR($arr_fetch);
				
				if(count($fetch_user) > 0){  
					$post_arr['status'] = '1';
					$post_arr['message'] = 'Order List...!';
					$post_arr['order_list'] = [];
					foreach($fetch_user as $fetch_order){
						$arr_order = [];
						$arr_fetch1 = array(
							'acc_id' => $acc_id,
							'transation_id' => $fetch_order->transation_id
						);
						$fetch_user1 = $this->order_model->viewRecordAnyR($arr_fetch1);
						$customer_name = $this->customer_model->find_by_row($fetch_order->cust_id);
						foreach($fetch_user1 as $fetch_p){
							$arr_fetch_prod = array(
								'id' => $fetch_p->prod_id
							);
							$fetch_prod = $this->product_model->viewRecordAny($arr_fetch_prod);
							
							$arr_order[] = [
								'prod_category_id' => $fetch_p->prod_category,
								'prod_category_name' => $this->category_model->find_by_name($fetch_p->prod_category),
								'prod_id' => $fetch_p->prod_id,
								'prod_name' => $fetch_prod->p_name,
								'prod_image' => U_A_ASST_I.$fetch_prod->p_image,
								'order_price' => $fetch_p->order_price,
								'order_qty' => $fetch_p->order_qty,
								'o_status' => $fetch_p->o_status,
								'o_status_name' => o_status($fetch_p->o_status),
								'o_date_time' => $fetch_p->created_on,
							];
						}
						array_push($post_arr['order_list'],[
							'acc_id' => $acc_id,
							'transation_id' => $fetch_order->transation_id,
							'cust_id' => $fetch_order->cust_id,
							'cust_name' => $customer_name->cust_name,
							'cust_membership_card_no' => $customer_name->membership_card_no,
							'card_number' => $fetch_order->card_number,
							'is_membership_id' => $fetch_order->is_membership,
							'is_membership_name' => (string) status1($fetch_order->is_membership),
							'payment_mode' => payment_mode_type($fetch_order->payment_mode),
							'total_payment' => $fetch_order->total_payment,
							'total_discount' => $fetch_order->total_discount,
							'product_list' => $arr_order
						]);
						
					}
					$this->set_response($post_arr, REST_Controller::HTTP_OK);
				}else{
					$post_arr['status'] = '2';
					$post_arr['message'] = 'No order found!';
					$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);  
				}  
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Parameter invalid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	 
}
