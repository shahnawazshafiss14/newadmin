<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Users extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();  
        $this->methods['users_login_post']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['users_forget_post']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
	public function users_login_post(){
		$user_name = $this->post('username');
		$user_pass = $this->post('password');
		$post_arr = [];
		if(!empty($user_name) && !empty($user_pass)){
			$arr_fetch = array(
				'user_name' => $user_name,
				'user_password' => md5($user_pass)
			);
			$fetch_user = $this->login_model->viewRecordAny($arr_fetch);
			if(count($fetch_user) > 0){
				if(($fetch_user->view_status == 1) && ($fetch_user->status == 1)){
					$post_arr['status'] = '1'; 
					$post_arr['fullname'] = $fetch_user->user_fullname;
					$post_arr['acc_id'] = $fetch_user->acc_id;
					$post_arr['user_loginid'] = $fetch_user->user_name;
					$post_arr['user_email'] = $fetch_user->user_email;
					$post_arr['user_level'] = $fetch_user->user_level;
					$post_arr['user_parent'] = $fetch_user->user_parent;
					$post_arr['user_adharno'] = $fetch_user->user_adharno;
					$post_arr['user_mobile'] = $fetch_user->user_mobile;
					$post_arr['user_address'] = $fetch_user->user_address;
					$post_arr['user_company'] = $fetch_user->user_company;
					$post_arr['user_company_slogn'] = $fetch_user->user_company_slogn;
					$post_arr['user_gstno'] = $fetch_user->user_gstno;
					$post_arr['user_signature'] = $fetch_user->user_signature;
					$post_arr['user_address2'] = $fetch_user->user_address2;
					$post_arr['user_state_id'] = $fetch_user->user_state;
					$post_arr['user_state_name'] = $this->location_model->locationId($fetch_user->user_state);
					$post_arr['user_town'] = $fetch_user->user_town;
					$post_arr['user_city_id'] = $fetch_user->user_city;
					$post_arr['user_city_name'] = $this->location_model->locationId($fetch_user->user_city);
					$post_arr['user_zone_id'] = $fetch_user->user_zone;
					$post_arr['user_zone_name'] = $this->zone_model->find_by_name($fetch_user->user_zone);
					$post_arr['join_date'] = $fetch_user->created_date;
					$post_arr['message'] = 'Login Successfully!';
					$this->set_response($post_arr, REST_Controller::HTTP_OK);
				}else{
					$post_arr['status'] = '3';
					$post_arr['message'] = 'Your account has been suspened!';
					$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
				}
			}else{
				$post_arr['status'] = '2';
				$post_arr['message'] = 'Invalid user name and passowrd!';
				$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
			}
			
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Parameter invalid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
	public function users_forget_post(){
		$user_name = $this->post('email');
		$post_arr = [];
		if(!empty($user_name)){
			$arr_fetch = array(
				'user_name' => $user_name
			);
			$fetch_user = $this->login_model->viewRecordAny($arr_fetch);
			if(count($fetch_user) > 0){ 
					$post_arr['status'] = '1'; 
					$post_arr['fullname'] = $fetch_user->user_fullname;
					$post_arr['acc_id'] = $fetch_user->acc_id;
					$this->load->library('email');
					$this->email->from('info@zupatech.com', 'Zupa Tech');
					$this->email->to('shahnawazshafiss9@gmail.com');
					//$this->email->cc('another@another-example.com');
					//$this->email->bcc('them@their-example.com');
					$this->email->subject('Email Test');
					$this->email->message('Testing the email class.');
					$this->email->send();
					
					$this->load->view('mail/reset_password_template');
					$post_arr['user_loginid'] = $fetch_user->user_name;
					$post_arr['user_email'] = $fetch_user->user_email;
					$post_arr['user_level'] = $fetch_user->user_level;
					$post_arr['user_parent'] = $fetch_user->user_parent;
					 
					$post_arr['message'] = 'Login Successfully!';
					$this->set_response($post_arr, REST_Controller::HTTP_OK);
				 
			}else{
				$post_arr['status'] = '2';
				$post_arr['message'] = 'Invalid email id!';
				$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
			}
			
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Parameter invalid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
		}
		
	}

     

}
