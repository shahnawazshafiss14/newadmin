<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Dashboard extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();  
        $this->methods['dashboard_total_get']['limit'] = 500; // 500 requests per hour per user/key
    } 
	public function dashboard_total_get(){
		$acc_id = $this->get('acc_id'); 
		$post_arr = [];
		if(!empty($acc_id)){
			 $post_arr = [];
			 $fetch_arr_customer = array('acc_id' => $acc_id);
			 $fetch_customer = $this->customer_model->viewRecordAnyR($fetch_arr_customer);
			 $fetch_arr_product = array('view_status' => '1');
			 $fetch_product = $this->product_model->viewRecordAnyR($fetch_arr_product);
			 
			 $fetch_arr_order = array('o_status' => '1');
			 $fetch_order = $this->order_model->viewRecordAnyR($fetch_arr_order);
			 
			 $post_arr['status'] = '1'; 
			 $post_arr['total_customer'] = (string) count($fetch_customer);
			 $post_arr['total_product'] = (string) count($fetch_product);
			 $post_arr['total_order'] = (string) count($fetch_order);
			 $post_arr['message'] = 'Found result';
			 $this->set_response($post_arr, REST_Controller::HTTP_OK);
				
		}else{
			$post_arr['status'] = '0';
			$post_arr['message'] = 'Parameter invalid!';
			$this->set_response($post_arr, REST_Controller::HTTP_NOT_FOUND);
		}
		
	}
}
