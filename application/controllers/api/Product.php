<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Product extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['products_post']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['combo_discount_post']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['product_details_post']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['category_list_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['products_search_post']['limit'] = 100; // 500 requests per hour per user/key
        $this->methods['products_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['products_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    public function products_post()
    {
        // Users from a data store e.g. database
        $arr_prod = array(
			'view_status' => '1'
		);
		$products  = $this->product_model->viewRecordAnyR($arr_prod);
		$post_arr = ['status' => '1'];
		if(count($products) > 0){
		$post_arr['list'] = [];
		foreach($products as $product){
			if(!empty($product->p_rpolicy)){
				$return_policy = $product->p_rpolicy;
				$sle = "* from tbl_return_policy where id IN (".$return_policy.")";
				$sge = $this->returnpolicy_model->viewRecordIN($sle, 'r_description');	
			}
			
				array_push($post_arr['list'],[
					'id' => $product->id,
					'p_name' => $product->p_name,
					'p_slug' => $product->p_slug,
					'p_skucode' => $product->p_skucode,
					'p_price' => $product->p_price,
					'p_stock' => $product->p_stock,
					'p_location_id' => $product->p_location,
					'p_location_name' => $this->zone_model->find_by_name($product->p_location),
					'p_category_id' => $product->p_category,
					'p_category_name' => $this->category_model->find_by_name($product->p_category),
					'p_memberdiscount' => $product->p_memberdiscount,
					'p_discount_price' => (string) ($product->p_price - ($product->p_price * $product->p_memberdiscount / 100)),
					'p_image' => U_A_ASST_I. $product->p_image,
					'p_desc' => $product->p_desc,
					'p_rpolicy' => $product->p_rpolicy,
					'p_rpolicy_details' => $sge
				]);
		}
		if (!empty($products))
        {
            $this->set_response($post_arr, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
		}
        else
        {
            $this->set_response([
                'status' => '0',
                'message' => 'Product could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        } 
    }
	public function products_search_post()
    {
        $cat_id = $this->post('cat_id');
        $prod_name = $this->post('prod_name');
		$category = "";
		if(!empty($cat_id)){
			$category  = "p_category=$cat_id and";
		}
		// Users from a data store e.g. database
        if(!empty($prod_name)){
		$arr_prod = "p_category from tbl_products where ".$category." p_name LIKE '%".$prod_name."%'";
		
		$products  = $this->product_model->viewRecordGCINR($arr_prod);
		$post_arr = ['status' => '1'];
		if(count($products) > 0){
		$post_arr['list'] = [];
		foreach($products as $product){
				if(!empty($product->p_rpolicy)){
					$return_policy = $product->p_rpolicy;
					$sle = "* from tbl_return_policy where id IN (".$return_policy.")";
					$sge = $this->returnpolicy_model->viewRecordIN($sle, 'r_description');	
				}
				
					array_push($post_arr['list'],[
						'id' => $product->id,
						'p_name' => $product->p_name,
						'p_slug' => $product->p_slug,
						'p_skucode' => $product->p_skucode,
						'p_price' => $product->p_price,
						'p_stock' => $product->p_stock,
						'p_location_id' => $product->p_location,
						'p_location_name' => $this->zone_model->find_by_name($product->p_location),
						'p_category_id' => $product->p_category,
						'p_category_name' => $this->category_model->find_by_name($product->p_category),
						'p_memberdiscount' => $product->p_memberdiscount,
						'p_discount_price' => (string) ($product->p_price - ($product->p_price * $product->p_memberdiscount / 100)),
						'p_image' => U_A_ASST_I.$product->p_image,
						'p_desc' => $product->p_desc,
						'p_rpolicy' => $product->p_rpolicy,
						'p_rpolicy_details' => !empty($sge) ? $sge : ''
					]);
			}
			if (!empty($products))
			{
				$this->set_response($post_arr, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
			}
		}
        else
        {
            $this->set_response([
                'status' => '2',
                'message' => 'Product could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
		}else{
			$this->set_response([
                'status' => '0',
                'message' => 'Parameter invalid!'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		}		
    }
	public function category_list_get()
    {
        // Users from a data store e.g. database
        $arr_prod = array(
			'view_status' => '1'
		);
		$products  = $this->category_model->viewRecordAnyR($arr_prod);
		$post_arr = ['status' => '1'];
		if(count($products) > 0){
		$post_arr['list'] = [];
		foreach($products as $product){ 
				array_push($post_arr['list'],[
					'id' => $product->id,
					'category_name' => $product->category_name
				]);
		}
		if (!empty($products))
        {
            $this->set_response($post_arr, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
		}
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'category could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        } 
    }
	public function combo_discount_get()
    {
		$id = $this->get('acc_id');
        $arr_prod = array(
			'combo_view_status' => '1',
			'acc_id' => $id,
			'combo_status' => '1'
		);
		$products  = $this->combooffer_model->viewRecordAny($arr_prod);
		$post_arr = ['status' => '1'];
		if(!empty($products->id)){
		$post_arr['combo_id']  = $products->id;
		$post_arr['combo_name']  = $products->combo_name;
		$post_arr['combo_offer']  = $products->combo_offer;
		 
			if (!empty($products))
			{
				$this->set_response($post_arr, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
			}
		}
        else
        {
            $this->set_response([
                'status' => '0',
                'message' => 'category could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        } 
    }
	public function product_details_get()
    {
        // Users from a data store e.g. database
		$id = $this->get('id');
		$post_arr = ['status' => '1'];
        $arr_prod = array(
			'view_status' => '1',
			'id' => $id
			
		);
		$products  = $this->product_model->viewRecordAny($arr_prod);
		if(count($products) > 0){
		if(!empty($products->p_rpolicy)){
			$return_policy = $products->p_rpolicy;
			$sle = "* from tbl_return_policy where id IN (".$return_policy.")";
			$sge = $this->returnpolicy_model->viewRecordIN($sle, 'r_description');	
			}
		
		$post_arr['id'] = $products->id;
		$post_arr['p_name'] = $products->p_name;
		$post_arr['p_slug'] = $products->p_slug;
		$post_arr['p_skucode'] = $products->p_skucode;
		$post_arr['p_price'] = $products->p_price;
		$post_arr['p_stock'] = $products->p_stock;
		$post_arr['p_location_id'] = $products->p_location;
		$post_arr['p_location_name'] = $this->zone_model->find_by_name($products->p_location);
		$post_arr['p_category_id'] = $products->p_category;
		$post_arr['p_category_name'] = $this->category_model->find_by_name($products->p_category);
		
		$post_arr['p_memberdiscount'] = $products->p_memberdiscount;
		$post_arr['p_image'] = U_A_ASST_I. $products->p_image;
		$post_arr['p_desc'] = $products->p_desc;
		$post_arr['p_rpolicy'] = $products->p_rpolicy;
		$post_arr['p_rpolicy_details'] = !empty($sge) ? $sge : '';
		if (!empty($products))
        {
            $this->set_response($post_arr, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
		}
        else
        {
            $this->set_response([
                'status' => '0',
                'message' => 'Product could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        } 
    }

     

    public function products_delete()
    {
        $id = (int) $this->get('id');

        // Validate the id.
        if ($id <= 0)
        {
            // Set the response and exit
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // $this->some_model->delete_something($id);
        $message = [
            'id' => $id,
            'message' => 'Deleted the resource'
        ];

        $this->set_response($message, REST_Controller::HTTP_NO_CONTENT); // NO_CONTENT (204) being the HTTP response code
    }

}
