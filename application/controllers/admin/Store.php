<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Store extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function details(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Store Details";
			$aray_fetch = array(
				'acc_id' => $find_url_slug
			);
			$fetch_value = $this->login_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['stored'] = $this->login_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		} 
		$data['contentView'] = 'admin/store/show_details';
		$this->load->view('admin/_template_model1', $data);
	}
	public function find_client(){ 
		$consig_id = $this->input->post('consig_id');
		$type = $this->input->post('type');
		if($type == 'consignee'){
			$con = '1';
		}else{
			$con = '2';
		}
		if(!empty($consig_id)){
			$data['Page_Title'] = "Store Details";
			$aray_fetch = array(
				'id' => $consig_id,
				'type' => $con
			);
			$fetch_value = $this->client_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$clientd = $this->client_model->viewRecordAny($aray_fetch);
				$arr_post = array('status' => '1');
				$arr_post['details'] =  $clientd->company_name . ', '. $clientd->client_name_ower .', '.$clientd->consignee .', '.$clientd->address;
				$arr_post['gst'] =  $clientd->gst_no;
				$arr_post['price'] =  $clientd->price;
				echo json_encode($arr_post);
			}else{
				$arr_post = array('status' => '0', 'message' => 'record not found!');
				echo json_encode($arr_post);
			}
		}else{
			redirect('page');
		} 
		 
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Details";
			$aray_fetch = array(
				'acc_id' => $find_url_slug
			);
			$fetch_value = $this->login_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['stored'] = $this->login_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['Page_Title'] = "Add Store";
		}
		$data['contentView'] = 'admin/store/add';
		$this->load->view('admin/_template_model1', $data);
	}
	public function lists(){	 
		$data['Page_Title']='Client List';
		$where = array(
			'view_status' => '1',
			'user_level' => '3'
		);
		$data['stores']= $this->login_model->viewRecordAnyR($where);	  
		$data['contentView'] = 'admin/store/lists';
		$this->load->view('admin/_template_model1', $data);  
  }
	public function savestore(){
		
		$emp_slugp = $this->input->post('store_slug'); 
		$user_name = $this->input->post('user_name');
		$user_email = $this->input->post('user_email');
		$user_adharno = $this->input->post('user_adharno');
		$user_mobile= $this->input->post('user_mobile');
		$user_address = $this->input->post('user_address');
		$user_state = $this->input->post('user_state');
		$user_town = $this->input->post('user_town');
		$user_salary = $this->input->post('user_salary');
		$user_city = $this->input->post('user_city');
		
		$emp_slug = 'TRA'.time();
			if(!empty($emp_slugp)){
				$aray_fetch = array(
					'acc_id' => $emp_slugp
				);
				$fetch_value = $this->login_model->viewRecordAny($aray_fetch);
				if(count($fetch_value) > 0){
					$array_save = array(
						'user_fullname' => $user_name,
						'user_name' => $user_name,
						'user_email' => $user_email,
						'user_adharno' => $user_adharno,
						'user_mobile' => $user_mobile,
						'user_address' => $user_address,
						'user_state' => '',
						'user_salary' => $user_salary,
						'user_town' => $user_town,
						'user_city' => $user_city
					);
					$inserted_id = $this->login_model->recordUpdateSlug('acc_id',$emp_slugp, $array_save);
					$this->session->set_flashdata('store_save', 'success');
					redirect(site_url(ADMIN_URL.'/store/details/'. $emp_slugp));
				}else{
					redirect('page');
				} 
			}else{
				if(!empty($user_name) && !empty($user_adharno)){
				$array_save = array(
						'user_fullname' => $user_name,
						'user_name' => $user_name,
						'user_email' => $user_email,
						'acc_id' => $emp_slug,
						'user_password' => md5('123456'),
						'user_level' => '2',
						'user_parent' => $this->acc_id,
						'user_adharno' => $user_adharno,
						'user_mobile' => $user_mobile,
						'user_address' => $user_address,
						'user_state' => '',
						'user_salary' => '0',
						'user_town' => $user_town,
						'user_city' => $user_city
					);
				$inserted_id = $this->login_model->recordInsert($array_save);
				$this->session->set_flashdata('store_save', 'success');
				redirect(site_url(ADMIN_URL.'/store/add'));
				}else {
					
				}
			} 
		
	}
	 
}


?>