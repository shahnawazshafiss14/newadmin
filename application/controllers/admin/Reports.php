<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends Admin_Controller{
	public $where1 = "";
	function __construct(){
		parent::__construct();
			if($this->acc_level != '1'){
				$this->where1 = " AND acc_id='".$this->acc_id."'";	
			}	
		
	}
	public function totalInvoice(){ 
		$find_url_slug = $this->uri->segment(3); 
		if(!empty($find_url_slug)){
			$get_start_date = $this->input->get('to');
			$get_end_date = $this->input->get('from'); 
			$start_date = !empty($get_start_date) ? $get_start_date : date('Y-m-d');
			$end_date = !empty($get_end_date) ? $get_end_date : date('Y-m-d');
			$select = "acc_id from tbl_consignment where view_status=1 ".$this->where1." AND status=1 AND (date_format(created_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')";
			$data['start_date'] = $start_date;
			$data['end_date'] = $end_date;
			$data['consignments'] = $this->consignment_model->viewRecordGCINR($select);
			$data['Page_Title']='Consignment reports';	
			$data['contentView'] = 'admin/reports/consigment_list';
			$this->load->view('admin/_template_model1', $data);
			 
		}else{
			redirect('page');
		} 
		 
	}
	public function totalcollection(){ 
		$find_url_slug = $this->uri->segment(3);
		 		
		if(!empty($find_url_slug)){
			$get_start_date = $this->input->get('to');
			$get_end_date = $this->input->get('from'); 
			$start_date = !empty($get_start_date) ? $get_start_date : date('Y-m-d');
			$end_date = !empty($get_end_date) ? $get_end_date : date('Y-m-d');
			$select = "acc_id from tbl_consignment where view_status=1 ".$this->where1." AND status=1 AND (date_format(created_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')";
			$data['start_date'] = $start_date;
			$data['end_date'] = $end_date;
			$data['consignments'] = $this->consignment_model->viewRecordGCINR($select);
			$data['Page_Title']='Consignment reports Collection';	
			$data['contentView'] = 'admin/reports/consigment_list_amount';
			$this->load->view('admin/_template_model1', $data); 
		}else{
			redirect('page');
		} 
	}
	public function totaltopay(){ 
		$find_url_slug = $this->uri->segment(3);
		 		
		if(!empty($find_url_slug)){
			$get_start_date = $this->input->get('to');
			$get_end_date = $this->input->get('from'); 
			$start_date = !empty($get_start_date) ? $get_start_date : date('Y-m-d');
			$end_date = !empty($get_end_date) ? $get_end_date : date('Y-m-d');
			$select = "acc_id from tbl_consignment where paid_by=1 AND view_status=1 ".$this->where1." AND status=1 AND (date_format(created_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')";
			$data['start_date'] = $start_date;
			$data['end_date'] = $end_date;
			$data['consignments'] = $this->consignment_model->viewRecordGCINR($select);
			$data['Page_Title']='Consignment reports To-pay';	
			$data['contentView'] = 'admin/reports/consigment_list_amount_topay';
			$this->load->view('admin/_template_model1', $data); 
		}else{
			redirect('page');
		} 
	}
	public function totaltopaid(){ 
		$find_url_slug = $this->uri->segment(3); 
		if(!empty($find_url_slug)){
			$get_start_date = $this->input->get('to');
			$get_end_date = $this->input->get('from'); 
			$start_date = !empty($get_start_date) ? $get_start_date : date('Y-m-d');
			$end_date = !empty($get_end_date) ? $get_end_date : date('Y-m-d');
			$select = "acc_id from tbl_consignment where paid_by=2 ".$this->where1." AND view_status=1 AND status=1 AND (date_format(created_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')";
			$data['start_date'] = $start_date;
			$data['end_date'] = $end_date;
			$data['consignments'] = $this->consignment_model->viewRecordGCINR($select);
			$data['Page_Title']='Consignment reports To-paid';	
			$data['contentView'] = 'admin/reports/consigment_list_amount_paid';
			$this->load->view('admin/_template_model1', $data); 
		}else{
			redirect('page');
		} 
	} 
	
}


?>