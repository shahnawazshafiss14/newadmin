<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Expense extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function details(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Expense Details";
			$aray_fetch = array(
				'slug' => $find_url_slug
			);
			$fetch_value = $this->expense_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['expense'] = $this->expense_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		} 
		$data['contentView'] = 'admin/expense/show_details';
		$this->load->view('admin/_template_model1', $data);
	}
	public function find_expense(){ 
		$consig_id = $this->input->post('consig_id');
		$type = $this->input->post('type');
		if($type == 'consignee'){
			$con = '1';
		}else{
			$con = '2';
		}
		if(!empty($consig_id)){
			$data['Page_Title'] = "Expense Details";
			$aray_fetch = array(
				'id' => $consig_id,
				'type' => $con
			);
			$fetch_value = $this->expense_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$expensed = $this->expense_model->viewRecordAny($aray_fetch);
				$arr_post = array('status' => '1');
				$arr_post['details'] =  $expensed->company_name . ', '. $expensed->expense_name_ower .', '.$expensed->consignee .', '.$expensed->address;
				$arr_post['gst'] =  $expensed->gst_no;
				$arr_post['price'] =  $expensed->price;
				echo json_encode($arr_post);
			}else{
				$arr_post = array('status' => '0', 'message' => 'record not found!');
				echo json_encode($arr_post);
			}
		}else{
			redirect('page');
		} 
		 
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Details";
			$aray_fetch = array(
				'slug' => $find_url_slug
			);
			$fetch_value = $this->expense_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['expensed'] = $this->expense_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['Page_Title'] = "Add Expense";
		}
		$data['contentView'] = 'admin/expense/add';
		$this->load->view('admin/_template_model1', $data);
	}
	public function lists(){	 
	   $data['Page_Title']='Expense List'; 
	   $where = array(
			'view_status' => '1'
		);
      $data['expensed']= $this->expense_model->viewRecordAnyR($where);
	  $data['contentView'] = 'admin/expense/lists';
	  $this->load->view('admin/_template_model1', $data);  
  }
	public function saveexpense(){
		
		$expense_slugp = $this->input->post('expense_slug'); 
		$expn_by = $this->input->post('expn_by');
		$expn_date = $this->input->post('expn_date');
		$expn_type = $this->input->post('expn_type');
		$expn_amount = $this->input->post('expn_amount');
		$expn_mode = $this->input->post('expn_mode');
		$rand = rand(); 
		$expense_slug = strtolower($expn_by.'-'.$rand);
			if(!empty($expense_slugp)){
				$aray_fetch = array(
					'expense_slug' => $expense_slugp
				);
				$fetch_value = $this->expense_model->viewRecordAny($aray_fetch);
				if(count($fetch_value) > 0){
					$array_save = array( 
						'acc_id' => $this->acc_id,
						'slug' => $expense_slug, 
						'expn_type' => $expn_type,
						'expn_date' => $expn_date,
						'expn_amount' => $expn_amount,
						'expn_by' => $expn_by,
						'expn_mode' => $expn_mode 
					);
					$inserted_id = $this->expense_model->recordUpdateSlug('expense_slug',$expense_slugp, $array_save);
					$this->session->set_flashdata('expense_save', 'success');
					redirect(site_url(ADMIN_URL.'/expense/details/'. $expense_slugp));
				}else{
					redirect('page');
				} 
			}else{
				if(!empty($expn_date) && !empty($expn_amount)){
				$array_save = array( 
					'acc_id' => $this->acc_id,
					'slug' => $expense_slug, 
					'expn_type' => $expn_type,
					'expn_date' => $expn_date,
					'expn_amount' => $expn_amount,
					'expn_by' => $expn_by,
					'expn_mode' => $expn_mode 
				);
				$inserted_id = $this->expense_model->recordInsert($array_save);
				$this->session->set_flashdata('expense_save', 'success');
				redirect(site_url(ADMIN_URL.'/expense/add'));
				}else {
					
				}
			}
			
			 
			
		 
		
	}
}


?>