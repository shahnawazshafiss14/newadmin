<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Userp extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function index(){	 
	   $data['Page_Title']='Permission Page';	
	  
		$data['contentView'] = 'admin/users/permission_top';
		$this->load->view('admin/_template_model1', $data);  
	}
	public function checked_menu(){
		$accident_id = $this->input->post('accountid');
		$menuid = $this->input->post('menuid');
		$status = $this->input->post('statusa');
		if(!empty($menuid)){
			$arr_fetch = array(
				'acc_id' => $accident_id,
				'top_menu_id' => $menuid
			);
			$fetch_permission = $this->permison_model->viewRecordAny($arr_fetch);
			if(count($fetch_permission) > 0){ 
				$insert_data = array(
					'acc_id' => $accident_id,
					'top_menu_id' => $menuid,
				);
				$this->permison_model->recordDelete($insert_data);
				echo json_encode(
				array(
					'status' => '2',
					'message' => 'All permission has been inActive!'
				));	
			}else{
				$insert_data = array(
					'acc_id' => $accident_id,
					'top_menu_id' => $menuid,
				);
				$this->permison_model->recordInsert($insert_data);	
				echo json_encode(
				array(
					'status' => '1',
					'message' => 'All permission has been Actived!'
				));	
			}
			
			
		}else{
			json_encode(
			array(
			'status' => '0',
			'message' => 'invalid somthing wrongs'
			));
		}
	}
	public function checked_menu_child(){
		$menuid = $this->input->post('menuid');
		$status = $this->input->post('statusa');
		$accident_id = $this->input->post('accountid');
		if($status == '1'){
			$update1 = '1';
		}else{
			$update1 = '0';
		} 
		$types = $this->input->post('types');
		if(!empty($menuid) && !empty($types)){
			 
			$arr_fetch = array(
						'acc_id' => $accident_id,
						'top_menu_id' => $menuid 
					);
			$fetch_permission = $this->permison_model->viewRecordAny($arr_fetch);
			if(count($fetch_permission) > 0){ 
				if($types == 'is_viewed'){
					$update_data = array(
						'is_viewed' => $update1
					);
					$this->permison_model->recordUpdateAny($arr_fetch, $update_data);
				}else if($types == 'is_inserted'){
					$update_data = array(
						'is_inserted' => $update1
					);
					$this->permison_model->recordUpdateAny($arr_fetch, $update_data);
				}else if($types == 'is_viewed'){
					$update_data = array(
						'is_viewed' => $update1
					);
					$this->permison_model->recordUpdateAny($arr_fetch, $update_data);
				}else if($types == 'is_edited'){
					$update_data = array(
						'is_edited' => $update1
					);
					$this->permison_model->recordUpdateAny($arr_fetch, $update_data);
				}else if($types == 'is_deleted'){
					$update_data = array(
						'is_deleted' => $update1
					);
					$this->permison_model->recordUpdateAny($arr_fetch, $update_data);
				}else if($types == 'is_printed'){
					$update_data = array(
						'is_printed' => $update1
					);
					$this->permison_model->recordUpdateAny($arr_fetch, $update_data);
				} 
				echo json_encode(
				array(
					'status' => '1',
					'message' => 'Permission has been updated!' 
				));	
			}  
		}else{
			json_encode(
			array(
			'status' => '0',
			'message' => 'invalid somthing wrongs'
			));
		}
	}
	public function details(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Consignment Details";
			$aray_fetch = array(
				'consignment_slug' => $find_url_slug
			);
			$fetch_value = $this->consignment_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['consignmentd'] = $this->consignment_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		} 
		//$data['contentView'] = 'admin/consignment/show_details';
		$this->load->view('admin/consignment/show_details', $data);
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Consignment";
			$aray_fetch = array(
				'consignment_slug' => $find_url_slug
			);
			$fetch_value = $this->consignment_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['consignmentd'] = $this->consignment_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['Page_Title'] = "Add Consignment";
		}
		$data['contentView'] = 'admin/consignment/add';
		$this->load->view('admin/_template_model1', $data);
	}
	
	public function addgoods(){ 
	 
		$consigmentd = $this->input->post('consigmentd');
		$goods_name = $this->input->post('goods_name'); 
		$nugs = $this->input->post('nugs'); 
		$chargednug = $this->input->post('chargednug');
		$weightnug = $this->input->post('weightnug'); 
		
		$array_save =  array(
			'consignment_id' => $consigmentd,
			'acc_id' => $this->acc_id,
			'goods_id' => $goods_name,
			'order_nugs' => $nugs,
			'order_nug_charge' => $chargednug,
			'order_nug_weight' => $weightnug
		);
		$this->consignmentgoods_model->recordInsert($array_save);
		$ar_post = array('status' => '1');
		$ar_post['message'] = 'Consignment has been created!'; 
		echo json_encode($ar_post);
	}
	
	public function saveconsignment(){
		
	 
		$bilty_date = $this->input->post('bilty_date'); 
		$consignment_slugp = $this->input->post('consignment_slug'); 
		$consignee_name = $this->input->post('consignee_name');
		$consignee_details = $this->input->post('consignee_details');
		$consignee_gst = $this->input->post('consignee_gst');
		$consigner = $this->input->post('consigner');
		$consigner_details = $this->input->post('consigner_details');
		$consigner_gst = $this->input->post('consigner_gst');
		$freight = $this->input->post('freight');
		$bilty = $this->input->post('bilty');
		$narrationcrg = $this->input->post('narrationcrg');
		$narrationother = $this->input->post('narrationother');
		$grandtotal = $this->input->post('grandtotal');
		$invoice_no = $this->input->post('invoice_no');
		$gst_percent = $this->input->post('gst_percent');
		$gst_paid = $this->input->post('gst_paid');
		$sgst = $this->input->post('sgst');
		$cgst = $this->input->post('cgst');
		$igst = $this->input->post('igst');
		$nettotal = $this->input->post('nettotal');
		$form_number = $this->input->post('form_number');
		$rs_value = $this->input->post('rs_value');
		$paid_by = $this->input->post('paid_by');
		$payment_type = $this->input->post('payment_type');
		$created_date = $this->input->post('created_date'); 
		$consignment_slug = strtolower($invoice_no.'-'.$consignee_name); 
		
			if(!empty($consignment_slugp)){
				$aray_fetch = array(
					'consignment_slug' => $consignment_slugp
				);
				$fetch_value = $this->consignment_model->viewRecordAny($aray_fetch);
				 
				if(count($fetch_value) > 0){
					
					$array_save = array(  
						'consignee_name' => $consignee_name,
						'consignee_details' => $consignee_details,
						'consignee_gst' => $consignee_gst,
						'consigner' => $consigner,
						'consigner_details' => $consigner_details,
						'consigner_gst' => $consigner_gst,
						'freight' => $freight,
						'bilty_date' => $bilty_date,
						'bilty' => $bilty,
						'narrationcrg' => $narrationcrg,
						'narrationother' => $narrationother,
						'grandtotal' => $grandtotal,
						'invoice_no' => $invoice_no,
						'gst_percent' => $gst_percent,
						'gst_paid' => $gst_paid,
						'sgst' => $sgst,
						'cgst' => $cgst,
						'igst' => $igst,
						'nettotal' => $nettotal,
						'form_number' => $form_number,
						'rs_value' => $rs_value,
						'paid_by' => $paid_by,
						'payment_type' => $payment_type,
						'status' => '1',
						'last_upate_date' => date('Y-m-d H:i:s')
					);
					 
					 $this->consignment_model->recordUpdateSlug('consignment_slug',$consignment_slugp, $array_save);
					$ar_post = array('status' => '1');
					$ar_post['message'] = 'Consignment has been created!';
					$ar_post['url'] = base_url('admin/consignment/details/'. $consignment_slug);
					echo json_encode($ar_post);
					 
				}else{
					redirect('page');
				} 
			}else{
				if(!empty($consignee_name) && !empty($invoice_no)){
					
					
					$array_save = array(
						'acc_id' => $this->acc_id,
						'consignment_slug' => $consignment_slug,
						'consignee_name' => $consignee_name,
						'consignee_details' => $consignee_details,
						'consignee_gst' => $consignee_gst,
						'consigner' => $consigner,
						'consigner_details' => $consigner_details,
						'consigner_gst' => $consigner_gst,
						'freight' => $freight,
						'bilty_date' => $bilty_date,
						'bilty' => $bilty,
						'narrationcrg' => $narrationcrg,
						'narrationother' => $narrationother,
						'grandtotal' => $grandtotal,
						'invoice_no' => $invoice_no,
						'gst_percent' => $gst_percent,
						'gst_paid' => $gst_paid,
						'sgst' => $sgst,
						'cgst' => $cgst,
						'igst' => $igst,
						'nettotal' => $nettotal,
						'form_number' => $form_number,
						'rs_value' => $rs_value,
						'paid_by' => $paid_by,
						'payment_type' => $payment_type,
						'status' => '1',
						'created_date' => date('Y-m-d H:i:s')
					);
					$this->consignment_model->recordInsert($array_save);
					$inserted_id = $this->db->insert_id() ;
					$goods_name = explode(",", $this->input->post('goods_name')); 
					$goods_nugs = explode(",", $this->input->post('goods_nugs')); 
					$chargednug = explode(",", $this->input->post('chargednug')); 
					$weightnug = explode(",", $this->input->post('weightnug'));  
					$goods_nugs_count = count($goods_nugs);
					$orders = array();
					 
					for($i = 0; $i < $goods_nugs_count; $i++){
						$orders[] = array(
							'consignment_id' => $inserted_id,
							'acc_id' => $this->acc_id,
							'goods_id' => $goods_name[$i],
							'order_nugs' => $goods_nugs[$i],
							'order_nug_charge' => $chargednug[$i],
							'order_nug_weight' => $weightnug[$i]
							);
					}
					 $this->db->insert_batch('tbl_consignment_goods_orders', $orders); 
					///$this->session->set_flashdata('consignment_save', 'success');
					$ar_post = array('status' => '1');
					$ar_post['message'] = 'Consignment has been created!';
					$ar_post['url'] = base_url('admin/consignment/details/'. $consignment_slug);
					echo json_encode($ar_post);
					//redirect(site_url(ADMIN_URL.'/consignment/add'));
				}else {
					
				}
			} 
	}
	public function goods_delete(){
		$deleteid = $this->input->post('deleteid');
		$data_id = array(
			'goods_order_id' => $deleteid
		);
		$this->consignmentgoods_model->recordDelete1('tbl_consignment_goods_orders', $data_id);
		echo json_encode(
		array(
		'status' => '1',
		'message' => 'Goods has been removed'
		));
	}
	public function deletecong(){
		$deleteid = $this->input->post('deleteid'); 
		$data_id = array(
			'consignment_id' => $deleteid
		);
		$data = array(
				'view_status' => '0',
				'status' => '0'
 			);
			
		$this->consignment_model->recordUpdateAny($data_id, $data);
		echo json_encode(
		array(
		'status' => '1',
		'message' => 'Consignment has been removed'
		));
	}
}


?>