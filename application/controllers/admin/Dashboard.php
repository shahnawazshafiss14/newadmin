<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller{
  function __construct(){
    parent::__construct(); 
  }
 
  public function index(){ 
       
			/* Today reporting */
			$where1 = "";
			if($this->acc_level != '1'){
				$where1 = " AND acc_id='".$this->acc_id."'";	
			}
			$get_start_date = $this->input->get('to');
			$get_end_date = $this->input->get('from'); 
			$start_date = !empty($get_start_date) ? $get_start_date : date('Y-m-d');
			$end_date = !empty($get_end_date) ? $get_end_date : date('Y-m-d');
			$select = "acc_id from tbl_consignment where view_status=1".$where1." AND status=1 AND (date_format(created_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')";
			/*total count */
			$data['today_total_count'] = $this->consignment_model->viewRecordGCINR($select);
			
			/*total To pay */
			$total_topay = " SUM(nettotal) as topay from tbl_consignment where paid_by=1 ".$where1." AND view_status=1 AND status=1 AND (date_format(created_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')";
			$data['today_topay'] = $this->consignment_model->viewRecordGCINRS($total_topay);
			
			/*total Paid */
			$total_topay = " SUM(nettotal) as topay from tbl_consignment where paid_by=2 ".$where1." AND view_status=1 AND status=1 AND (date_format(created_date, '%Y-%m-%d') BETWEEN '".$start_date."' AND '".$end_date."')";
			$data['today_topaid'] = $this->consignment_model->viewRecordGCINRS($total_topay);
			
			/* end today reporting */
			$data['contentView'] = 'admin/dashboard';
			$data['Page_Title'] = "Dashboard";
			$this->load->view('admin/_template_model1', $data); 
       
  }
 
  public function staff(){
    //Allowing akses to staff only
    if($this->session->userdata('level')==='2'){
      $this->load->view('dashboard_view');
    }else{
        echo "Access Denied";
    }
  }
 
  public function author(){
    //Allowing akses to author only
    if($this->session->userdata('level')==='3'){
      $this->load->view('dashboard_view');
    }else{
        echo "Access Denied";
    }
  }
 
}
