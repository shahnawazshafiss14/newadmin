<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Master extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function zone(){	 
		$find_url_slug = $this->uri->segment(4);	
		if(!empty($find_url_slug)){
			$data['Page_Title']='Edit Zone';
		$where = array(
			'zone_slug' => $find_url_slug
		);
		$data['zone']= $this->zone_model->viewRecordAny($where);
		}else{
			$data['Page_Title']='Zones';
			$where = array(
				'view_status' => '1',
				'status' => '1'	
			);
			$data['zones']= $this->zone_model->viewRecordAnyR($where);			
		}
		$data['contentView'] = 'admin/master/zones';
		$this->load->view('admin/_template_model1', $data);  
  }
  public function mcard(){	 
		$find_url_slug = $this->uri->segment(4);	
		if(!empty($find_url_slug)){
			$data['Page_Title']='Edit Membership card';
		$where = array(
			'mcard_slug' => $find_url_slug
		);
		$data['membership']= $this->mcard_model->viewRecordAny($where);
		}else{
			$data['Page_Title']='Membership Card';
			$where = array(
				'view_status' => '1' 
			);
			$data['memberships']= $this->mcard_model->viewRecordAnyR($where);			
		}
		$data['contentView'] = 'admin/master/mcard';
		$this->load->view('admin/_template_model1', $data);  
  }
	public function addzone(){
		$zone_slugp = $this->input->post('zone_slug'); 
		$zone_name = $this->input->post('zone_name');		
		$zone_slug = strtolower($zone_name.'-'.time());
		
			if(!empty($zone_slugp)){
				$aray_fetch = array(
					'zone_slug' => $zone_slugp
				);
				$fetch_value = $this->zone_model->viewRecordAny($aray_fetch);
				if(count($fetch_value) > 0){
					$array_save = array(
						'zone_name' => $zone_name,
						'zone_slug' => $zone_slug
					);
					$inserted_id = $this->zone_model->recordUpdateSlug('zone_slug',$zone_slugp, $array_save);
					$this->session->set_flashdata('zone_save', 'success');
					redirect(site_url(ADMIN_URL.'/master/zone/'));
				}else{
					redirect('page');
				} 
			}else{
				if(!empty($zone_name)){
				$array_save = array(
					'zone_name' => $zone_name,
					'acc_id' => $this->acc_id,
					'zone_slug' => $zone_slug
				);
				$inserted_id = $this->zone_model->recordInsert($array_save);
				$this->session->set_flashdata('zone_save', 'success');
				redirect(site_url(ADMIN_URL.'/master/zone'));
				}else {
					
				}
			} 
	}
	public function addmcard(){
		$zone_slugp = $this->input->post('mcard_slug'); 
		$mcard_no = $this->input->post('mcard_no');		
		$mcard_status = $this->input->post('mcard_status');		
		$mcard_slug = $mcard_no;
		$aray_fetch = array(
			'mcard_no' => $mcard_no
		);
		$fetch_value = $this->mcard_model->viewRecordAny($aray_fetch);
		if(!empty($zone_slugp)){
					$array_save = array(
						'mcard_no' => $mcard_no,
						'mcard_slug' => $mcard_slug,
						'mcard_status' => $mcard_status 
					);
					if(count($fetch_value) > 0){
						if($fetch_value->mcard_no == $mcard_no){
							
							$array_save_status = array(
								'mcard_status' => $mcard_status 
							);
							$this->mcard_model->recordUpdateSlug('mcard_slug',$zone_slugp, $array_save_status);
							$this->session->set_flashdata('mcard_save', 'Your status has been changed.');
							redirect(site_url(ADMIN_URL.'/master/mcard/'));
						}else{
							$this->session->set_flashdata('mcard_save', 'Card no. Already Exits.');
							redirect(site_url(ADMIN_URL.'/master/mcard/'));
						}			
					}else{
					$inserted_id = $this->mcard_model->recordUpdateSlug('mcard_slug',$zone_slugp, $array_save);
					$this->session->set_flashdata('mcard_save', 'success');
					redirect(site_url(ADMIN_URL.'/master/mcard/'));			
					} 
			}else{ 
				if(!empty($mcard_no)){
					$array_save = array(
						'mcard_no' => $mcard_no,
						'acc_id' => $this->acc_id,
						'mcard_slug' => $mcard_no
					);
					if(count($fetch_value) > 0){
						$this->session->set_flashdata('mcard_save', 'Card no. Already Exits.');
					}else{
						$inserted_id = $this->mcard_model->recordInsert($array_save);
						$this->session->set_flashdata('mcard_save', 'Card no has been added!!');
						
					}					
					redirect(site_url(ADMIN_URL.'/master/mcard'));
				}else {
					
				}
			} 
	}
	
	public function details(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Client Details";
			$aray_fetch = array(
				'client_slug' => $find_url_slug
			);
			$fetch_value = $this->client_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['clientd'] = $this->client_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		} 
		$data['contentView'] = 'admin/client/show_details';
		$this->load->view('admin/_template_model1', $data);
	}
	public function find_client(){ 
		$consig_id = $this->input->post('consig_id');
		$type = $this->input->post('type');
		if($type == 'consignee'){
			$con = '1';
		}else{
			$con = '2';
		}
		if(!empty($consig_id)){
			$data['Page_Title'] = "Client Details";
			$aray_fetch = array(
				'id' => $consig_id,
				'type' => $con
			);
			$fetch_value = $this->client_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$clientd = $this->client_model->viewRecordAny($aray_fetch);
				$arr_post = array('status' => '1');
				$arr_post['details'] =  $clientd->company_name . ', '. $clientd->client_name_ower .', '.$clientd->consignee .', '.$clientd->address;
				$arr_post['gst'] =  $clientd->gst_no;
				$arr_post['price'] =  $clientd->price;
				echo json_encode($arr_post);
			}else{
				$arr_post = array('status' => '0', 'message' => 'record not found!');
				echo json_encode($arr_post);
			}
		}else{
			redirect('page');
		} 
		 
	}
	public function find_city(){
		$state_id = $this->input->post('state_id');
		$html = '';
		$html .= '<select id="user_city" name="user_city" class="form-control">
					<option value="">Select City</option>';
					$arr_location = array(
						'location_parent' => $state_id
					);
					$fetch_location = $this->location_model->viewRecordAnyR($arr_location);
					foreach($fetch_location as $loc){
		$html .= '<option value="'. $loc->location_id .'">'. $loc->location_name .'</option>';
					}
		echo $html .= '</select>';
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Details";
			$aray_fetch = array(
				'client_slug' => $find_url_slug
			);
			$fetch_value = $this->client_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['clientd'] = $this->client_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['Page_Title'] = "Add Client";
		}
		$data['contentView'] = 'admin/client/add';
		$this->load->view('admin/_template_model1', $data);
	}
	
}


?>