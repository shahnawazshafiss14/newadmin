<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Combooffer extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function add(){	 
		$find_url_slug = $this->uri->segment(4);	
		if(!empty($find_url_slug)){
			$data['Page_Title']='Edit Details';
		$where = array(
			'combo_slug' => $find_url_slug
		);
		$data['combo']= $this->combooffer_model->viewRecordAny($where);
		}else{
			$data['Page_Title']='Combo Offer';
			$where = array(
				'combo_view_status' => '1'
			);
			$data['combos']= $this->combooffer_model->viewRecordAnyR($where);			
		}
		$data['contentView'] = 'admin/combooffer/combooffer';
		$this->load->view('admin/_template_model1', $data);  
  } 
	public function addcombo(){
		$combo_slugp = $this->input->post('combo_slug'); 
		$combo_name = $this->input->post('combo_name');		
		$combo_offer = $this->input->post('combo_offer');		
		$combo_status = $this->input->post('combo_status');		
		$combo_slug = strtolower($combo_name.'-'.time());
		
			if(!empty($combo_slugp)){
				$aray_fetch = array(
					'combo_slug' => $combo_slugp
				);
				$fetch_value = $this->combooffer_model->viewRecordAny($aray_fetch);
				if(count($fetch_value) > 0){
					$array_save = array(
						'combo_name' => $combo_name,
						'combo_offer' => $combo_offer,
						'combo_status' => $combo_status,
						'combo_slug' => $combo_slug
					);
					$inserted_id = $this->combooffer_model->recordUpdateSlug('combo_slug',$combo_slugp, $array_save);
					$this->session->set_flashdata('combo_save', 'success');
					redirect(site_url(ADMIN_URL.'/combooffer/add'));
				}else{
					redirect('page');
				} 
			}else{
				if(!empty($combo_name)){
				$array_save = array(
					'combo_name' => $combo_name,
					'combo_offer' => $combo_offer,
					'acc_id' => $this->acc_id,
					'combo_slug' => $combo_slug
				);
				$inserted_id = $this->combooffer_model->recordInsert($array_save);
				$this->session->set_flashdata('combo_save', 'success');
				redirect(site_url(ADMIN_URL.'/combooffer/add'));
				}else {
					
				}
			} 
	}
	  
	
}


?>