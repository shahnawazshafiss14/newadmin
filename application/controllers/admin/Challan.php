<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Challan extends Admin_Controller{
	function __construct(){
		parent::__construct();
	} 
	public function lists(){	 
	   $data['Page_Title']='Create Challan';
		if($this->acc_level == '1'){
			$where = array(
				'view_status' => '1',
				'status' => '1',
				'challan_status <>' => '1'
				
			);
		}else{
			$where = array(
				'view_status' => '1',
				'status' => '1',
				'acc_id' => $this->acc_id,
				'challan_status <>' => '1'
			);	
		}
      $data['consignments']= $this->consignment_model->viewRecordAnyR($where);
	  $data['contentView'] = 'admin/challan/lists';
		$this->load->view('admin/_template_model1', $data);  
	}
	public function updatestatus(){
		$challan_no = $this->input->post('challanno');
			$aray_fetch = array(
				'challan_no' => $challan_no
			);
		$arr_post = array();
		$fetch_value = $this->challan_model->viewRecordAny($aray_fetch);
		if(count($fetch_value) > 0){
			$data = array(
				'status' => '2'
			);
			$this->challan_model->recordUpdate($fetch_value->id, $data);
			$arr_post['status'] = '1';
			$arr_post['message'] = 'Request has been submitted!';
			
		}else{
			$arr_post['status'] = '0';	
			$arr_post['message'] = 'Somthing going worng';
		}
		echo json_encode($arr_post);
			
	}
	
	public function invoice(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Challan Details";
			$aray_fetch = array(
				'challan_no' => $find_url_slug
			);
			$fetch_value = $this->challan_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['consignmentd'] = $this->challan_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		}
		//$data['contentView'] = 'admin/challan/show_invoices';
		$this->load->view('admin/challan/show_invoices', $data);  
	}
	public function index(){	 
	   $data['Page_Title']='Challan List';
		if($this->acc_level == '1'){
			$where = array(
				'view_status' => '1',
			);
		}else{
			$where = array(
				'view_status' => '1',
				'acc_id' => $this->acc_id 
			);	
		}
      $data['consignments']= $this->challan_model->viewRecordAnyR($where);
	  $data['contentView'] = 'admin/challan/list';
		$this->load->view('admin/_template_model1', $data);  
	}
	 
	public function challancreate(){
		$consig_code = $this->input->post('consig_code');
		$c_zone_to = $this->input->post('c_challan_zone');
		$c_zone_from = $this->acc_zone;
		$consig_exp = explode(',', $consig_code);
		
		$challan_no = time();
		$arr_chall = array(
			'challan_no' => $challan_no,
			'c_zone_from' => $c_zone_from,
			'c_zone_to' => $c_zone_to,
			'consignee_ids' => $consig_code,
			'acc_id' => $this->acc_id,
			'created_date' => date('Y-m-d H:i:s')
		); 
		$this->challan_model->recordInsert($arr_chall);
		for($i=0; $i <= count($consig_exp); $i++){
			$data = array(
				'challan_status' => '1',
				'challan_no' => $challan_no
			);
			$idss = $consig_exp[$i];
			$this->consignment_model->recordUpdate($idss, $data);
		}
		$arrya_jso = array(
			'status' => '1',
			'message' => 'Challan has been created!'
		);
		echo json_encode($arrya_jso);
	}
	 
	 
}


?>