<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends Admin_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
     redirect(site_url(ADMIN_URL.'/login'));
    }
	$this->load->model('product_model');
  }
 
  public function add(){	
  $data['Page_Title'] = "Add Product";
	$data['contentView'] = 'admin/product/add';
		$this->load->view('admin/_template_model1', $data);   
  }
  public function imageupdate(){
	$productEdit = $this->input->post('productEdit');  
 $productd = $this->input->post('eventid'); 
 
	if(isset($_FILES["p_image"]["name"])){
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assets/images/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload('p_image')) {
		    echo $this->upload->display_errors();
		}else {
		   $data = $this->upload->data();
		   $data1 = array('update_data' => $this->upload->data()); 
		   $datas = array(
						'p_image' => $data1['update_data']['file_name'],
					);
					 
		    $this->product_model->recordUpdate($productd,$datas);
			echo json_encode(array(
				'status' => '1',
				'message' => 'Product has been updated!!!'
			));
		}
	}
  }
  public function deleterowimage(){
	 $productEdit = $this->input->post('row_id'); 
	 
	$datas = array(	
		'view_status' => '0' 
		);
	$this->pimages_model->recordUpdate($productEdit,$datas);
	echo json_encode(array(
		'status' => '1',
		'message' => 'Product has been updated!!!'
		));
	}
	
  
  public function save(){
	$p_name = $this->input->post('p_name');
	$p_category = $this->input->post('p_category');
	$p_desc = $this->input->post('p_desc');
	$p_skucode = $this->input->post('p_skucode');
	$p_price = $this->input->post('p_price');
	$p_stock = $this->input->post('p_stock');
	$p_memberdiscount = $this->input->post('p_memberdiscount');
	$p_location = $this->input->post('p_location');
	//$p_image = $this->input->post('p_image');
	$p_extra_image = $this->input->post('p_extra_image');
	$p_rpolicy1 = $this->input->post('p_rpolicy');
	$p_rpolicy = implode(',', $p_rpolicy1);
 
	$productEdit = $this->input->post('slug');
	//$related_product = implode(",",$_POST['cate']);
	$product_title = $this->input->post('product_name');
	$product_slug = str_replace(" ", "-", strtolower($product_title));
	if (!empty($productEdit)) {
	    $data= array(
			'p_name' => $p_name,
			'p_category' => $p_category,
			'p_desc' => $p_desc,
			'p_skucode' => $p_skucode,
			'p_slug' => strreplace($p_name),
			'p_price' => $p_price,
			'p_stock' => $p_stock,
			'p_memberdiscount' => $p_memberdiscount,
			'p_location' => $p_location,
			'p_image' => $p_image,
			'p_rpolicy' => $p_rpolicy
			);
	    $this->product_model->recordUpdate($productEdit, $datas);
	    $this->session->set_flashdata('product_uploaded', 'Your product has been updated Successfully');
	    redirect('admin/product/productLists');
	} else {
	    if (isset($_FILES["p_image"]["name"])) {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assets/images/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('p_image')) {
		    echo $this->upload->display_errors();
		} else {
		   $data = $this->upload->data();
		   $data1 = array('update_data' => $this->upload->data()); 
		   $datas= array(
				'p_name' => $p_name,
				'p_category' => $p_category,
				'p_desc' => $p_desc,
				'p_slug' => strreplace($p_name), 
				'p_skucode' => $p_skucode,
				'p_price' => $p_price,
				'p_stock' => $p_stock,
				'p_memberdiscount' => $p_memberdiscount,
				'p_location' => $p_location,
				'p_image' => $data1['update_data']['file_name'],
				'p_rpolicy' => $p_rpolicy
			);
		    $this->product_model->recordInsert($datas);
			$last_Id = $this->db->insert_id();
			if ($_FILES["files"]["name"] != '') {
			$config['encrypt_name'] = TRUE;
			$config['upload_path'] = './assets/images/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			for ($count = 0; $count < count($_FILES["files"]["name"]); $count++) {
				$_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
				$_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
				$_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
				$_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
				if ($this->upload->do_upload('file')) {
					$data = $this->upload->data(); 
				}
				$ar = $data['file_name'];
				$araray = array(
					'file_name' => $ar,
					'pid' => $last_Id  
				);
				$this->pimages_model->recordInsert($araray);
			}
			}
		    $this->session->set_flashdata('product_uploaded', 'Your product has been uploaded Successfully');
		    redirect('admin/product/add');
		}
	    }
	}
	 
  }
  public function extraimagesupload() { 
	$productEdit = $this->input->post('productEdit');
	if (!empty($productEdit)) {
	    if ($_FILES["files"]["name"] != '') {
		$config['encrypt_name'] = TRUE;
		$config['upload_path'] = './assets/images/';
		$config['allowed_types'] = 'jpg|jpeg|png|gif';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		for ($count = 0; $count < count($_FILES["files"]["name"]); $count++) {
		    $_FILES["file"]["name"] = $_FILES["files"]["name"][$count];
		    $_FILES["file"]["type"] = $_FILES["files"]["type"][$count];
		    $_FILES["file"]["tmp_name"] = $_FILES["files"]["tmp_name"][$count];
		    $_FILES["file"]["size"] = $_FILES["files"]["size"][$count];
		    if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
			$data1 = array('update_data' => $this->upload->data());
		    }
		    $ar = $data['file_name'];
		    $araray = array(
			'file_name' => $ar,
			'pid' => $productEdit
		    );
		    $this->pimages_model->recordInsert($araray); 
		}
		
	    }
		redirect('admin/product/details/' . $productEdit);
	}
    }

    public function resizing($path, $file) {
	$config['image_library'] = 'gd2';
	$config['source_image'] = $path;
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 200;
	$config['height'] = 200;
	$config['new_image'] = './assets/images/' . $file;
	$this->load->library('image_lib', $config);
	$this->image_lib->initialize();
	$this->image_lib->resize();
    }

    public function resizinglarge($path, $file) {
	$config['image_library'] = 'gd2';
	$config['source_image'] = $path;
	$config['create_thumb'] = FALSE;
	$config['maintain_ratio'] = TRUE;
	$config['width'] = 411;
	$config['height'] = 274;
	$config['new_image'] = './assets/images/' . $file;
	$this->load->library('image_lib', $config);
	$this->image_lib->initialize();
	$this->image_lib->resize();
    }
  
  function lists(){	 
	   $data['Page_Title']='Product List';	
	   $arr_prod = array(
		'view_status' => '1'
	   );
		$data['products']=$this->product_model->viewRecordAnyR($arr_prod);
		$data['contentView'] = 'admin/product/lists';
		$this->load->view('admin/_template_model1', $data);  
  }
	function details($id){ 
		$this->db->select('*');
		$this->db->from('tbl_products');
		$this->db->where('id', $id);
		$query = $this->db->get();
		$data['product'] = $query->row(); 
		$data['Page_Title']='Product Details';
		$data['contentView'] = 'admin/product/show_details';
		$this->load->view('admin/_template_model1', $data);  
	}  

	function edit($id){ 

	  if ($this->input->server('REQUEST_METHOD') == 'GET'){
			  $this->db->select('*');
			  $this->db->from('tbl_products');
			  $this->db->where('id', $id);
			  $query = $this->db->get();
			  $data['product'] = $query->row(); 
			  $data['Page_Title']='Edit Product';
			  
			 $data['contentView'] = 'admin/product/edit';
			$this->load->view('admin/_template_model1', $data);  
			 
	  }else if ($this->input->server('REQUEST_METHOD') == 'POST'){
			
			 $this->db->select('*');
			 $this->db->from('tbl_products');
			 $this->db->where('id', $this->input->post('p_id'));
			 $query = $this->db->get();
			 $product = $query->row(); 
			 $original_value=$product->p_name;
			 
			if($this->input->post('p_name') != $original_value) {
			   $is_unique =  '|is_unique[tbl_products.p_name]';
			} else {
			   $is_unique =  '';
			}

		    $this->form_validation->set_rules(
				'p_name', 'Text Field Five', 'trim|required'.$is_unique, array('required'=>'Please enter product name!','is_unique'=>'Product name already exits!'));	
			if ($this->form_validation->run() == FALSE)
			{
				$data['product'] = $product; 
				$data['Page_Title']='Edit Product';	
				$data['contentView'] = 'admin/product/edit';
				$this->load->view('admin/_template_model1', $data); 
			}else{
			$p_rpolicy1 = $this->input->post('p_rpolicy');
			$p_rpolicy = implode(',', $p_rpolicy1);		
			$p_name= $this->input->post('p_name');	
			$slug = strreplace($p_name);			
			  $this->db->where('id', $this->input->post('p_id'));
			  $this->db->update('tbl_products', array(
				'p_name' => $p_name,
				'p_slug' => $slug,
				'p_skucode' => $this->input->post('p_skucode'),
				'p_price' => $this->input->post('p_price'),
				'p_stock' => $this->input->post('p_stock'),
				'p_location' => $this->input->post('p_location'),
				'p_category' => $this->input->post('p_category'),
				'p_memberdiscount' => $this->input->post('p_memberdiscount'),
				'p_desc' => $this->input->post('p_desc'),
				'p_rpolicy' => $p_rpolicy
				));
			  echo $this->session->set_flashdata('msg','Product has been Updated');
			  redirect(site_url(ADMIN_URL.'/product/lists'));  
			}  
	  }		
  }  
}
