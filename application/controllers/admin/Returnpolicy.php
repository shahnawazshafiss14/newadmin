<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Returnpolicy extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function details(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Fleet Details";
			$aray_fetch = array(
				'slug' => $find_url_slug
			);
			$fetch_value = $this->returnpolicy_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['fleetmanagementd'] = $this->returnpolicy_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		} 
		$data['contentView'] = 'admin/fleetmanagement/show_details';
		$this->load->view('admin/_template_model1', $data);
	}
	public function find_fleetmanagement(){ 
		$consig_id = $this->input->post('consig_id');
		$type = $this->input->post('type');
		if($type == 'consignee'){
			$con = '1';
		}else{
			$con = '2';
		}
		if(!empty($consig_id)){
			$data['Page_Title'] = "Fleet Details";
			$aray_fetch = array(
				'id' => $consig_id,
				'type' => $con
			);
			$fetch_value = $this->returnpolicy_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$fleetmanagementd = $this->returnpolicy_model->viewRecordAny($aray_fetch);
				$arr_post = array('status' => '1');
				$arr_post['details'] =  $fleetmanagementd->company_name . ', '. $fleetmanagementd->fleetmanagement_name_ower .', '.$fleetmanagementd->consignee .', '.$fleetmanagementd->address;
				$arr_post['gst'] =  $fleetmanagementd->gst_no;
				$arr_post['price'] =  $fleetmanagementd->price;
				echo json_encode($arr_post);
			}else{
				$arr_post = array('status' => '0', 'message' => 'record not found!');
				echo json_encode($arr_post);
			}
		}else{
			redirect('page');
		} 
		 
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Details";
			$aray_fetch = array(
				'slug' => $find_url_slug
			);
			$fetch_value = $this->returnpolicy_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['returnpolicyd'] = $this->returnpolicy_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['Page_Title'] = "Add Return Policy";
		}
		$data['contentView'] = 'admin/returnpolicy/add';
		$this->load->view('admin/_template_model1', $data);
	}
	public function lists(){	 
	   $data['Page_Title']='Return Policy List';
	   if($this->acc_level == '1'){
			$where = array(
				'view_status' => '1',
				'status' => '1'
			);
		}else{
			$where = array(
				'view_status' => '1',
				'status' => '1' 
			);	
		} 
	   
      $data['returnpolicys']= $this->returnpolicy_model->viewRecordAnyR($where);
		$data['contentView'] = 'admin/returnpolicy/lists';
		$this->load->view('admin/_template_model1', $data);  
  }
	public function savereturnpolicy(){ 
		$slugp = $this->input->post('slug'); 
		$r_cutoff = $this->input->post('r_cutoff');
		$nodays = $this->input->post('nodays');
		$r_description = $this->input->post('r_description');
		$slug = $nodays.'-'.time();
			if(!empty($slugp)){
				$aray_fetch = array(
					'slug' => $slugp
				);
				$fetch_value = $this->returnpolicy_model->viewRecordAny($aray_fetch);
				if(count($fetch_value) > 0){
					$array_save = array(
						'r_description' => $r_description,
						'acc_id' => $this->acc_id,
						'r_cutoff' => $r_cutoff, 
						'r_days' => $nodays, 
						'slug' => $slug 
					);
					$inserted_id = $this->returnpolicy_model->recordUpdateSlug('slug',$slugp, $array_save);
					$this->session->set_flashdata('rpolicy_save', 'Fleet has been updated successful.');
					redirect(site_url(ADMIN_URL.'/returnpolicy/details/'. $slugp));
				}else{
					redirect('page');
				} 
			}else{
				if(!empty($nodays)){
				
					$array_save = array(
						'r_description' => $r_description,
						'acc_id' => $this->acc_id,
						'r_cutoff' => $r_cutoff, 
						'r_days' => $nodays, 
						'slug' => $slug 
					);
					$inserted_id = $this->returnpolicy_model->recordInsert($array_save);
					$this->session->set_flashdata('rpolicy_save', 'Fleet has been add successful.');
					redirect(site_url(ADMIN_URL.'/returnpolicy/add'));
				}else{
					echo "something wong!";
				}
			} 
	}
	public function rdelete(){
		
	}
}


?>