<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
    parent::__construct();
    $this->load->model('login_model');
  }
 
  function index(){
    $this->load->view(ADMIN_URL.'/login');
  }
 
  function auth(){
    $email    = $this->input->post('email',TRUE);
    $password = md5($this->input->post('password',TRUE));
    $validate = $this->login_model->validate($email,$password);
    if($validate->num_rows() > 0){
        $data  = $validate->row_array();
        $name  = $data['user_name'];
        $email = $data['user_email'];
        $level = $data['user_level'];
        $zone_id = $data['user_zone'];
        $sesdata = array(
            'acc_id'  => $data['acc_id'],
            'username'  => $name,
            'email'     => $email,
            'level'     => $level,
            'zone_id'     => $zone_id,
            'logged_in' => TRUE
        );
        $this->session->set_userdata($sesdata);
        // access login for admin
         
            redirect(base_url(ADMIN_URL.'/dashboard'));
        // access login for staff
         
    }else{
        echo $this->session->set_flashdata('msg','Username or Password is Wrong');
        redirect(base_url(ADMIN_URL.'/login'));
    }
  }
 
  function logout(){
      $this->session->sess_destroy();
     redirect(base_url(ADMIN_URL.'/login'));
  }
}
