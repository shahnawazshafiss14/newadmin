<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Settings extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function profile(){ 
	 
			$data['Page_Title'] = "Profile Details";
			$aray_fetch = array(
				'acc_id' => $this->acc_id
			);
			$fetch_value = $this->login_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['employeed'] = $this->login_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		 
		$data['contentView'] = 'admin/users/user_profile';
		$this->load->view('admin/_template_model1', $data);
	}
	public function find_client(){ 
		$consig_id = $this->input->post('consig_id');
		$type = $this->input->post('type');
		if($type == 'consignee'){
			$con = '1';
		}else{
			$con = '2';
		}
		if(!empty($consig_id)){
			$data['Page_Title'] = "Employee Details";
			$aray_fetch = array(
				'id' => $consig_id,
				'type' => $con
			);
			$fetch_value = $this->client_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$clientd = $this->client_model->viewRecordAny($aray_fetch);
				$arr_post = array('status' => '1');
				$arr_post['details'] =  $clientd->company_name . ', '. $clientd->client_name_ower .', '.$clientd->consignee .', '.$clientd->address;
				$arr_post['gst'] =  $clientd->gst_no;
				$arr_post['price'] =  $clientd->price;
				echo json_encode($arr_post);
			}else{
				$arr_post = array('status' => '0', 'message' => 'record not found!');
				echo json_encode($arr_post);
			}
		}else{
			redirect('page');
		} 
		 
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Details";
			$aray_fetch = array(
				'acc_id' => $find_url_slug
			);
			$fetch_value = $this->login_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['employeed'] = $this->login_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['Page_Title'] = "Add Employee";
		}
		$data['contentView'] = 'admin/employee/add';
		$this->load->view('admin/_template_model1', $data);
	}
	public function lists(){	 
		$data['Page_Title']='Client List';
		$where = array(
			'view_status' => '1',
			'user_level' => '3'
		);
		$data['employees']= $this->login_model->viewRecordAnyR($where);	  
		$data['contentView'] = 'admin/employee/lists';
		$this->load->view('admin/_template_model1', $data);  
  }
	public function saveemployee(){
		$emp_slugp = $this->acc_id; 
		$user_fullname = $this->input->post('user_fullname');
		$user_name = $this->input->post('user_name');
		$user_email = $this->input->post('user_email');
		$user_adharno = $this->input->post('user_adharno');
		$user_mobile= $this->input->post('user_mobile');
		$user_address = $this->input->post('user_address');
		$user_address2 = $this->input->post('user_address2');
		$user_state = $this->input->post('user_state');
		$user_town = $this->input->post('user_town');
		$user_city = $this->input->post('user_city');
		$user_salary = $this->input->post('user_salary');
		$user_zone = $this->input->post('user_zone');
		$user_company = $this->input->post('user_company');
		$user_company_slogn = $this->input->post('user_company_slogn');
		$user_gstno = $this->input->post('user_gstno');
		$user_signature = $this->input->post('user_signature');
		$aray_fetch = array(
			'acc_id' => $emp_slugp
		);
		$fetch_value = $this->login_model->viewRecordAny($aray_fetch);
		if(count($fetch_value) > 0){
			$array_save = array(
				'user_fullname' => $user_fullname,
				'user_name' => $user_name,
				'user_email' => $user_email,
				'user_adharno' => $user_adharno,
				'user_mobile' => $user_mobile,
				'user_address' => $user_address,
				'user_address2' => $user_address2,
				'user_state' => $user_state,
				'user_zone' => $user_zone,
				'user_salary' => $user_salary,
				'user_town' => $user_town,
				'user_city' => $user_city,
				'user_company' => $user_company,
				'user_company_slogn' => $user_company_slogn,
				'user_gstno' => $user_gstno,
				'user_signature' => $user_signature,
			);
			$inserted_id = $this->login_model->recordUpdateSlug('acc_id',$emp_slugp, $array_save);
			$this->session->set_flashdata('message', 'success');
			redirect(site_url(ADMIN_URL.'/settings/profile'));
		}else{
			redirect('page');
		}
	}
	public function add_salary(){
		$add_amount = $this->input->post('add_amount'); 
		$emp_id = $this->input->post('emp_id');
		$aray_fetch = array(
			'acc_id' => $emp_id
		);
		$fetch_value = $this->login_model->viewRecordAny($aray_fetch);
		if(!empty($add_amount) && !empty($emp_id)){
			$array_save = array(
					'acc_id' => $this->acc_id,
					'emp_id' => $emp_id,
					'slug' => "SAL".time(),
					'trans_amount' => $add_amount,
					'trans_date' => date('Y-m-d'),
					'created_on' => date('Y-m-d H:i:s')
				);
			$inserted_id = $this->salary_model->recordInsert($array_save);
			echo "1";
		}
	}
}


?>