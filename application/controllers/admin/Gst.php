<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Gst extends Admin_Controller{
  function __construct(){
    parent::__construct();
    if($this->session->userdata('logged_in') !== TRUE){
     redirect(site_url(ADMIN_URL.'/login'));
    }
	$this->load->model('gst_model');
  }
 
  function add(){
	$data['Page_Title'] = "Add Gst";
	$data['contentView'] = 'admin/gst/add';
	$this->load->view('admin/_template_model1', $data);
		
	//$this->load->template(ADMIN_URL.'/gst/add',array('Page_Title'=>'Add Gst Rate'));  
  }
  
  function save(){
	
	$this->form_validation->set_rules('gst_rate', 'Text Field Five', 'trim|required|is_unique[tbl_gsts.gst_rate]',
            array('required'=>'Please enter gst rate!','is_unique'=>'Please enter another gst rate!'));
			
			
	if ($this->form_validation->run() == FALSE)
        {
			$data['Page_Title'] = "Add Gst";
			$data['contentView'] = 'admin/gst/add';
	$this->load->view('admin/_template_model1', $data);
         //   $this->load->template(ADMIN_URL.'/gst/add',array('Page_Title'=>'Add Gst Rate'));  
        }
        else
        {
			$data=array('gst_rate'=>$this->input->post('gst_rate'));
            $this->gst_model->insert($data);
	  echo $this->session->set_flashdata('msg','New Gst Rate Added');
	    $data['Page_Title']='Gst Rate List';	
	   
  		     redirect(site_url(ADMIN_URL.'/gst/lists'));
        }
  }
  
  function lists(){	 
	   $data['Page_Title']='Gst Rate List';	
       $data['gsts']=$this->gst_model->get_gsts();
		$data['contentView'] = 'admin/gst/lists';
		$this->load->view('admin/_template_model1', $data);  
	  //$this->load->template(ADMIN_URL.'/gst/lists',$data);  
  }	

  function edit($id){ 

	  if ($this->input->server('REQUEST_METHOD') == 'GET'){
			  $this->db->select('*');
			  $this->db->from('tbl_gsts');
			  $this->db->where('id', $id);
			  $query = $this->db->get();
			  $data['gst'] = $query->row(); 
			  $data['Page_Title']='Edit Gst Rate';	
			//$this->load->template(ADMIN_URL.'/gst/edit',$data);  
			$data['contentView'] = 'admin/gst/edit';
			$this->load->view('admin/_template_model1', $data); 
		
	  }else if ($this->input->server('REQUEST_METHOD') == 'POST'){
			
			 $this->db->select('*');
			 $this->db->from('tbl_gsts');
			 $this->db->where('id', $this->input->post('g_id'));
			 $query = $this->db->get();
			 $gst = $query->row(); 
			 $original_value=$gst->gst_rate;
			 
			if($this->input->post('gst_rate') != $original_value) {
			   $is_unique =  '|is_unique[tbl_gsts.gst_rate]';
			} else {
			   $is_unique =  '';
			}

		    $this->form_validation->set_rules('gst_rate', 'Text Field Five', 'trim|required'.$is_unique,
            array('required'=>'Please enter gst rate!','is_unique'=>'gst rate already exits!'));	
			if ($this->form_validation->run() == FALSE)
			{
				$data['gst'] = $gst; 
				$data['Page_Title']='Edit Gst Rate';	
				$data['contentView'] = 'admin/gst/edit';
			$this->load->view('admin/_template_model1', $data); 
				//$this->load->template(ADMIN_URL.'/gst/edit',$data);  
			}else{		
			  $this->db->where('id',$this->input->post('g_id'));
			  $this->db->update('tbl_gsts', array('gst_rate'=>$this->input->post('gst_rate')));
			  echo $this->session->set_flashdata('msg','Gst rate has been Updated');
			   
			  redirect(site_url(ADMIN_URL.'/gst/lists'));  
			}  
	  }		
  }  
}
