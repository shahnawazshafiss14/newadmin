<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Gatepass extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	
	public function index(){	 
	   $data['Page_Title']='Gate Pass List';
		if($this->acc_level == '1'){
			$where = array(
				'tbl_challan.view_status' => '1',
				'tbl_challan.status' => '2'
			);
		}else{
			$where = array(
				'tbl_challan.view_status' => '1',
				'zone_to' => $this->acc_zone,
				'tbl_challan.status' => '2'
			);	
		}	   
      // $data['consignments']= $this->consignment_model->viewRecordAnyR($where);
      $data['consignments']= $this->consignment_model->viewRecordAnyjoinCh($where);
	  $data['contentView'] = 'admin/gatepass/index';
		$this->load->view('admin/_template_model1', $data);  
	}
	public function challangenerate(){	 
	   $data['Page_Title']='Generate Challan';
		if($this->acc_level == '1'){
			$where = array(
				'view_status' => '1'
			);
		}else{
			$where = array(
				'view_status' => '1',
				'c_zone_to' => $this->acc_zone
			);	
		}	   
		$data['consignments']= $this->challan_model->viewRecordAnyR($where);
		$data['contentView'] = 'admin/challan/generatechallan';
		$this->load->view('admin/_template_model1', $data);  
	}
	
	
	 
	public function details(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Gate Pass Details";
			$aray_fetch = array(
				'consignment_slug' => $find_url_slug
			);
			$fetch_value = $this->consignment_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['consignmentd'] = $this->consignment_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		}
		$data['contentView'] = 'admin/gatepass/show_details';
		$this->load->view('admin/_template_model1', $data);  
	}
	public function generategetpass(){
		$payment_type = $this->input->post('payment_type');
		$totalamount = $this->input->post('totalamount');
		$slug = $this->input->post('consignment_slug');
		$consignement_id = $this->input->post('consignement_id');
        $receiver_name = $this->input->post('receiver_name');
        $hamali = $this->input->post('hamali');
        $statistical = $this->input->post('statistical');
        $demurrage = $this->input->post('demurrage');
		$checks = $this->input->post('checks');
		$remarks = $this->input->post('remarks');
		$gatepass_id = $this->input->post('gatepass_id');
		$payment_id_id = $this->input->post('payment_id_id');
		 
		$payment = array(
			'payment_mode' => $payment_type,
			'acc_id' => $this->acc_id,
			'consignement_id' => $consignement_id,
			'amount' => $totalamount,
			'payment_matching' => '2',
			'checks' => $checks,
			'remarks' => $remarks
		);
		if(!empty($payment_id_id)){
			$this->consigpayment_model->recordUpdate($payment_id_id, $payment);
			$payment_id = $payment_id_id;
		}else{
			$this->consigpayment_model->recordInsert($payment);
			$payment_id = $this->db->insert_id();
		}
		
		if(!empty($gatepass_id)){
			$getpass = array(
				'acc_id' => $this->acc_id,
				'payment_id' => $payment_id,
				'consigment_id' => $consignement_id,
				'demurrage' => $demurrage,
				'statistical' => $statistical,
				'hamali' => $hamali,
				'receiver_name' => $receiver_name 
			);
			$this->gatepass_model->recordUpdate($gatepass_id, $getpass);
		}else{
			$getpass = array(
				'acc_id' => $this->acc_id,
				'payment_id' => $payment_id,
				'consigment_id' => $consignement_id,
				'demurrage' => $demurrage,
				'statistical' => $statistical,
				'hamali' => $hamali,
				'receiver_name' => $receiver_name,
				'gp_id' => time(),
				'genrated_date' => date('Y-m-d H:i:s')
			);
			$this->gatepass_model->recordInsert($getpass); 
		} 
		redirect(site_url(ADMIN_URL.'/gatepass/invoice/'.$slug));
	}
	public function invoice(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Gate Pass Invoice";
			$aray_fetch = array(
				'consignment_slug' => $find_url_slug
			);
			$fetch_value = $this->consignment_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['consignmentd'] = $this->consignment_model->viewRecordAnyjion($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		}
		//$data['contentView'] = 'admin/gatepass/show_invoice';
		$this->load->view('admin/gatepass/show_invoice', $data);  
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Consignment";
			$aray_fetch = array(
				'consignment_slug' => $find_url_slug
			);
			$fetch_value = $this->consignment_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['consignmentd'] = $this->consignment_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['Page_Title'] = "Add Consignment";
		}
		$data['contentView'] = 'admin/consignment/add';
		$this->load->view('admin/_template_model1', $data);
	}
	
	public function addgoods(){  
		$consigmentd = $this->input->post('consigmentd');
		$goods_name = $this->input->post('goods_name'); 
		$nugs = $this->input->post('nugs'); 
		$chargednug = $this->input->post('chargednug');
		$weightnug = $this->input->post('weightnug'); 
		
		$array_save =  array(
			'consignment_id' => $consigmentd,
			'acc_id' => $this->acc_id,
			'goods_id' => $goods_name,
			'order_nugs' => $nugs,
			'order_nug_charge' => $chargednug,
			'order_nug_weight' => $weightnug
		);
		$this->consignmentgoods_model->recordInsert($array_save);
		$ar_post = array('status' => '1');
		$ar_post['message'] = 'Consignment has been created!'; 
		echo json_encode($ar_post);
	}
	
	
	public function saveconsignment(){
		
	 
		$bilty_date = $this->input->post('bilty_date'); 
		$consignment_slugp = $this->input->post('consignment_slug'); 
		$consignee_name = $this->input->post('consignee_name');
		$consignee_details = $this->input->post('consignee_details');
		$consignee_gst = $this->input->post('consignee_gst');
		$consigner = $this->input->post('consigner');
		$truck = $this->input->post('truck');
		$consigner_details = $this->input->post('consigner_details');
		$consigner_gst = $this->input->post('consigner_gst');
		$freight = $this->input->post('freight');
		$bilty = $this->input->post('bilty');
		$narrationcrg = $this->input->post('narrationcrg');
		$narrationother = $this->input->post('narrationother');
		$grandtotal = $this->input->post('grandtotal');
		$invoice_no = $this->input->post('invoice_no');
		$gst_percent = $this->input->post('gst_percent');
		$gst_paid = $this->input->post('gst_paid');
		$sgst = $this->input->post('sgst');
		$cgst = $this->input->post('cgst');
		$igst = $this->input->post('igst');
		$nettotal = $this->input->post('nettotal');
		$form_number = $this->input->post('form_number');
		$rs_value = $this->input->post('rs_value');
		$paid_by = $this->input->post('paid_by');
		$payment_type = $this->input->post('payment_type');
		$created_date = $this->input->post('created_date'); 
		$consignment_slug = strtolower($invoice_no.'-'.$consignee_name.'-'.time()); 
		
			if(!empty($consignment_slugp)){
				$aray_fetch = array(
					'consignment_slug' => $consignment_slugp
				);
				$fetch_value = $this->consignment_model->viewRecordAny($aray_fetch);
				 
				if(count($fetch_value) > 0){
					
					$array_save = array(  
						'consignee_name' => $consignee_name,
						'consignee_details' => $consignee_details,
						'consignee_gst' => $consignee_gst,
						'consigner' => $consigner,
						'consigner_details' => $consigner_details,
						'consigner_gst' => $consigner_gst,
						'freight' => $freight,
						'truck_no' => $truck,
						'bilty_date' => $bilty_date,
						'bilty' => $bilty,
						'narrationcrg' => $narrationcrg,
						'narrationother' => $narrationother,
						'grandtotal' => $grandtotal,
						'invoice_no' => $invoice_no,
						'gst_percent' => $gst_percent,
						'gst_paid' => $gst_paid,
						'sgst' => $sgst,
						'cgst' => $cgst,
						'igst' => $igst,
						'nettotal' => $nettotal,
						'form_number' => $form_number,
						'rs_value' => $rs_value,
						'paid_by' => $paid_by,
						'payment_type' => $payment_type,
						'status' => '1',
						'last_upate_date' => date('Y-m-d H:i:s')
					);
					 
					 $this->consignment_model->recordUpdateSlug('consignment_slug',$consignment_slugp, $array_save);
					$ar_post = array('status' => '1');
					$ar_post['message'] = 'Consignment has been created!';
					$ar_post['url'] = base_url('admin/consignment/details/'. $consignment_slug);
					echo json_encode($ar_post);
					 
				}else{
					redirect('page');
				} 
			}else{
				if(!empty($consignee_name) && !empty($invoice_no)){
					
					
					$array_save = array(
						'acc_id' => $this->acc_id,
						'consignment_slug' => $consignment_slug,
						'consignee_name' => $consignee_name,
						'consignee_details' => $consignee_details,
						'consignee_gst' => $consignee_gst,
						'consigner' => $consigner,
						'consigner_details' => $consigner_details,
						'consigner_gst' => $consigner_gst,
						'freight' => $freight,
						'truck_no' => $truck,
						'bilty_date' => $bilty_date,
						'bilty' => $bilty,
						'narrationcrg' => $narrationcrg,
						'narrationother' => $narrationother,
						'grandtotal' => $grandtotal,
						'invoice_no' => $invoice_no,
						'gst_percent' => $gst_percent,
						'gst_paid' => $gst_paid,
						'sgst' => $sgst,
						'cgst' => $cgst,
						'igst' => $igst,
						'nettotal' => $nettotal,
						'form_number' => $form_number,
						'rs_value' => $rs_value,
						'paid_by' => $paid_by,
						'payment_type' => $payment_type,
						'status' => '1',
						'created_date' => date('Y-m-d H:i:s')
					);
					$this->consignment_model->recordInsert($array_save);
					$inserted_id = $this->db->insert_id() ;
					$goods_name = explode(",", $this->input->post('goods_name')); 
					$goods_nugs = explode(",", $this->input->post('goods_nugs')); 
					$chargednug = explode(",", $this->input->post('chargednug')); 
					$weightnug = explode(",", $this->input->post('weightnug'));  
					$goods_nugs_count = count($goods_nugs);
					$orders = array();
					 
					for($i = 0; $i < $goods_nugs_count; $i++){
						$orders[] = array(
							'consignment_id' => $inserted_id,
							'acc_id' => $this->acc_id,
							'goods_id' => $goods_name[$i],
							'order_nugs' => $goods_nugs[$i],
							'order_nug_charge' => $chargednug[$i],
							'order_nug_weight' => $weightnug[$i]
							);
					}
					 $this->db->insert_batch('tbl_consignment_goods_orders', $orders); 
					///$this->session->set_flashdata('consignment_save', 'success');
					$ar_post = array('status' => '1');
					$ar_post['message'] = 'Consignment has been created!';
					$ar_post['url'] = base_url('admin/consignment/details/'. $consignment_slug);
					echo json_encode($ar_post);
					//redirect(site_url(ADMIN_URL.'/consignment/add'));
				}else {
					
				}
			} 
	}
	public function goods_delete(){
		$deleteid = $this->input->post('deleteid');
		$data_id = array(
			'goods_order_id' => $deleteid
		);
		$this->consignmentgoods_model->recordDelete1('tbl_consignment_goods_orders', $data_id);
		echo json_encode(
		array(
		'status' => '1',
		'message' => 'Goods has been removed'
		));
	}
	public function deletecong(){
		$deleteid = $this->input->post('deleteid'); 
		$data_id = array(
			'consignment_id' => $deleteid
		);
		$data = array(
				'view_status' => '0',
				'status' => '0'
 			);
			
		$this->consignment_model->recordUpdateAny($data_id, $data);
		echo json_encode(
		array(
		'status' => '1',
		'message' => 'Consignment has been removed'
		));
	}
}


?>