<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends Admin_Controller{
	function __construct(){
		parent::__construct();
	}
	public function details(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Category Details";
			$aray_fetch = array(
				'category_slug' => $find_url_slug
			);
			$fetch_value = $this->category_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['categoryd'] = $this->category_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			redirect('page');
		} 
		$data['contentView'] = 'admin/category/show_details';
		$this->load->view('admin/_template_model1', $data);
	}
	public function find_category(){ 
		$consig_id = $this->input->post('consig_id');
		$type = $this->input->post('type');
		if($type == 'consignee'){
			$con = '1';
		}else{
			$con = '2';
		}
		if(!empty($consig_id)){
			$data['Page_Title'] = "Category Details";
			$aray_fetch = array(
				'id' => $consig_id,
				'type' => $con
			);
			$fetch_value = $this->category_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$categoryd = $this->category_model->viewRecordAny($aray_fetch);
				$arr_post = array('status' => '1');
				$arr_post['details'] =  $categoryd->company_name . ', '. $categoryd->category_name_ower .', '.$categoryd->address;
				$arr_post['gst'] =  $categoryd->gst_no;
				$arr_post['price'] =  $categoryd->price;
				echo json_encode($arr_post);
			}else{
				$arr_post = array('status' => '0', 'message' => 'record not found!');
				echo json_encode($arr_post);
			}
		}else{
			redirect('page');
		} 
		 
	}
	public function add(){ 
		$find_url_slug = $this->uri->segment(4);
		if(!empty($find_url_slug)){
			$data['Page_Title'] = "Edit Details";
			$aray_fetch = array(
				'category_slug' => $find_url_slug
			);
			$fetch_value = $this->category_model->viewRecordAny($aray_fetch);
			if(count($fetch_value) > 0){
				$data['categoryd'] = $this->category_model->viewRecordAny($aray_fetch);
			}else{
				redirect('page');
			}
		}else{
			$data['Page_Title'] = "Add Category";
		}
		$data['contentView'] = 'admin/category/add';
		$this->load->view('admin/_template_model1', $data);
	}
	public function lists(){	 
	   $data['Page_Title']='Category List';
	   if($this->acc_level == '1'){
			$where = array(
				'view_status' => '1',
				'category_status' => '1'
			);
		}else{
			$where = array(
				'view_status' => '1',
				'category_status' => '1',
				'acc_id' => $this->acc_id
			);	
		} 
	   
      $data['categorys']= $this->category_model->viewRecordAnyR($where);
	  //$this->load->template(ADMIN_URL.'/category/lists',$data); 
		$data['contentView'] = 'admin/category/lists';
		$this->load->view('admin/_template_model1', $data);  
  }
	public function savecategory(){
		
		$category_slugp = $this->input->post('category_slug'); 
		$category_name = $this->input->post('category_name');
		$category_slug = strtolower($category_name.'-'.time());
		
		if(!empty($category_slugp)){
				$aray_fetch = array(
					'category_slug' => $category_slugp
				);
				$fetch_value = $this->category_model->viewRecordAny($aray_fetch);
				if(count($fetch_value) > 0){
					$array_save = array(
						'category_name' => $category_name,
						'category_parent' => '0',
						'category_slug' => strreplace($category_slug),
						'acc_id' => $this->acc_id,
					);
					$inserted_id = $this->category_model->recordUpdateSlug('category_slug',$category_slugp, $array_save);
					$this->session->set_flashdata('category_save', 'success');
					redirect(site_url(ADMIN_URL.'/category/lists/'));
				}else{
					redirect('page');
				} 
			}else{
				if(!empty($category_name)){
					$array_save = array(
						'category_name' => $category_name,
						'category_parent' => '0',
						'category_slug' => strreplace($category_slug),
						'acc_id' => $this->acc_id,
					);
					$inserted_id = $this->category_model->recordInsert($array_save);
					$this->session->set_flashdata('category_save', 'success');
					redirect(site_url(ADMIN_URL.'/category/add'));
					}else {
					
					}
			} 
		
	}
}


?>