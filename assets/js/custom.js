function reset() { 
    $("#toggleCSS").attr("href", "alertify.default.css"); 
    alertify.set({
        labels: {
            ok: "OK",
            cancel: "Cancel"
        },
        delay: 3000,
        buttonReverse: false,
        buttonFocus: "ok"
    });
}

$(document).ready(function() {
	var base_url = 'http://billing.zupascrap.com/admin/';
	var base_urlass = 'http://billing.zupascrap.com/assets/';
	var erroradd = 'animated shake errors'; 
   $('#btnLogin').click(function(){
	   var email = $('#email').val();
	   var password = $('#password').val();
	   if($.trim(email) == ''){
		   $('#email').addClass(erroradd);
		   setTimeout(function () {
			   $('#email').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(password) == ''){
		   $('#password').addClass(erroradd);
		   setTimeout(function () {
			   $('#password').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else{
		   return true;
	   }
   });
   $('#btnProduct').click(function(){
	   var p_name = $('#p_name').val();
	   var p_category = $('#p_category').val();
	   var p_price = $('#p_price').val();
	   var p_stock = $('#p_stock').val();
	   var p_memberdiscount = $('#p_memberdiscount').val();
	   var p_location = $('#p_location').val();
	   var p_image = $('#p_image').val();
	   var p_rpolicy = $('.p_rpolicy').is(':checked');
	   
	   if($.trim(p_name) == ''){
		   $('#p_name').addClass(erroradd);
		   setTimeout(function () {
			   $('#p_name').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(p_category) == ''){
		   $('#p_category').addClass(erroradd);
		   setTimeout(function () {
			   $('#p_category').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(p_price) == ''){
		   $('#p_price').addClass(erroradd);
		   setTimeout(function () {
			   $('#p_price').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(p_stock) == ''){
		   $('#p_stock').addClass(erroradd);
		   setTimeout(function () {
			   $('#p_stock').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(p_memberdiscount) == ''){
		   $('#p_memberdiscount').addClass(erroradd);
		   setTimeout(function () {
			   $('#p_memberdiscount').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(p_location) == ''){
		   $('#p_location').addClass(erroradd);
		   setTimeout(function () {
			   $('#p_location').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(p_image) == ''){
		   $('#p_image').addClass(erroradd);
		   setTimeout(function () {
			   $('#p_image').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if(p_rpolicy == false){
		   $('.p_rpolicy').addClass(erroradd);
		   setTimeout(function () {
			   $('.p_rpolicy').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else{
		   return true;
	   }
   });
     
   $('.btnchallanUpdate').click(function(){
	   var Thises = $(this);
	   var challanno = $(this).data('challanno');
	   swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                successMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
					   url: base_url + 'challan/updatestatus',
					   method: "POST",
					   data:{challanno: challanno},
					   success:function(data){
							data = jQuery.parseJSON(data);
						  console.log(data); 
							alertify.warning(data.message);  
						   setTimeout(function(){
							   Thises.hide();
							 // location.reload();
						  },2000); 
					   }
				   });
                } else {
                    swal("Your Data have safe!", {
                        icon: "error",
                    });
                }
            });
	    
   });
   $('.btnCreateChallan').click(function(){
	  var find_check = $('.checkChallan').is(':checked');
	  var c_challan_zone = $('#c_challan_zone').val();
	  if(find_check == false){
		  alert('Please select any item');
		  return false;
	  }else if($.trim(c_challan_zone) == ''){
		  alert('Please select Zone name');
		  return false;
	  }else{
		  var selectedCheck = $("input[name='createChallan[]']:checked").map(function(){
		  return $(this).val();
		  }).get();
		  var form_data = new FormData();
		  form_data.append('consig_code', selectedCheck);
		  form_data.append('c_challan_zone', c_challan_zone);
		  $.ajax({
			 url: base_url + 'challan/challancreate',
			 method: "POST",
			 data: form_data,
			 contentType: false,
			 cache: false,
			 processData: false,
			 success: function(data){ 
			  alertify.success('Challan has been created!');
			   setTimeout(function(){
				  location.reload();
			  },2000);
			  
			 
			 }
		  });
	  }
   });
   
   $('.checkmenpermission').change(function(){
	   var checked = $(this).is(':checked');
	   var menuid = $(this).data('id');
	     var accountid = $(this).data('accountid');
	   var status1;
	   if(checked == true){
		   status1 = '1';
	   }else{
		   status1 = '0';
	   } 
	   $.ajax({
		  url: base_url + 'Userp/checked_menu',
		  method: "POST",
		  data: {menuid:menuid, statusa:status1, accountid:accountid},
		  success:function(data){
			  data = jQuery.parseJSON(data);
			  if(data.status == 1){
			  alertify.success(data.message);
			  }else{
				alertify.error(data.message);  
			  }
			  setTimeout(function(){
				  location.reload();
			  },2000);
			  
		  }
	   }); 
   });
   $('.checkedchid').change(function(){ 
	   var checked = $(this).is(':checked');
	   var menuid = $(this).data('id');
	   var accountid = $(this).data('accountid');
	   var types = $(this).data('types');
	   var status1;
	   if(checked == true){
		   status1 = '1';
	   }else{
		   status1 = '0';
	   } 
	   $.ajax({
		  url: base_url + 'Userp/checked_menu_child',
		  method: "POST",
		  data: {menuid:menuid, statusa:status1, types:types, accountid: accountid},
		  success:function(data){
			  data = jQuery.parseJSON(data);
			  console.log(data); 
				alertify.warning(data.message);  
			  
			  setTimeout(function(){
				  location.reload();
			  },2000);
			  
		  }
	   }); 
   });
   $('#uploadproductImage').click(function(){
	var ext = $('#productImage').val().split('.').pop().toLowerCase();
	var cateimagec1 = document.getElementById("productImage").files[0];
	var cateEdit = $('#productEdit').val();
	 
	if ($.trim(cateimagec1) == '') {
        $("#productImage").notify("Image Required").focus();
        $('#productImage').keyup(function () {
            $('#productImage').removeClass(validationErro);
        });
        return false;
    }
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        $("#productImage").notify("invalid extension!");
        return false;
    } else {
        var form_data = new FormData();
        form_data.append('p_image', cateimagec1);
        form_data.append('eventid', cateEdit);
        $.ajax({
            url: base_url + "product/imageupdate",
            method: "POST",
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                $('#response').html('<img src="'+base_urlass+'loading.gif" />');
            },
            success: function (data) {
				console.log(data);
                 location.reload();
            }
        });
    }
   });
   $(".deleteimg").on('click', function () { 
    var row_id = $(this).data('id'); 
    alertify.confirm("You really want to Delete?", function (e) {
        if (e) {
            $.ajax({
                url: base_url + "product/deleterowimage",
                method: "POST",
                data: {row_id: row_id},
                success: function (data) {
					location.reload();
					console.log(data);
                }
            });
            alertify.success("Record has deleted Successfully");
            //$('.deleteTemptrP' + del_id).animate({backgroundColor: '#003'}, 'slow').animate({opacity: "hide"}, 'slow');
        } else {
            alertify.error("You've Cancel");
        }
    });
    return false;
});
   $('#consignee').change(function(){
	   var consig_id = $(this).val();
	   var consignee = 'consignee';
	   $.ajax({
		  url: base_url + 'client/find_client',
		  method: "POST",
		  data: {consig_id:consig_id, type:consignee},
		  success:function(data){
			  data = jQuery.parseJSON(data);
			  $('#consignee_details').val(data.details);
			  $('#consignee_gst').val(data.gst);
			  $('.chargednug').val(data.price);
			  
		  }
	   }); 
   });
   $('#addMores').click(function(){
	   $('#ssss').clone().insertBefore("#insertbefore");
   });
   $('#consigner').change(function(){
	   var consig_id = $(this).val();
	   var consigner = 'consigner'; 
	   $.ajax({
		  url: base_url + 'client/find_client',
		  method: "POST",
		  data: {consig_id:consig_id, type:consigner},
		  success:function(data){
			  data = jQuery.parseJSON(data);
			  $('#consigner_details').val(data.details);
			  $('#consigner_gst').val(data.gst);
			  
		  }
	   }); 
   });
   $('.add_salary').click(function(){
	   var emp_id = $(this).data('empid');
	   var modeid = $(this).data('modeid');
	   var add_amount = $('#add_amount').val();
	   $.ajax({
		  url: base_url + 'employee/add_salary',
		  method: "POST",
		  data: {emp_id:emp_id, add_amount:add_amount},
		  success:function(data){
			  data = jQuery.parseJSON(data);
			  alertify.success("Salary has been transfered!"); 
			  setTimeout(function(){
				 $('#'+modeid).hide();
			  },1000);
			  
 
			  console.log(data);
			  //$('#consigner_details').val(data.details);
			  //$('#consigner_gst').val(data.gst);
			  
		  }
	   }); 
   });
   $('#consigner').change(function(){
	   var consig_id = $(this).val();
	   var consigner = 'consigner'; 
	   $.ajax({
		  url: base_url + 'client/find_client',
		  method: "POST",
		  data: {consig_id:consig_id, type:consigner},
		  success:function(data){
			  data = jQuery.parseJSON(data);
			  $('#consigner_details').val(data.details);
			  $('#consigner_gst').val(data.gst);
			  
		  }
	   }); 
   });
   $(".page").on("change keyup keydown paste propertychange bind mouseover", function(){
    calculater();
	freight();
	bilty();
	narrationcrg();
	narrationother();
	paid_by_gst();
	by_paid()
});
 $(".totalrem").on("change keyup keydown paste propertychange bind mouseover", function(){
    hamali();
    statistical();
	demurrage();
	 
});
function hamali(){
	  var freight = $('#hamali').val();
	  var grandtotal = $('#totalamount1').val();
	  var subtoal = parseFloat(freight) + parseFloat(grandtotal);
	   if(freight.length !=0){
		$('#totalamount').val(subtoal.toFixed(2));   
	   } 
   }
   function statistical(){
	  var freight = $('#statistical').val();
	  var grandtotal = $('#totalamount').val();
	  var subtoal = parseFloat(freight) + parseFloat(grandtotal);
	   if(freight.length !=0){
		$('#totalamount').val(subtoal.toFixed(2));   
	   } 
   }
   function demurrage(){
	  var freight = $('#demurrage').val();
	  var grandtotal = $('#totalamount').val();
	  var subtoal = parseFloat(freight) + parseFloat(grandtotal);
	   if(freight.length !=0){
		$('#totalamount').val(subtoal.toFixed(2));   
	   } 
   }
   function calculater(){
	  var sum = 0;
	   $('.totalnugprice1').each(function(){
		   if(!isNaN(this.value) && this.value.length !=0){
			   var get_unit = $(this).closest('.row').find('.nugs').val();
			   var get_price = $(this).closest('.row').find('.chargednug').val();
			   var total_sub_toal = (get_unit * get_price);
			   $(this).val(total_sub_toal.toFixed(2));
			   sum += parseFloat(total_sub_toal.toFixed(2));
		   }else{
			    $(this).val(0.00);
		   }
	   });
	     $('#grandtotal').val(sum.toFixed(2));
   }
   function freight(){
	  var freight = $('#freight').val();
	  var grandtotal = $('#grandtotal').val();
	  var subtoal = parseFloat(freight) + parseFloat(grandtotal);
	   if(freight.length !=0){
		$('#grandtotal').val(subtoal.toFixed(2));   
	   } 
   }
    function bilty(){
	  var bilty = $('#bilty').val();
	  var grandtotal = $('#grandtotal').val();
	  var subtoal = parseFloat(bilty) + parseFloat(grandtotal);
	   if(bilty.length !=0){
		$('#grandtotal').val(subtoal.toFixed(2));   
	   } 
   }
   function narrationcrg(){
	  var narrationcrg = $('#narrationcrg').val();
	  var grandtotal = $('#grandtotal').val();
	  var subtoal = parseFloat(narrationcrg) + parseFloat(grandtotal);
	   if(narrationcrg.length !=0){
		$('#grandtotal').val(subtoal.toFixed(2));   
	   } 
   }
   function narrationother(){
	  var narrationother = $('#narrationother').val();
	  var grandtotal = $('#grandtotal').val();
	  var subtoal = parseFloat(narrationother) + parseFloat(grandtotal);
	   if(narrationother.length !=0){
		$('#grandtotal').val(subtoal.toFixed(2));   
	   } 
   }
   function paid_by_gst(){
	  var totalses = parseFloat('750'); 
	 
	  var paid_by = $('#paid_by').val();
	  var gst_percent = $('#gst_percent').val();
	  var grandtotal = $('#grandtotal').val();
	  var toistal = (parseFloat(grandtotal) * parseInt(gst_percent) / 100);
	  
	  
	  
	  if(parseFloat(grandtotal) > totalses){
		  $('.gstclass').show();
		  var nettotal = (parseFloat(grandtotal) + parseFloat(toistal));
		  $('#nettotal').val(nettotal.toFixed(2));
		  if(paid_by == 1){ 
				$('#gst_paid').html('<option value="1" selected>Consinee</option><option value="2">Consiner</option><option value="3">Transporter</option>');
				$('#gstid').html('<div class="col-xl-3 col-md-3 mb-3 gstclass"><div class="form-group"><label>IGST </label><input type="text" name="igst" id="igst" class="form-control" placeholder="" value="'+  toistal.toFixed(2) +'"></div></div>');
			}else{ 
				var totalic = (parseFloat(toistal) / 2); 
			  
			$('#gst_paid').html('<option value="1">Consinee</option><option value="2" selected>Consiner</option><option value="3">Transporter</option>');
			$('#gstid').html('<div class="col-xl-3 col-md-3 mb-3 gstclass"><div class="form-group"><label>CGST </label><input type="text" name="cgst" id="cgst" class="form-control" placeholder="" value="'+ totalic.toFixed(2) +'"></div></div><div class="col-xl-3 col-md-3 mb-3 gstclass"><div class="form-group"><label>SGST </label><input type="text" name="sgst" id="sgst" class="form-control" placeholder="" value="'+ totalic.toFixed(2) +'"></div></div>');
			}
	  }else{
		  var nettotal = parseFloat(grandtotal);
		  $('#nettotal').val(nettotal.toFixed(2));
		  $('.gstclass').hide();
	  }	
	   
	   
   }
   function by_paid(){
	  var paid_by = $('#paid_by').val(); 
	  if(paid_by == 1){ 
			$('select[name="payment_type"]').hide();
			$('input[name="payment_type"]').show();
	  }else{
		  $('select[name="payment_type"]').show();
		  $('input[name="payment_type"]').hide();
	  }
	  
   }
   
   /*
   function calculator() {
        var amt = $('.totalnugprice'),
            tot = $('#grandtotal');
        amt.text(function () {
            var tr = $(this).closest('.row');
            var qty = tr.find('.nug_unit').val();
            var price = tr.find('.chargednug').val();
            return parseFloat(qty) * parseFloat(price);
        });
        tot.text(function () {
            var sum = 0;
            amt.each(function () {
                sum += parseFloat($(this).text())
            });
            return sum;
        });
    }
    calculator();
    $('.nugs,.chargednug').change(calculator);
	/*
	$('.nugs').keyup(function(){
	   var hiss = $(this);
	   var nug_unit = hiss.val();
	   var nug_price = hiss.closest('.row').find('.chargednug').val();
	   var total_price = (nug_unit * nug_price);
	   hiss.closest('.row').find('.totalnugprice').val(total_price);  
   });
   $('.chargednug').keyup(function(){
	   var hiss = $(this);
	   var nug_unit = hiss.val();
	   var nug_price = hiss.closest('.row').find('.nugs').val();
	   var total_price = (nug_unit * nug_price);
	   hiss.closest('.row').find('.totalnugprice').val(total_price);
   });
   */
  $('#genrateGate').click(function(){  
  
	 
	var payment_type11 = $('#payment_type11').val(); 
	var receiver_name = $('#receiver_name').val(); 
	 
	if($.trim(payment_type11) == ''){
		   $('#payment_type11').addClass(erroradd);
		   $('#payment_type11').notify('Payment mode required!').focus();
		   setTimeout(function () {
			   $('#payment_type11').removeClass(erroradd);
			}, 2000);
		   return false;
	}else if($.trim(receiver_name) == ''){
		   $('#receiver_name').addClass(erroradd);
		   $('#receiver_name').notify('Receiver name required!').focus();
		   setTimeout(function () {
			   $('#receiver_name').removeClass(erroradd);
			}, 2000);
		   return false;
	}else{
		return true;
	} 
	
  });
   $('#btnSubmitCongsiment').click(function(){ 
		var This = $(this);
		var consignment_slug = $('#consignment_slug').val();
		var d_week = $('#d_week').val().split("/");
		var bilty_date = d_week[2] +'-'+ d_week[1] +'-'+ d_week[0]; 
		var consignee_name = $('#consignee').val();
		var truck = $('#truck').val(); 
		var zone_from = $('#zone_from').val(); 
		var zone_to = $('#zone_to').val(); 
		var consignee_details = $('#consignee_details').val();
		var consignee_gst = $('#consignee_gst').val();
		var consigner = $('#consigner').val();
		var consigner_details = $('#consigner_details').val();
		var consigner_gst = $('#consigner_gst').val();
		var freight = $('#freight').val();
		var bilty = $('#bilty').val();
		var narrationcrg = $('#narrationcrg').val();
		var narrationother = $('#narrationother').val();
		var grandtotal = $('#grandtotal').val();
		var invoice_no = $('#invoice_no').val();
		var gst_percent = $('#gst_percent').val();
		var gst_paid = $('#gst_paid').val();
		var sgst = $('#sgst').val();
		var cgst = $('#cgst').val();
		var igst = $('#igst').val();
		var nettotal = $('#nettotal').val();
		var form_number = $('#form_number').val();
		var rs_value = $('#rs_value').val();
		var paid_by = $('#paid_by').val();
		var payment_type = $('#payment_type').val(); 		
		var goods_name = $("select[name='goods_name[]']")
              .map(function(){return $(this).val();}).get().join( ", " ); 
		var goods_nugs = $("input[name='nugs[]']").map(function(){return $(this).val();}).get().join( ", " );
		var chargednug = $("input[name='chargednug[]']").map(function(){return $(this).val();}).get().join( ", " );
		var weightnug = $("input[name='weightnug[]']").map(function(){return $(this).val();}).get().join( ", " ); 
		if($.trim(truck) == ''){
		   $('#truck').addClass(erroradd);
		   $('#truck').notify('Select Truck required!').focus();
		   setTimeout(function () {
			   $('#truck').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(zone_to) == ''){
		   $('#zone_to').addClass(erroradd);
		   $('#zone_to').notify('Select zone name required!').focus();
		   setTimeout(function () {
			   $('#zone_to').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(consignee_name) == ''){
		   $('#consignee').addClass(erroradd);
		   $('#consignee').notify('Select Consignee Name required!').focus();
		   setTimeout(function () {
			   $('#consignee').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(consigner) == ''){
		   $('#consigner').addClass(erroradd);
		   $('#consigner').notify('Select consigner Name required!').focus();
		   setTimeout(function () {
			   $('#consigner').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(invoice_no) == ''){
		   $('#invoice_no').addClass(erroradd);
		   $('#invoice_no').notify('Please Enter Invoice Name required!').focus();
		   setTimeout(function () {
			   $('#invoice_no').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(form_number) == ''){
		   $('#form_number').addClass(erroradd);
		   $('#form_number').notify('Please Enter Invoice Number required!').focus();
		   setTimeout(function () {
			   $('#form_number').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else{
		   var data_var = {
			   bilty_date:bilty_date,
			   goods_name:goods_name,
			   goods_nugs:goods_nugs,
			   chargednug:chargednug,
			   weightnug:weightnug,
			   truck : truck,
			   zone_from : zone_from,
			   zone_to : zone_to,
				consignment_slug : consignment_slug,
				consignee_name : consignee_name,
				consignee_details : consignee_details,
				consignee_gst : consignee_gst,
				consigner : consigner,
				consigner_details : consigner_details,
				consigner_gst : consigner_gst,
				freight : freight,
				bilty : bilty,
				narrationcrg : narrationcrg,
				narrationother : narrationother,
				grandtotal : grandtotal,
				invoice_no : invoice_no,
				gst_percent : gst_percent,
				gst_paid : gst_paid,
				sgst : sgst,
				cgst : cgst,
				igst : igst,
				nettotal : nettotal,
				form_number : form_number,
				rs_value : rs_value,
				paid_by : paid_by,
				payment_type : payment_type, 
		   };
		   This.attr("disabled", true);
		   $.ajax({
			   url: base_url + "consignment/saveconsignment",
			   type: "POST",
			   data: data_var,
			   success: function(data){
				    data = jQuery.parseJSON(data); 
				   console.log(data);
				    alertify.success("Consignment has been created!"); 
				   setTimeout(function(){
					   window.location.href = data.url;
				   }, 2000);
				   
			   }
		   });
		}
   });
   
	 $('#btnAddReturn').click(function(){
	   var nodays = $('#nodays').val();
	   var r_cutoff = $('#r_cutoff').val();
	   var r_description = $('#r_description').val();
	    
	   if($.trim(nodays) == ''){
		   $('#nodays').addClass(erroradd);
		   $('#nodays').notify('Number of days required!').focus();
		   setTimeout(function () {
			   $('#nodays').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else if($.trim(r_cutoff) == ''){
		   $('#r_cutoff').addClass(erroradd);
		    $('#r_cutoff').notify('Cut off price required!').focus();
		   setTimeout(function () {
			   $('#r_cutoff').removeClass(erroradd);
			}, 2000);
		   return false;
	   }else{
		   return true;
	   }
   });
	 
	$('#btnAddCategory').click(function(){
	   var category_name = $('#category_name').val();
	   if($.trim(category_name) == ''){
		   $('#category_name').addClass(erroradd);
		   $('#category_name').notify('Category id required!').focus();
		   setTimeout(function () {
			   $('#category_name').removeClass(erroradd);
			}, 2000);
		   return false;
	   }
	   else{
		   return true;
	   }
   });
   $(".quantity").keypress(function (e) {

    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g))

        return false;

});

$(".number").keypress(function (evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode

    if (charCode > 31 && (charCode < 48 || charCode > 57)) {

        $(this).notify("Only Number");

        return false;

    }

});

$(".strnum").keypress(function (event) { 

     var regex = new RegExp("^[a-zA-Z0-9 ._\b]+$"); 

    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    if (!regex.test(key)) {

        $(this).notify("Only Alphabets, Number");

        return false;

    }

});

$(".str").keypress(function (e) {

    if (  (e.which >= 65 && e.which <= 90) || (e.which >= 97 && e.which <= 122) || (e.which == 32) || (e.which== 8)) {

        return true;

    }else{

    $(this).notify("Only alphabets");

     return false;

   }

}); 

$(".price").keypress(function () { 
    var regex = new RegExp("^[0-9.]+$"); 
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
        $(this).notify("Only Price"); 
        return false;
    }	
});
$(".deletes").click(function(){
	var deleturl1 = $(this).data('url');
	var deleteid = $(this).data('deleteid'); 
    alertify.confirm("You really want to Delete?", function (e) { 
        if (e) { 
			$.ajax({
				url: base_url + deleturl1,
				type: "POST",
				data: {deleteid: deleteid},
				success: function(data){
					console.log(data);
					location.reload();
				}
			});
	} else { 
            alertify.error("You've Cancel"); 
        }

    }); 
    return false; 
});
$("#addmore").click(function(){
	$('.first').toggle();
});
$(".addmoregoods").click(function(){
	var consigmentd = $(this).data('consigmentd');
	var goods_name = $(this).closest('.row').find('.goods_name').val(); 
	 
	var nugs = $(this).closest('.row').find('.nugs').val(); 
	var chargednug = $(this).closest('.row').find('.chargednug').val(); 
	var weightnug = $(this).closest('.row').find('.weightnug').val();   
			$.ajax({
				url: base_url + 'consignment/addgoods',
				type: "POST",
				data: {consigmentd: consigmentd, goods_name: goods_name, nugs:nugs, chargednug:chargednug, weightnug:weightnug},
				success: function(data){
					console.log(data);
					location.reload();
			}
		}); 
	});	
	$(".find_city").change(function(){
		var state_id = $(this).val();
			$.ajax({
			url: base_url + 'master/find_city',
			type: "POST",
			data: {state_id: state_id},
			success: function(data){
				$('#userajaxcity').html(data);
			}
		}); 
	});
	$('#payment_type11').change(function(){
	   var consig_id = $(this).val();
	   if(consig_id == 1){
		   $('#checks').hide();
	   }else{
		   $('#checks').show();
	   } 
   });	
});